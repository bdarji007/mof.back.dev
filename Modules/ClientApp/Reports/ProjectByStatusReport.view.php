<?php

use \koolreport\widgets\koolphp\Table;
use \koolreport\processes\CalculatedColumn;
use \koolreport\inputs\BSelect;
use \koolreport\inputs\Select;
use \koolreport\processes\Sort;
use \koolreport\inputs\Select2;
use \koolreport\datagrid\DataTables;
use \koolreport\sparklines;
use \koolreport\inputs\DateTimePicker;
use \koolreport\inputs\CheckBoxList;
use Modules\ClientApp\Reports\ProjectByStatusReport;
use \koolreport\processes\Filter;
use \koolreport\widgets\google\BarChart;
use \koolreport\widgets\google\ColumnChart;
use \koolreport\widgets\google\TreeMap;


$language = '';
if (isset($this->params['language']) && !empty($this->params['language'])) {
    $language = $this->params['language'];
}

//-------------------------generating datastore for section------------------------------
if ($this->params["sector"] != null) {

    // ----------------------------------------selecting roles corresponding to this sector--------------------------------------------
    if ($this->datastore('section111')->count() > 0) {
        if ($this->params["oid"] != "null") {

            $role_section_id = $this->dataStore("section111")->only("id")->data();
            $j = 0;
            foreach ($role_section_id as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v != NULL) {
                        // echo "Key=" . $k . ", Value=" . $v;
                        // echo "<br>";
                        $r_section[$j] = $v;
                        $j = $j + 1;
                    }
                }
            }
            $store = [];


            foreach ($r_section as $key => $v) {
                $section_name = "sect" . $v;
                $store = array_merge($store, $this->dataStore($section_name)->data());
            }
        }
    }

    if ($this->datastore('section222')->count() > 0) {
        if ($this->params["oid"] != "null") {
            $store = $this->dataStore("section222")
                ->filter(function ($row) {
                    return $row["id"] != NULL;
                });

        }
    }

    if ($this->params["oid"] == "null") {
        $store = $this->dataStore("section1")
            ->filter(function ($row) {
                return $row["id"] != NULL;
            });

    }
}
//----------------------------------------------------------------------------------

$sector_name = $this->dataStore('sector_name');

$sector11 = $sector_name->get(0, "name");
$transtable = $this->dataStore('translation');
// var_dump($this->params['project_list']);

// echo "<pre>";
$treemap = $this->dataStore('project_details1')->only('proj_name1', 'parent', 'pd_planned_duration1', 'color', 'proj_id')->prepend(array("proj_name" => "project", "parent" => null, "pd_planned_duration1" => 0, "color" => 0, "proj_id" => null))->toTableArray();

$ONTRACK = $this->dataStore("ontrackpie")->filter("sv_sched_var_pct", "!=", NULL);
$CRITICAL = $this->dataStore("criticalpie")->filter("sv_sched_var_pct", "!=", NULL);


// echo "<pre>";
// var_dump($ONTRACK->toArray());
// var_dump($treemap);
// var_dump($this->params['sector']);

// $new=$this->datastore('project_details')
//prepend(array("proj_name"=>"John","age"=>35))->
?>

<!DOCTYPE html>
<?php if ($language == 'ar')
    $dir = "rtl";

else
    $dir = "ltr";
?>
<html dir="<?php echo $dir; ?>">
<!-- <html dir="rtl" onchange="console.log('value of name field is changed')"> -->
<head>
    <meta charset="utf-8">
    <title>
    </title>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

</head>
<style>
    .buttons-print {
        background-color: #ffffff;
        boder: none;
    }

    table {
        width: 70%;
        table-layout: fixed;
        /* transform: scale(0.50) translate(-100px,0px); */

    }


    .color {
        border: 0px solid black;
    }

    .insideBorder {
        border: 10px solid white;
    }

    .line {
        width: 1320px;
        border-bottom: 1px solid black;
        position: absolute;
    }

    .dataTables_filter input {
        width: 450px
    }

    .cssHeader {
        background-color: #73818f;
        color: #fff;
        text-align: <?php echo $language=='ar'?'right':'left' ;?>;
        font-size: 12px;
        overflow-wrap: break-word;
    }

    .cssItem {
        background-color: #fdffe8;
        font-size: 12px;
    }

    .container {
        display: flex;
        width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .container1 {
        display: inline-flex;
        column-width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .header img {
        float: left;
        width: 100px;
        height: 100px;
        background: #555;
    }

    .select {
        margin: 10px 10px 0px 325px;
    }

    .checkbox input[type="checkbox"]
        /* input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"]{ */
    {
        position: relative;
        margin-left: 0px;
        float: right;
        font-weight: 700;
    }

    .checkbox label {

        /* float:right; */
        font-weight: 300;
        text-align: left;
        padding: 10px;
        /* margin-top:10px; */
        margin-bottom: 10px;
        /* margin-left:20px; */

    }

    .decoratedLine1, .decoratedLine2, .decoratedLine3, .decoratedLine4, .decoratedLine5 {
        overflow: hidden;
    }

    .decoratedLine1 > span, .decoratedLine2 > span, .decoratedLine3 > span, .decoratedLine4 > span, .decoratedLine5 > span {
        position: relative;
        display: inline-block;
    }

    .decoratedLine1 > span:before, .decoratedLine2 > span:before, .decoratedLine3 > span:before, .decoratedLine4 > span:before, .decoratedLine5 > span:before {
        content: '';
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        width: 591px;
        margin: 0 20px;
    }

    .decoratedLine1 > span:before {
        border-bottom: 12px solid #01996d;
    }

    .decoratedLine2 > span:before {
        border-bottom: 12px solid #cc043e;
    }

    .decoratedLine1En > span:before, .decoratedLine2En > span:before, .decoratedLine3En > span:before, .decoratedLine4En > span:before, .decoratedLine5En > span:before {
        right: 100%;
    }

    .decoratedLine1Ar > span:before, .decoratedLine2Ar > span:before, .decoratedLine3Ar > span:before, .decoratedLine4Ar > span:before, .decoratedLine5Ar > span:before {
        left: 100%;
    }

    /* .table {
    /* font-size: 50%; */
    /* transform: <\?php echo $language=='ar'?'scale(0.7) translate(205px,50px); ':"" ;?>; */
    /* margin-right:20px;
    padding-right:20px; */
    /* } */


</style>
<body>
<p id="printarea"></p>
<div id="printissue">
    <div id="page"
         style="background-color:#ffffff;margin-left:10px;margin-right:5px;margin-top:0px;margin-bottom:30px;padding-top:30px;padding-right:10px;padding-left:10px;">
        <h4 class="mb-0 pt-2" style="text-align:center;color: #20a8d8;font-size: 20px;font-weight: normal;">
            <?php $textbit = 'title';
            $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_ar"));
            echo $translation ?>
        </h4>
        <scan class="form-group"
              style="float:<?php echo $language == 'ar' ? 'left' : 'right'; ?>;padding-right:150px;padding-left:150px;">
            <script> document.write(new Date().toDateString()); </script>
        </scan>

        <?php if ($language == 'ar') {
            $dir = "rtl";
            $lin = "left";
        } else {
            $dir = "ltr";
            $lin = "right";
        }
        ?>
        <div style="background-color:#ffffff;border: 1px solid #a5aeb7;position:relative;" class="col-md-12"
             style="float:right;">
            <div id="button1" dir="rtl" style="float:<?php echo $lin; ?>">
                <button onclick="myFunction();" style="float:left;border:none;background-color: #ffffff;"><i
                        style="padding-top:5px;color:#a9a9a9;;" class="fa fa-angle-up 4x"></i></button>
                <button onClick="javascript:printDiv()"
                        style="float:left;border:none;background-color: #ffffff;"><i
                        style="padding-top:5px;color:#a9a9a9;font-size:12px" class="fa fa-print "></i></button>

            </div>
        </div>
        <br/>
        <?php $new = $this->dataStore('project_details')->process(new CalculatedColumn(array("id" => "{#}+1"))); ?>
        <?php $style = "";
        if (empty($_POST)) {
            $style = 'display:none !important;';
        } else {
            $style = 'display:block !important;';
        } ?>
        <?php if ($language == 'ar') {
            $dir = "rtl";
            $lin = "right";
        } else {
            $dir = "ltr";
            $lin = "left";
        }
        ?>

        <div class="col-md-12" id="myDIV"
             style="overflow:auto;width:100%;background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;<?php echo $style; ?>">
            <form id="form1" method="post">
                <!-- <input type="hidden" id="unit_pie" name="unit_pie" value="<\?php echo($this->params['click_unit'] == 1) ? '2' : '1' ?>"  >
               <input type="hidden" id="summary_pie" name="summary_pie" value="<\?php echo($this->params['click_summary'] == 1) ? '2' : '1' ?>">	 -->

                <div class="col-md-3 form-group" style="float:<?php echo $lin; ?>">
                    <strong>
                        <!-- sector  -->
                        <?php $textbit = 'sector';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php
                    select2::create(array(
                        // "multiple"=>false,
                        "name" => "sector",
                        "defaultOption" => array("--" => null),
                        "dataStore" => $this->dataStore("sector1"),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){
                        $('#section').val(null).trigger('change');

                        $('#form1').submit();
                    }"

                        )
                    ));
                    ?>
                </div>
                <div class="col-md-3 form-group" style="float: <?php echo $lin; ?>">
                    <strong>
                        <!-- section                  -->
                        <?php $textbit = 'section';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php

                    Select2::create(array(
                        // "multiple"=>true,
                        "name" => "section",
                        "defaultOption" => array("--" => null),
                        "dataStore" => $store,
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){
                        $('#form1').submit();
                    }"

                        )
                    ));
                    ?>
                </div>

                <div class="col-md-3 form-group" style="float: <?php echo $lin; ?>">
                    <strong>
                        <!-- mtp -->
                        <?php $textbit = 'mtp';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php

                    Select2::create(array(
                        // "multiple"=>true,
                        "name" => "mtp",
                        "defaultOption" => array("--" => ""),
                        "dataStore" => $this->dataStore("mtp1"),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){
                        $('#form1').submit();
                    }"
                        ),

                    ));
                    ?>
                </div>
                <div class="col-md-3 form-group" style="float:<?php echo $lin; ?>">
                    <strong>
                        <!-- project_list -->
                        <?php $textbit = 'project_list';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_ar"));
                        echo $translation ?>               </strong>
                    <?php
                    select2::create(array(
                        "multiple" => true,
                        "name" => "proj_id",
                        "dataStore" => $this->dataStore("project_name"),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        // "clientEvents" => array(
                        //             "change" => "function(params){
                        //         $('#form1').submit();
                        //     }"
                        //         ),


                    ));
                    ?>
                </div>
                <div class="col-md-3 form-group" style="float:<?php echo $lin; ?>">
                    <strong>
                        <!-- project_category -->
                        <?php $textbit = 'project_category';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                    <?php
                    select2::create(array(
                        // "multiple" => true,
                        "name" => "project_cat",
                        // "dataStore" => $this->dataStore("project_"),
                        "defaultOption" => array("--" => ""),
                        "data" => array(
                            $language == 'en' ? $transtable->where('key_name', 'ministry')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'ministry')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 1,
                            $language == 'en' ? $transtable->where('key_name', 'development')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'development')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 2,
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),


                    ));
                    ?>
                </div>
                <div class="col-md-3 form-group" style="float:<?php echo $lin; ?>">
                    <strong>
                        <!-- project_type -->
                        <?php $textbit = 'project_type';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php
                    select2::create(array(
                        "multiple" => true,
                        "name" => "project_type",
                        "dataStore" => $this->dataStore("project_type"),
                        // "defaultOption" => array("--" => null),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        //     "clientEvents" => array(
                        //         "change" => "function(params){
                        //     $('#form1').submit();
                        // }"
                        //     ),

                    ));
                    ?>
                </div>
                <div class="col-md-3 form-group" style="float: <?php echo $lin; ?>">
                    <strong>
                        <!-- project_operational_status -->
                        <?php $textbit = 'status_operational';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_status_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                    <?php
                    select2::create(array(
                        "multiple" => true,
                        "name" => "status_operational",
                        // "defaultOption" => array("--" => ""),
                        "data" => array(
                            $language == 'en' ? $transtable->where('key_name', 'closed')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'closed')->where('key_pos', 'project_status_report')->get(0, "value_ar") => 0,
                            $language == 'en' ? $transtable->where('key_name', 'planning')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'planning')->where('key_pos', 'project_status_report')->get(0, "value_ar") => 1,
                            $language == 'en' ? $transtable->where('key_name', 'progressing')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'progressing')->where('key_pos', 'project_status_report')->get(0, "value_ar") => 2,
                            $language == 'en' ? $transtable->where('key_name', 'on_hold')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'on_hold')->where('key_pos', 'project_status_report')->get(0, "value_ar") => 3,


                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        //     "clientEvents" => array(
                        //         "change" => "function(params){
                        //     $('#form1').submit();
                        // }"


                    ));
                    ?>
                </div>

            </form>
            <!-- </div> -->
        </div>
        <!-- <div class="card"> -->
        <div class="col-md-12" id="pivot"
             style="overflow:auto;width:100%;background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:10px;padding-right:10px;">
            <div class="table1 col-md-8">
                <?php
                DataTables::create(array(
                    "dataSource" => $new,
                    "name" => "example",
                    "columns" => array(
                        "id" => array(
                            "label" => "#",
                        ),
                        "proj_symbol" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'proj_symbol')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_symbol')->where('key_pos', 'project_status_report')->get(0, "value_ar")
                            // "رمز",
                        ),
                        "proj_name" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'proj_name')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_name')->where('key_pos', 'project_status_report')->get(0, "value_ar"),
                            //"الاسم",
                            "cssStyle" => "overflow-wrap:beak-word;",


                        ),
                        "org_unit_name" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'org_unit_name')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'org_unit_name')->where('key_pos', 'project_status_report')->get(0, "value_ar"),
                            //"org unit",
                            "cssStyle" => "overflow-wrap:beak-word;",

                        ),

                        "proj_mgr_name" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'proj_mgr_name')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_mgr_name')->where('key_pos', 'project_status_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                            "cssStyle" => "overflow-wrap:beak-word;"

                        ),
                        "proj_owner_name" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'proj_owner_name')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_owner_name')->where('key_pos', 'project_status_report')->get(0, "value_ar"),//"communication officer",
                            "cssStyle" => "overflow-wrap:beak-word;"

                        ),

                        "start_dt_actual" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'start_dt_actual')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'start_dt_actual')->where('key_pos', 'project_status_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                            "cssStyle" => "overflow-wrap:beak-word;"

                        ),
                        "end_dt_base" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'end_dt_base')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'end_dt_base')->where('key_pos', 'project_status_report')->get(0, "value_ar"),//"communication officer",
                            "cssStyle" => "overflow-wrap:beak-word;"

                        ),
                        "progress_val" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'progress_val')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'progress_val')->where('key_pos', 'project_status_report')->get(0, "value_ar"),//"communication officer",
                            "cssStyle" => "overflow-wrap:beak-word;",
                            'formatValue' => function ($val, $row) {
                                if ($val != null) {
                                    $language = $this->params['language'];
                                    $data = number_format((float)($val), 4);
                                    $lin = $language == 'en' ? 'left' : 'right';
                                    //$data=(float)($val);
                                    if ($this->params['debug_modeprog'] == false && $data < 0)
                                        $data = 0;

                                    $data_val = (float)($val) * 100;
                                    //$data_val = preg_replace("/\.?0+$/", "", $data);

                                    return "<div style=\"text-align:$lin\">$data_val %</div>";

                                }
                            }

                        ),
                        "sv_sched_var_pct" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'sv_sched_var_pct')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'sv_sched_var_pct')->where('key_pos', 'project_status_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                            "cssStyle" => "overflow-wrap:beak-word;",
                            'formatValue' => function ($val, $row) {
                                if ($val != null) {
                                    $data = number_format((float)($val), 4);
                                    $data_val = $data * 100;
                                    return "<div>$data_val %</div>";

                                }
                            }

                        ),

                        "proj_status_name" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'proj_status_name')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_status_name')->where('key_pos', 'project_status_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                            "formatValue" => function ($value, $row) {
                                $language = $this->params['language'];
                                $transtable = $this->dataStore('translation');
                                $status = $value;
                                $color = '#' . $row["color_code"];

                                // if ($value < 0) {
                                // $term='delete_pending';
                                // $term = $language == 'en' ? $transtable->where('key_name', 'delete_pending')->where('key_pos', 'project_status_report ')->get(0, "value_en") : $transtable->where('key_name', 'delete_pending')->where('key_pos', 'project_status_report')->get(0, "value_ar");
                                return "<div> <i style=\"color:$color;\" class=\"fas fa-circle\"></i>&nbsp;&nbsp;" . $status . "&nbsp;</div>";                            // }

                            }
                            //  "Next Value Date"  ,
                        ),

                        "has_budget" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'has_budget')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'has_budget')->where('key_pos', 'project_status_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                            "formatValue" => function ($value, $row) {
                                if ($value == 1) {
                                    return "<div> <i class=\"fa fa-file-invoice-dollar fa-2x\"></i></div>";
                                    // return "<div>".$value."</div>";


                                } else {
                                    return "<div>" . '-' . "</div>";

                                }
                            }
                        ),

                        // "proj_id"
                        "proj_id" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'proj_id')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_id')->where('key_pos', 'project_status_report')->get(0, "value_ar"),//"communication officer",
                            "cssStyle" => "overflow-wrap:beak-word;"

                        ),
                        "project_type" => array(
                            //"label" => $language == 'en' ? $transtable->where('key_name', 'project_type')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'project_type')->where('key_pos', 'project_status_report')->get(0, "value_ar"),//"communication officer",
                            "cssStyle" => "overflow-wrap:beak-word;"

                        ),
                        "status_operational" => array(
                            // "label" => $language == 'en' ? $transtable->where('key_name', 'status_operational')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'status_operational')->where('key_pos', 'project_status_report')->get(0, "value_ar"),//"communication officer",
                            "cssStyle" => "overflow-wrap:beak-word;"

                        ),
                        "project_category" => array(
                            "label" => $language == 'en' ? $transtable->where('key_name', 'project_category')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'project_category')->where('key_pos', 'project_status_report')->get(0, "value_ar"),//"communication officer",
                            "cssStyle" => "overflow-wrap:beak-word;"

                        ),
                    ),
                    "cssClass" => array(
                        "table" => "table table-striped table-bordered color  ",
                        "th" => "cssHeader insideBorder ",
                        "tr" => "cssItem color",
                        "td" => "insideBorder"
                    ),
                    "options" => array(
                        "columnDefs" => array(
                            array("width" => 5, "targets" => 0),
                            array("width" => 10, "targets" => 1),
                            array("width" => 10, "targets" => 2),
                            array("width" => 10, "targets" => 3),
                            array("width" => 10, "targets" => 4),
                            array("width" => 10, "targets" => 5),
                            array("width" => 10, "targets" => 6),
                            array("width" => 10, "targets" => 7),
                            array("width" => 10, "targets" => 8),
                            array("width" => 10, "targets" => 9),
                            array("width" => 20, "targets" => 10),
                            array("width" => 10, "targets" => 11),

                        ),
                        "searching" => true,
                        "paging" => true,
                        "orders" => array(
                            array(0, "asc")
                        )
                    )


                ));
                ?>
            </div>
            <!-- <div class="row">

                <div class="col-sm-3">
                    <div
                        class="decoratedLine2 vlabelBold <\?php echo($language == 'en' ? 'decoratedLine2En' : 'decoratedLine2Ar') ?>"
                        style="<\?php echo($language == 'en' ? 'padding-left: 40px;' :
                            'padding-right: 40px;') ?>"><span><\?php echo "on track"; ?></span></div>
                </div>
                <div class="col-sm-3">
                    <div class="decoratedLine2 vlabelBold <\?php echo($language == 'en' ? 'decoratedLine2En' :
                        'decoratedLine2Ar') ?>" style="<\?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                            40px;') ?>"><span><\?php echo "critical" ?></span></div>
                </div>
            </div> -->
            <br>
            <!-- </div> -->

            <div id="charts1" class="col-md-4">
                <div class="row charts" id="piechart1" style="text-align:center;">
                    <h5>
                        <b><?php echo $this->params['language'] == 'en' ? 'Project Summary' : 'مخطط نسب الوحدات المؤدية/غير المؤدية'; ?></b>
                    </h5>
                    <?php
                    $p = $this->params['language'] == 'en' ? 'On Track' : 'مؤدي';
                    $np = $this->params['language'] == 'en' ? 'Critical' : 'غير مؤدي';

                    $performing_count = $ONTRACK->count();
                    $nonperforming_count = $CRITICAL->count();

                    \koolreport\d3\PieChart::create(array(
                        "dataStore" => array(
                            array("category" => $p, "count" => $performing_count),
                            array("category" => $np, "count" => $nonperforming_count),
                        ),
                        "colorScheme" => array(
                            "#01996d",
                            "#cc043e",
                        ),
                        "columns" => array(
                            "category" => array(
                                "cssStyle" => "white-space:nowrap"
                            ),
                            "count" => array(
                                "type" => "number",
                                "config" => array(
                                    "backgroundColor" => array("#0475CC", "#cc043e")
                                    //"backgroundColor"=>,
                                )
                                //"prefix"=>"$",
                            )
                        ),
                        "label" => array(
                            "use" => "ratio",
                        ),
                        "tooltip" => array(
                            "use" => "value",
                            //    "prefix"=>"$"
                        ),
                        "options" => array(
                            "legend" => array(
                                "position" => "bottom", // Accept "bottom",critical 1 "right", "inset"
                                "show" => false,
                            ),
                            "pieSliceText" => 'value',
                            "pieSliceText" => 'value-and-percentage',
                        ),
                        "clientEvents" => array(
                            "itemSelect" => "function(params){
                        console.log('hi');
                        var table = $('#example').DataTable();
                        console.log(params);

                        if(params.selectedRowIndex==1){
                        $.fn.dataTable.ext.search.push(
                            function(settings, data, dataIndex) {
                                var value1 = parseFloat(data[9] );
                                console.log(value1);
                                if(value1<0)
                                {
                                    return true;
                                }
                                else {
                                    return false;
                                }

                            }
                        );
                    }

                    if(params.selectedRowIndex==0){
                        $.fn.dataTable.ext.search.push(
                            function(settings, data, dataIndex) {
                                var value1 = parseFloat( data[9] );
                                console.log(value1);
                                if(value1>0)
                                {
                                    return true;
                                }
                                else {
                                    return false;
                                }

                            }
                        );
                    }

                        table.draw();
                        $.fn.dataTable.ext.search.pop();


                    }",
                        )

                    ));
                    ?>
                </div>
                <div class="row">
                    <div class="col-sm-2"></div>

                    <div class="col-sm-3">
                        <div class="decoratedLine1 vlabelBold <?php echo($language == 'en' ? 'decoratedLine1En' :
                            'decoratedLine1Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>"><span><?php echo "On Track" ?></span></div>
                    </div>
                    <div class="col-sm-3">
                        <div class="decoratedLine2 vlabelBold <?php echo($language == 'en' ? 'decoratedLine2En' :
                            'decoratedLine2Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>"><span><?php echo "Critical" ?></span></div>
                    </div>
                </div>
                <div class="row charts" id="piechart" style="text-align:center;">
                    <h5>
                        <b><?php echo $this->params['language'] == 'en' ? 'Project by Units' : 'مخطط نسب الوحدات المؤدية/غير المؤدية'; ?></b>
                    </h5>
                    <?php
                    // $p = $this->params['language'] == 'en' ? 'Performing' : 'مؤدي';
                    // $np = $this->params['language'] == 'en' ? 'Not Performing' : 'غير مؤدي';

                    $performing_count = 80;//$this->dataStore("performing1pie")->count();
                    $nonperforming_count = 90;//$this->dataStore("nonperforming1pie")->count();
                    \koolreport\d3\PieChart::create(array(
                        "dataStore" => $this->dataStore('project_units_pie'),
                        // "colorScheme" => array(
                        //     "#01996d",
                        //     "#cc043e",
                        // ),
                        "columns" => array(
                            "org_unit_name" => array(
                                "cssStyle" => "white-space:nowrap"
                            ),
                            "proj_id" => array(
                                "type" => "number",
                                // "config" => array(
                                //     "backgroundColor" => array("#0475CC", "#cc043e")
                                //     //"backgroundColor"=>,
                                // )
                                //"prefix"=>"$",
                            )
                        ),
                        "labels" => array(
                            "use" => "ratio",

                        ),
                        "tooltip" => array(
                            "use" => "value",
                            //    "prefix"=>"$"
                        ),
                        "options" => array(
                            "legend" => array(
                                "position" => "bottom", // Accept "bottom", "right", "inset"
                                "show" => false,
                            ),
                            "pieSliceText" => 'value-and-percentage',
                        ),
                        "clientEvents" => array(
                            "itemSelect" => "function(params){
                        console.log(params.selectedRow[0]);
                        var table = $('#example').DataTable();

                        console.log(params);

                        $.fn.dataTable.ext.search.push(
                            function(settings, data, dataIndex) {
                                var value1 = data[3];
                                console.log(value1);
                                if(value1===params.selectedRow[0])
                                {
                                    return true;
                                }
                                else {
                                    return false;
                                }

                            }
                        );

                        table.draw();
                        $.fn.dataTable.ext.search.pop();

                    }",
                        )

                    ));
                    ?>
                </div>
                <div class="row charts" id="treemap" style="text-align:center;">
                    <?php

                    // $data = [
                    //     ['Location', 'Parent', 'Market trade volume (size)', 'Market increase/decrease (color)'],
                    //     ['Global', null, 0, 0],
                    //     ['project1', 'Global', 360, 1],
                    //     ['project2', 'Global', 350, 2],
                    //     ['project3', 'Global', 340, 3],
                    //     ['project4', 'Global', 330, 1],
                    //     ['project5', 'Global', 320, 2],
                    // ];


                    TreeMap::create(array(
                        "dataSource" => $treemap,
                        "options" => array(
                            "minColor" => "#cc043e",
                            "midColor" => '#ddd',
                            "maxColor" => "#01996d",
                            "headerHeight" => 15,
                            "fontColor" => 'black',
                            "showScale" => true
                        ),


                        "clientEvents" => array(
                            "select" => "function(params){
                            console.log(params.selectedRowIndex);
                            var proj_id=params.table.fg[params.selectedRowIndex]['c'][4].v;

                            var url1 = document.referrer;
                            const url = new URL(url1)
                            console.log(url);
                            top.window.location.href = url.origin+'/projectview/'+proj_id;

                        }",
                        )

                    ));
                    ?>
                </div>
                <!-- <div class="row">
                    <div class="col-sm-2"></div>
                                top.window.location.href = url.origin+'/projectview';

                    <div class="col-sm-3">
                        <div class="decoratedLine1 vlabelBold <\?php echo($language == 'en' ? 'decoratedLine1En' :
                            'decoratedLine1Ar') ?>" style="<\?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                            40px;') ?>"><span><\?php echo "BAC" ?></span></div>
                    </div>
                    <div class="col-sm-3">
                        <div class="decoratedLine1 vlabelBold <\?php echo($language == 'en' ? 'decoratedLine1En' :
                            'decoratedLine1Ar') ?>" style="<\?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                            40px;') ?>"><span><\?php echo "remaining" ?></span></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2"></div>

                    <div class="col-sm-3">
                        <div
                            class="decoratedLine2 vlabelBold <\?php echo($language == 'en' ? 'decoratedLine2En' : 'decoratedLine2Ar') ?>"
                            style="<\?php echo($language == 'en' ? 'padding-left: 40px;' :
                                'padding-right: 40px;') ?>"><span><\?php echo "EAC"; ?></span></div>
                    </div>
                    <div class="col-sm-3">
                        <div class="decoratedLine2 vlabelBold <\?php echo($language == 'en' ? 'decoratedLine2En' :
                            'decoratedLine2Ar') ?>" style="<\?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                            40px;') ?>"><span><\?php echo "ETC" ?></span></div>
                    </div>
                </div> -->
                <br>
            </div>

            <br>
            <br>

        </div>

    </div>

    <script type="text/javascript">
        KoolReport.load.onDone(function () {
            var locale = '<?php echo $language;?>';

            var table = $('#example').DataTable({
                destroy: true,
                "pageLength": 50,
                "language": {
                    "sProcessing": locale == 'ar' ? "جارٍ التحميل..." : "Processing...",
                    "sLengthMenu": locale == 'ar' ? "اعرض _MENU_ سجلات" : "Show _MENU_ entries",
                    "sZeroRecords": locale == 'ar' ? "لم يعثر على أية سجلات" : "No matching records found",
                    "sInfo": locale == 'ar' ? "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل" : "Showing _START_ to _END_ of _TOTAL_ entries",
                    "sInfoEmpty": locale == 'ar' ? "يعرض 0 إلى 0 من أصل 0 سجل" : "Showing 0 to 0 of 0 entries",
                    "sInfoFiltered": locale == 'ar' ? "(منتقاة من مجموع _MAX_ مُدخل)" : "(filtered from _MAX_ total entries)",
                    "sInfoPostFix": "",
                    "sSearch": locale == 'ar' ? "ابحث:" : "Search:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": locale == 'ar' ? "الأول" : "First",
                        "sPrevious": locale == 'ar' ? "السابق" : "Last",
                        "sNext": locale == 'ar' ? "التالي" : "Next",
                        "sLast": locale == 'ar' ? "الأخير" : "Previous",
                    },
                    // buttons: {
                    //     colvisRestore: locale == 'ar' ? " إعادة إظهار الخانات" : "restore"
                    // }
                },
                // "buttons": [

                //     {
                //         extend: 'print',
                //         text: '<i style="color:#a9a9a9;" class="fa fa-print"></i>',
                //         titleAttr: 'طباعة',
                //         autoPrint: true,
                //         cssClass: 'printButton',
                //         exportOptions: {
                //             columns: ':visible',
                //         },
                //         customize: function (win) {
                //             $(win.document.body).find('pivot').addClass('display').css('font-size', '9px');
                //             // $(win.document.body).find('table').addClass('display').css('direction', '<?php echo $language == 'en' ? "ltr" : "rtl";?>');
                //             // $(win.document.body).find('tr:nth-child(odd) td').each(function (index) {
                //             //     $(this).css('background-color', '#D0D0D0');
                //             // });
                //             // $(win.document.body).find('h1').css('text-align', 'center');

                //             // $(win.document.body).prepend('<div style="text-align:center;"><?php echo $sector11; ?></div>'); //before the table
                //             // $(win.document.body).find('th').css('background-color', '#2f353a');
                //             // $(win.document.body).find('th').css('color', '#fff');
                //         }
                //     },
                // ],
                // responsive: true,
                "columnDefs": [{
                    "searchable": true,
                    "orderable": true,
                    "targets": 0
                },

                    {
                        'visible': false,
                        'targets': [12],
                        // className: 'noVis'
                    },
                    {
                        'visible': false,
                        'targets': [13],
                        // className: 'noVis'
                    },
                    {
                        'visible': false,
                        'targets': [14],
                        // className: 'noVis'
                    },
                    {
                        'visible': false,
                        'targets': [15],
                        // className: 'noVis'
                    },
                ],
                "scrollX": true,

                initComplete: function () {
                    this.api().columns([12, 13, 14]).every(function () {
                        //var selectField = 'title'+i;

                        var column = this;
                        var title = $(column.header()).text().replace(/[\s()]+/gi, '');
                        console.log(title);
                        var select = $('#' + title)


                            .on('change', function () {
                                // var val = $.fn.dataTable.util.escapeRegex(
                                //     $(this).val()
                                // );
                                console.log('hi');
                                ;

                                var data = $.map($(this).select2('data'), function (value, key) {
                                    console.log(value);
                                    return value.text ? '^' + $.fn.dataTable.util.escapeRegex(value.id) + '$' : null;
                                });

                                //if no data selected use ""
                                if (data.length === 0) {
                                    data = [""];
                                }
                                console.log(data)

                                //join array into string with regex or (|)
                                var val = data.join('|');
                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });


                    });


                }
            });
            $('#project_cat').on('change', function () {
                console.log("completecheck");
                $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {


                        var complete = $('#project_cat').val();
                        console.log(complete);
                        var spark = parseInt(data[15]);
                        // console.log(spark+"<="+complete+"----"+(spark<=complete));
                        if (complete.length > 0) {
                            if (spark == (complete - 1)) {
                                console.log(complete);

                                return true;
                            }
                            return false;
                        } else
                            return true;
                    }
                );


                table.draw();
            });

            // table.columns( [13,14,15,16] ).visible( false );
            table.buttons().container().appendTo($('#button1'));


        });

        function myFunction() {
            var x = document.getElementById("myDIV");
            if (x.style.display === "none") {
                $(x).show('slow');
            } else {
                $(x).hide('slow');
            }
        };

        function printDiv() {
            var divElements = document.getElementById('pivot').innerHTML;
            var charts = document.getElementById('charts1').innerHTML;

            var heading = "<?php echo $translation = ($language == 'en' ? $transtable->where('key_name', 'title')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'title')->where('key_pos', 'project_status_report')->get(0, "value_ar")); ?>";
            var siz = "";
            var test = '<\?php echo $language;?>';
            if (test == 'en') {
                lang = 'ltr';
            } else {
                lang = 'rtl';
            }


            var table = $('#example').DataTable()
            var heading = "<?php echo $translation = ($language == 'en' ? $transtable->where('key_name', 'title')->where('key_pos', 'project_status_report')->get(0, "value_en") : $transtable->where('key_name', 'title')->where('key_pos', 'project_status_report')->get(0, "value_ar")); ?>";
            var tableTag = "<table id=\"example1\" class=\"table table-striped table-bordered color no-footer dataTable\" role=\"grid\" aria-describedby=\"example_info\" style=\"width: 1234px;\">";
            var thead = table.table().header().outerHTML;
            var rows = table.rows({search: 'applied'}).nodes();
            var rowStr = "";
            for (var i = 0; i < rows.length; i++)
                rowStr += rows[i].outerHTML;

            // $('a').each(function() {
            //  $(this).data('href', $(this).attr('href')).removeAttr('href');
            //    });
            var datatest = tableTag + thead + rowStr//$('#example').prop('outerHTML')//$('#example').wrapAll('<div>').parent().html();
            data1 = "<html dir=\"rtl\" lang=\"ar\"><head><meta charset=\"utf-8\"> <title>" + heading + "</title><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'></head><body><div style=\"text-align:center\"><h1>" + heading + "</h1></div>" + datatest + "</table></body></html>";

            data2 = "<div><h4 style=\"text-align:center;font-size:9px;border: 0px !important;\">" + heading + "</h4><div><div>" + datatest + "</table></div><br><br><div style=\"text-align:center;\">" + charts + "</div></div></div>";

            //    data2="<div><h4 style=\"text-align:center;font-size:9px;border: 0px !important;\">" + heading + "</h4>" +  divElements + "</div>";


            document.getElementById('printarea').style.display = "block";

            document.getElementById('printarea').innerHTML = data2;
            document.getElementById('printissue').style.display = "none";

            window.print(); // call print
            console.log(data2)
            document.getElementById('printissue').style.display = "block";
            document.getElementById('printarea').style.display = "none";


        }

        function linktokpi() {
            var m = $('#sector').val()
            var n = $('#section').val()
            console.log(n);

            // history.replaceState('data to be passed', 'NAJAH', '/kpilist');
            // document.cookie = "sectorname="+m+"; path=/; ";
            document.cookie = "sectorname=" + m + ";domain=.najah.online; path=/; ";


            if (n != null)

                document.cookie = "orgname=" + n + ";domain=.najah.online; path=/; ";
            //document.cookie = "orgname="+n+"; path=/; ";
            //    top.window.location.href= "http://localhost:8080/kpilist";
            top.window.location.href = "https://dev.najah.online/kpilist";


        };
    </script>
    <style>
        .container1 {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
            display: inline-flex;
            column-width: 100%;
            padding: 0px 0px;
            background: #fff;
        }

        .buttons-print {
            background-color: #ffffff;
            border: none;
            /*color:black;*/
        }

        button.dt-button, div.dt-button, a.dt-button, a.dt-button:focus {
            border: none !important;
            background-color: #ffffff;
            background: none;
            padding: 0;
        }


        div.dt-button-collection {

            /* top: 19.6166px; */
            left: -98.433px;
            transform: translateX(-98px);
        }

        /* .card-body {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        } */
        .select2 {
            width: 100% !important;
            /*border: 1px solid #e8e8e8;*/
            min-height: 40px;
        }

        button.dt-button {
            font-size: 0.68em;
        }

        /* .select2:after{
            content: '';
            position:absolute;
            left:10px;
            top:15px;
            width:0;
            height:0;
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-top: 5px solid #888;
            direction: rtl;
        }; */

        @media print {

            #pivot {

            }

            body {
                transform: <?php echo $language=='ar'?'scale(0.5) translate(150px,-300px)':'scale(0.7) translate(-150px,-150px)' ;?>;

            }

        }
    </style>
</div>
</div>
</body>
</html>
