<?php

use \koolreport\widgets\koolphp\Table;

// use \koolreport\processes\CalculatedColumn;
use \koolreport\inputs\BSelect;
use \koolreport\inputs\Select;
use \koolreport\processes\Sort;
use \koolreport\inputs\Select2;
use \koolreport\inputs\RadioList;
use \koolreport\inputs\CheckBoxList;
use \koolreport\pivot\widgets\PivotTable;
use Modules\ClientApp\Reports\TopWorthProjectReport;
use \koolreport\processes\Filter;

$language = '';
if (isset($this->params['language']) && !empty($this->params['language'])) {
    $language = $this->params['language'];
}
//-------------------------generating datastore for section------------------------------
if ($this->params["sector"] != null) {

    // ----------------------------------------selecting roles corresponding to this sector--------------------------------------------

    if ($this->datastore('section111')->count() > 0) {
        if ($this->params["oid"] != "null") {

            $role_section_id = $this->dataStore("section111")->only("id")->data();
            $j = 0;
            foreach ($role_section_id as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v != NULL) {
                        // echo "Key=" . $k . ", Value=" . $v;
                        // echo "<br>";
                        $r_section[$j] = $v;
                        $j = $j + 1;
                    }
                }
            }
            $store = [];


            foreach ($r_section as $key => $v) {
                $section_name = "sect" . $v;
                $store = array_merge($store, $this->dataStore($section_name)->data());
            }
        }
    }

    if ($this->datastore('section222')->count() > 0) {
        if ($this->params["oid"] != "null") {
            $store = $this->dataStore("section222")
                ->filter(function ($row) {
                    return $row["id"] != NULL;
                });

        }
    }

    if ($this->params["oid"] == "null") {
        $store = $this->dataStore("section1")
            ->filter(function ($row) {
                return $row["id"] != NULL;
            });

    }
}
//----------------------------------------------------------------------------------
$transtable = $this->dataStore('translation');

?>

<!DOCTYPE html>
<?php if ($language == 'ar')
    $dir = "rtl";

else
    $dir = "ltr";
?>


<html dir="<?php echo $dir; ?>">
<!-- <html dir="rtl" onchange="console.log('value of name field is changed')"> -->
<head>
    <meta charset="utf-8">
    <title>
    </title>
    <!-- <link rel='stylesheet' href='../../../assets/font-awesome/css/font-awesome.min.css'> -->
    <!-- <script src="../../../public/koolreport_assets/PivotTable.js"></script>  -->

    <style>

        /*.table-bordered >tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th{    border: 0px !important;
             /* /* solid     #ddd;
        } */
        #form2 {
            display: inline;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox  */
        input[type=number] {
            -moz-appearance: textfield;
        }


        .fa1 i {
            visibility: hidden !important;
            /* text-align: <\?php echo $language=='ar'?'right':'left' ;?>;  */

            /* opacity:0; */
        }

        .pivot-row-header-text {
            text-align: <?php echo $language=='ar'?'right':'left' ;?>;

        }

        .pivot-data-field-zone {
            display: visible;
        }

        /* .pivot-row-header-total{
           border: 0px !important;
                   }


                   .pivot-column-header-total, .pivot-data-cell-column-total, .pivot-data-header-total {
                       border: 0px !important;
                   }*/

        .table-bordered {
            border: 0px !important;
        }

        /* .pivot-column{
            border:0px;
        } */
        .select2 {
            width: 100% !important;
            min-height: 40px;
        }

        @media print {
            /* .table {
                /* font-size: 50%;
                transform: <\?php echo $language=='ar'?'scale(0.7) translate(205px,50px); ':"" ;?>;
                /* margin-right:20px;
                padding-right:20px;
            } */
            table td {
                padding-left: 5px;
                padding-right: 5px;
            }

            .pivot-row-header-text i {
                visibility: hidden !important;
            }


        }
    </style>
<body>
<p id="printarea"></p>
<div id="printissue">
    <div
        style="background-color:#ffffff;margin-left:10px;margin-right:5px;margin-top:0px;margin-bottom:30px;padding-top:30px;padding-right:10px;padding-left:10px;">
        <h4 class="mb-0 pt-2" style="text-align:center;color: #20a8d8;font-size: 20px;font-weight: normal;">
            <?php $textbit = 'title';
            $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
            echo $translation ?>

        </h4>
        <scan class="form-group"
              style="float:<?php echo $language == 'ar' ? 'left' : 'right'; ?>;padding-right:150px;padding-left:150px;">
            <script> document.write(new Date().toDateString()); </script>
        </scan>

        <?php if ($language == 'ar') {
            $dir = "rtl";
            $lin = "left";
        } else {
            $dir = "ltr";
            $lin = "right";
        }
        ?>
        <div style="background-color:#ffffff;border: 1px solid #a5aeb7;position:relative;" class="col-md-12"
             style="float:right;">
            <!-- <div class="col-md-10"></div> -->
            <!-- <span id="backlink1" style="display:block;">
        <button onClick="linktokpi();" style="float:left;border:none;background-color: #ffffff;"><i
                style="padding-top:10px;color:#a9a9a9;font-size:12px;" class="fa fa-arrow-left "></i></button>
        </span> -->
            <div id="button1" dir="rtl" style="float:<?php echo $lin; ?>">
                <button onclick="myFunction()" style="float:left;border:none;background-color: #ffffff;"><i
                        style="padding-top:5px;color:#a9a9a9;" class="fa fa-angle-up 4x" title="toggle"></i></button>
                <form id="form2" method="post">
                    <input type="hidden" id="radiolist1" name="radiolist1"
                           value="<?php echo($this->params["radiolist"]); ?>">
                    <input type="hidden" id="sector1" name="sector1" value="<?php echo($this->params["sector"]); ?>">
                    <input type="hidden" id="section1" name="section1" value="<?php echo($this->params["section"]); ?>">
                    <input type="hidden" id="mtp1" name="mtp1" value="<?php echo($this->params["mtp"]); ?>">
                    <input type="hidden" id="operational_status1" name="operational_status1"
                           value="<?php echo implode(',', $this->params["operational_status"]); ?>">
                    <input type="hidden" id="project_type1" name="project_type1"
                           value="<?php echo implode(',', $this->params["project_type"]); ?>">
                    <input type="hidden" id="project_cat1" name="project_cat1"
                           value="<?php echo($this->params["project_cat"]); ?>">
                    <input type="hidden" id="top_performing1" name="top_performing1"
                           value="<?php echo($this->params["top_performing"]); ?>">
                    <input type="hidden" id="value_gt1" name="value_gt1"
                           value="<?php echo($this->params["value_gt"]); ?>">
                    <input type="hidden" id="value_lt1" name="value_lt1"
                           value="<?php echo($this->params["value_lt"]); ?>">


                    <button class="form-group" onClick="javascript:printDiv()"
                            style="float:left;border:none;background-color: #ffffff;"><i
                            style="padding-top:5px;color:#a9a9a9;font-size:12px" class="fa fa-print" title="print"></i>
                    </button>
                    <!-- <button class="form-group" onClick="javascript:expandPivot()" name="expand" value=2 style="float:left;border:none;background-color: #ffffff;"><i style="padding-top:5px;color:#a9a9a9;font-size:12px" class="fa fa-plus-square" title="expand"></i></button> -->
                    <button class="form-group" onClick="javascript:expandPivot()" name="expand" value="<?php echo
                    ($this->params['expand'] == 1) ? '2' : '1' ?>"
                            style="float:left;border:none;background-color: #ffffff;"><i
                            style="padding-top:5px;color:#a9a9a9;font-size:12px" class="<?php echo
                        ($this->params['expand'] == 1) ? 'fa fa-plus-square' : 'fa fa-minus-square' ?>"
                            title="expand"></i></button>
                </form>

            </div>
        </div>
        <br/>
        <?php $style = "";
        if (empty($_POST['sector']) && (empty($_POST['section'])) && (empty($_POST['mtp'])) && (empty($_POST['periodicity'])) && (empty($_POST['kpi_category'])) && (empty($_POST['kpi_activation_status'])) && (empty($_POST['status1']))) {
            // if(empty($_POST['filter'])){
            $style = 'display:none !important;';
        } else {
            $style = 'display:block !important;';
        } ?>

        <div id="myDIV" dir="<?php echo $language == 'ar' ? "rtl" : "ltr"; ?>"
             style="overflow:auto;width:100%;background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;<?php echo $style; ?>">
            <form id="form1" method="post">
                <!-- @method('PUT') -->
                <!-- <input type="hidden" id="expand1" name="expand1" value="<\?php echo $this->params["expand"]; ?>">  -->
                <?php $expVal = 0;
                if ($this->params['expand'] == 1) {
                    $expVal = $this->params['expand'];
                } elseif ($this->params['expand'] == 2) {
                    $expVal = $this->params['expand'];
                }


                if ($language == 'ar') {
                    $dir = "rtl";
                    $lin = "right";
                } else {
                    $dir = "ltr";
                    $lin = "left";
                }
                ?>
                <input type="hidden" id="expand1" name="expand1" value="<?php echo $expVal; ?>">
                <input type="hidden" id="first_time" name="first_time" value=1>
                <div class="col-md-4 form-group" style="float:<?php echo $lin; ?>">
                    <strong>
                        <!-- sector  -->
                        <?php $textbit = 'sector';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php
                    select2::create(array(
                        // "multiple"=>false,
                        "name" => "sector",
                        "defaultOption" => array("--" => ""),
                        "dataStore" => $this->dataStore("sector1"),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){
                        $('#section').val(null).trigger('change');

                        $('#form1').submit();
                    }"
                        )
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float: <?php echo $lin; ?>">
                    <strong>
                        <!-- section  -->
                        <?php $textbit = 'section';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php

                    Select2::create(array(
                        // "multiple"=>true,
                        "name" => "section",
                        "defaultOption" => array("--" => ""),
                        "dataStore" => $store,
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){
                        $('#form1').submit();
                    }"
                        )

                    ));
                    ?>
                </div>

                <div class="col-md-4 form-group" style="float: <?php echo $lin; ?>">
                    <strong>

                        <?php $textbit = 'mtp';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php

                    Select2::create(array(
                        // "multiple"=>true,
                        "name" => "mtp",
                        // "defaultOption" => array("--" => ""),
                        "dataStore" => $this->dataStore("mtp1"),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){
                        $('#form1').submit();
                    }"
                        ),

                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float:<?php echo $language == 'ar' ? "right" : "left"; ?>;">

                    <strong>
                        <!-- operational_status -->
                        <?php $textbit = 'status_operational';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                    <?php
                    select2::create(array(
                        "multiple" => true,
                        "name" => "operational_status",
                        "defaultOption" => array("--" => ""),
                        "data" => array(
                            $language == 'en' ? $transtable->where('key_name', 'Planning')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'planning')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 1,
                            $language == 'en' ? $transtable->where('key_name', 'Progressing')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'progressing')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 2,
                            $language == 'en' ? $transtable->where('key_name', 'On_hold')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'on_hold')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 3,
                            $language == 'en' ? $transtable->where('key_name', 'closed')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'closed')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 4,


                        ),
                        // "attributes" => array(
                        //     "class" => "col-md-4 form-control"
                        // ),
                        "clientEvents" => array(
                            "change" => "function(params){

                                $('#form1').submit();


                        }"
                        ),
                    ));
                    ?>
                </div>

                <div class="col-md-4 form-group" style="float:<?php echo $language == 'ar' ? "right" : "left"; ?>;">
                    <strong></strong>
                    <div dir="ltr">
                        <?php
                        RadioList::create(array(
                            "name" => "radiolist",
                            "data" => array(
                                $language == 'en' ? $transtable->where('key_name', 'performing')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'performing')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 1,
                                $language == 'en' ? $transtable->where('key_name', 'nonperforming')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'nonperforming')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 2,
                                $language == 'en' ? $transtable->where('key_name', 'top_perf_non_perf')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'top_perf_nonperf')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 3,

                            ),
                            "clientEvents" => array(
                                "change" => "function(params){
                            $('#form1').submit();
                        }"
                            ),
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-4 form-group" style="float:<?php echo $language == 'ar' ? "right" : "left"; ?>;">
                    <strong>
                        <!-- top_performing_greater_than -->
                        <?php $textbit = 'top_performing_greater_than';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>

                    <input class="col-md-4 form-control" type="number" id="value_gt" name="value_gt" min="80"
                           max="100" value="<?php echo $this->params["value_gt"]; ?>">

                </div>

                <div class="col-md-4 form-group" style="float:<?php echo $language == 'ar' ? "right" : "left"; ?>;">
                    <strong>
                        <!-- top_nonperforming_less_than -->
                        <?php $textbit = 'top_nonperforming_less_than';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>

                    <input class="col-md-4 form-control" type="number" id="value_lt" name="value_lt" min="0"
                           max="50" value="<?php echo $this->params["value_lt"]; ?>">

                </div>

                <div class="col-md-4 form-group" style="float:<?php echo $language == 'ar' ? "right" : "left"; ?>;">
                    <strong>
                        <!-- no_of_records -->
                        <?php $textbit = 'no_of_records';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                    <?php
                    select2::create(array(
                        // "multiple"=>true,
                        "name" => "top_performing",

                        "data" => array(
                            "5" => 5,
                            "10" => 10,
                            "15" => 15,

                        ),

                        "clientEvents" => array(
                            "change" => "function(params){
                            $('#form1').submit();
                        }"
                        ),
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float:<?php echo $language == 'ar' ? "right" : "left"; ?>;">
                    <strong>
                        <!-- project_category -->
                        <?php $textbit = 'project_category';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                    <?php
                    select2::create(array(
                        // "multiple"=>true,
                        // "multiple" => true,
                        "name" => "project_cat",
                        // "dataStore" => $this->dataStore("project_"),
                        "defaultOption" => array("--" => ""),
                        "data" => array(
                            $language == 'en' ? $transtable->where('key_name', 'ministry')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'ministry')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 1,
                            $language == 'en' ? $transtable->where('key_name', 'development')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'development')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar") => 2,
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),


                        "clientEvents" => array(
                            "change" => "function(params){
                            $('#form1').submit();
                        }"
                        ),
                    ));
                    ?>
                </div>

                <div class="col-md-4 form-group" style="float:<?php echo $language == 'ar' ? "right" : "left"; ?>;">
                    <strong>
                        <!-- project_type -->
                        <?php $textbit = 'project_type';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'top_worth_project_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                    <?php
                    select2::create(array(
                        "multiple" => true,
                        "name" => "project_type",

                        "dataStore" => $this->dataStore("project_type"),
                        "defaultOption" => array("--" => ""),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){
                            $('#form1').submit();


                        }"
                        ),
                    ));
                    ?>
                </div>
            </form>
        </div>

        <!-- <div class="card"> -->
        <div id="pivot"
             style="overflow:auto;width:100%;background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:10px;padding-right:10px;">


            <?php

            if ($this->params["radiolist"] === "1") {

                PivotTable::create(array(
                    //    'dataStore'=>($this->params["radiolist"]=="1"?$this->datastore('performing'):($this->params["radiolist"]=="2"?$this->datastore('nonperforming'):($this->params["radiolist"]=="3"?$this->datastore('user_details'):""))),
                    'dataStore' => $this->datastore('performing'),//$datastore,
                    'rowDimension' => 'row',//$this->datastore('performing'),//

                    'measures' => array(
                        'perf_name',
                        'org_unit_name',
                        'proj_name',
                        'progress_val',
                        'proj_status_name',
                        'proj_perf',

                    ),
                    'rowSort' => array(
                        'proj_perf1' => 'asc',
                    ),
                    //  'rowSort' => array(
                    //      'kpi_perf' => 'desc',
                    //   ),

                    // 'rowSort' => array(
                    //     'kpi_perf' => function($a, $b) {
                    //         // var_dump(hello);
                    //         return (int)$a < (int)$b;
                    //     },
                    // 'orderDay' => 'asc'
                    // ),
                    'map' => array(
                        'rowHeader' => function ($rowHeader, $headerInfo) {
                            $v = $rowHeader;
                            if (isset($headerInfo['childOrder']))
                                $v = $headerInfo["fieldName"] == "id" ? substr($headerInfo['childOrder'], -1, 1) : $v;
                            return $v;
                        },
                        'dataCell' => function ($value, $cellInfo) {
                            if ($cellInfo["fieldName"] === "proj_perf") {
                                $cellInfo["formattedValue"] = $value . "%";
                                // }else if ($cellInfo["fieldName"] === "hours"){
                                // $cellInfo["formattedValue"] => round($value,0);
                            }
                            return $cellInfo["formattedValue"];
                        }
                    ),
                    'hideTotalRow' => true,
                    'hideTotalColumn' => true,
                    'hideSubtotalRow' => true,
                    'hideSubtotalColumn' => true,
                    //    'showDataHeaders'=>true,
                    'rowCollapseLevels' => $this->params['expand'] == 2 || (isset($_POST['expand1']) &&
                        $_POST['expand1'] == 2) ?
                        array(4) : array(1),

                    'headerMap' => array(
                        'perf_name' => ($language == 'en' ? $transtable->where('key_name', 'perf_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'perf_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),
                        'org_unit_name' => ($language == 'en' ? $transtable->where('key_name', 'org_unit_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'org_unit_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Communicator Name',
                        'proj_name' => ($language == 'en' ? $transtable->where('key_name', 'proj_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Email',
                        'progress_val' => ($language == 'en' ? $transtable->where('key_name', 'progress_val')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'progress_val')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Phone',
                        'proj_status_name' => ($language == 'en' ? $transtable->where('key_name', 'proj_status_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_status_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Org Unit',
                        'proj_perf' => ($language == 'en' ? $transtable->where('key_name', 'proj_perf')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_perf')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Next Reading Date',

                    ),
                    'cssClass' => array(
                        'rowHeader' => function ($value, $cellInfo) {

                            if (($cellInfo['fieldName'] === "proj_name") || ($cellInfo['fieldName'] === "progress_val") || ($cellInfo['fieldName'] === "proj_status_name") || ($cellInfo['fieldName'] === "proj_perf"))
                                // <script>$(this).find('.fa-circle').removeClass('fa-circle')</script>

                                return "fa1";
                        }
                    ),

                ));
            } else if ($this->params["radiolist"] === "2") {
                // var_dump("nonperforming");
                //$datastore=$this->datastore('nonperforming');
                PivotTable::create(array(
                    //    'dataStore'=>($this->params["radiolist"]=="1"?$this->datastore('performing'):($this->params["radiolist"]=="2"?$this->datastore('nonperforming'):($this->params["radiolist"]=="3"?$this->datastore('user_details'):""))),
                    'dataStore' => $this->datastore('nonperforming'),//$datastore,
                    'rowDimension' => 'row',// $this->datastore('performing'),//
                    //    'rowSort' => array(
                    //     'kpi_perf' => 'desc'
                    //     ),

                    'measures' => array(
                        // 'dollar_sales - sum',
                        'perf_name',
                        'org_unit_name',
                        'proj_name',
                        'progress_val',
                        'proj_status_name',
                        'proj_perf',

                        //  'proj_perf1',
                    ),

                    // 'rowSort' => array(
                    //     'proj_perf1 - sum' => 'asc',
                    // ),
                    //  'rowSort' => array(
                    //      'kpi_perf' => 'desc',
                    //   ),

                    // 'rowSort' => array(
                    //     'kpi_perf' => function($a, $b) {
                    //         // var_dump(hello);
                    //         return (int)$a < (int)$b;
                    //     },
                    // 'orderDay' => 'asc'
                    // ),
                    'map' => array(
                        'rowHeader' => function ($rowHeader, $headerInfo) {
                            $v = $rowHeader;
                            if (isset($headerInfo['childOrder']))
                                $v = $headerInfo["fieldName"] == "id" ? substr($headerInfo['childOrder'], -1, 1) : $v;
                            return $v;


                        },
                        'dataCell' => function ($value, $cellInfo) {
                            if ($cellInfo["fieldName"] === "proj_perf") {
                                $cellInfo["formattedValue"] = round($value, 0) . "%";
                                // }else if ($cellInfo["fieldName"] === "hours"){
                                // $cellInfo["formattedValue"] => round($value,0);
                            }
                            return $cellInfo["formattedValue"];
                        }
                    ),
                    'hideTotalRow' => true,
                    'hideTotalColumn' => true,
                    'hideSubtotalRow' => true,
                    'hideSubtotalColumn' => true,
                    //    'showDataHeaders'=>true,
                    'rowCollapseLevels' => $this->params['expand'] == 2 || (isset($_POST['expand1']) &&
                        $_POST['expand1'] == 2) ?
                        array(4) : array(1),

                    'headerMap' => array(
                        'perf_name' => ($language == 'en' ? $transtable->where('key_name', 'perf_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'perf_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),
                        'org_unit_name' => ($language == 'en' ? $transtable->where('key_name', 'org_unit_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'org_unit_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Communicator Name',
                        'proj_name' => ($language == 'en' ? $transtable->where('key_name', 'proj_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Email',
                        'progress_val' => ($language == 'en' ? $transtable->where('key_name', 'progress_val')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'progress_val')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Phone',
                        'proj_status_name' => ($language == 'en' ? $transtable->where('key_name', 'proj_status_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_status_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Org Unit',
                        'proj_perf' => ($language == 'en' ? $transtable->where('key_name', 'proj_perf')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_perf')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Next Reading Date',

                    ),
                    'cssClass' => array(
                        'rowHeader' => function ($value, $cellInfo) {

                            if (($cellInfo['fieldName'] === "proj_name") || ($cellInfo['fieldName'] === "progress_val") || ($cellInfo['fieldName'] === "proj_status_name") || ($cellInfo['fieldName'] === "proj_perf"))
                                // <script>$(this).find('.fa-circle').removeClass('fa-circle')</script>

                                return "fa1";
                        }
                    ),


                ));
            } else if ($this->params["radiolist"] === "3") {


                PivotTable::create(array(
                    //    'dataStore'=>($this->params["radiolist"]=="1"?$this->datastore('performing'):($this->params["radiolist"]=="2"?$this->datastore('nonperforming'):($this->params["radiolist"]=="3"?$this->datastore('user_details'):""))),
                    'dataStore' => $this->datastore('combined'),//$datastore,
                    'rowDimension' => 'row',//$this->datastore('performing'),//
                    //    'rowSort' => array(
                    //     //  'sector_name'=>'desc',
                    //     // 'perf_name'=>'desc',

                    //     'proj_perf1' => 'asc',

                    //     ),

                    'measures' => array(
                        // 'dollar_sales - sum',
                        'perf_name',
                        'org_unit_name',
                        'proj_name',
                        'progress_val',
                        'proj_status_name',
                        'proj_perf',
                        // 'unit_name',
                        // 'proj_perf1 - sum',
                        //    'kpi_perf-count',
                    ),
                    'rowSort' => array(
                        'perf_name' => 'desc',
                        // 'proj_perf1-sum'=>'asc',

                        'proj_perf1' => 'asc',

                        // 'proj_perf1 - sum' => 'asc',


                    ),

                    //  'rowSort' => array(
                    //      'kpi_perf' => 'desc',
                    //   ),

                    // 'rowSort' => array(
                    //     'kpi_perf' => function($a, $b) {
                    //         // var_dump(hello);
                    //         return (int)$a < (int)$b;
                    //     },
                    // 'orderDay' => 'asc'
                    // ),
                    'map' => array(
                        'rowHeader' => function ($rowHeader, $headerInfo) {
                            $v = $rowHeader;
                            if (isset($headerInfo['childOrder']))
                                $v = $headerInfo["fieldName"] == "id" ? substr($headerInfo['childOrder'], -1, 1) : $v;
                            return $v;
                        },
                        'dataCell' => function ($value, $cellInfo) {
                            if ($cellInfo["fieldName"] === "proj_perf") {
                                $cellInfo["formattedValue"] = $value . "%";
                                // }else if ($cellInfo["fieldName"] === "hours"){
                                // $cellInfo["formattedValue"] => round($value,0);
                            }
                            return $cellInfo["formattedValue"];
                        }
                    ),
                    'hideTotalRow' => true,
                    'hideTotalColumn' => true,
                    'hideSubtotalRow' => true,
                    'hideSubtotalColumn' => true,
                    'showDataHeaders' => true,
                    'rowCollapseLevels' => $this->params['expand'] == 2 || (isset($_POST['expand1']) &&
                        $_POST['expand1'] == 2) ?
                        array(5) : array(1),

                    'headerMap' => array(
                        'perf_name' => ($language == 'en' ? $transtable->where('key_name', 'perf_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'perf_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),
                        'org_unit_name' => ($language == 'en' ? $transtable->where('key_name', 'org_unit_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'org_unit_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Communicator Name',
                        'proj_name' => ($language == 'en' ? $transtable->where('key_name', 'proj_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Email',
                        'progress_val' => ($language == 'en' ? $transtable->where('key_name', 'progress_val')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'progress_val')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Phone',
                        'proj_status_name' => ($language == 'en' ? $transtable->where('key_name', 'proj_status_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_status_name')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Org Unit',
                        'proj_perf' => ($language == 'en' ? $transtable->where('key_name', 'proj_perf')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_perf')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Next Reading Date',

                    ),
                    'cssClass' => array(
                        'rowHeader' => function ($value, $cellInfo) {

                            if (($cellInfo['fieldName'] === "proj_name") || ($cellInfo['fieldName'] === "progress_val") || ($cellInfo['fieldName'] === "proj_status_name") || ($cellInfo['fieldName'] === "proj_perf"))
                                // <script>$(this).find('.fa-circle').removeClass('fa-circle')</script>

                                return "fa1";
                        }
                    ),

                ));
            }

            // PivotTable::create(array(
            //     // 'dataStore' => ($this->params["status1"] == 1 ? $this->datastore('outdated') : ($this->params["status1"] == 2 ? $this->datastore('updated') : $this->datastore('user_details'))),
            //     'dataStore' => $this->datastore('user_details'),
            //     'rowDimension' => 'row',
            //     'measures' => array(
            //         // 'dollar_sales - sum',
            //         'org_unit_name',
            //         'proj_name',
            //         'progress_val',
            //         'proj_status_name',
            //         'proj_perf',

            //     ),
            //     //   'map' => array(
            //     //     'dataHeader' => function($dataField, $fieldInfo) {
            //     //          $v = 1;//$fiel["fieldName"];
            //     //         return $v;

            //     //     }),

            //     'rowCollapseLevels' => $this->params['expand'] == 2 || (isset($_POST['expand1']) && $_POST['expand1'] == 2) ? array(7) : array(0, 3),
            //     'hideTotalRow' => true,
            //     'hideTotalColumn' => true,
            //     'hideSubtotalRow' => true,
            //     'hideSubtotalColumn' => true,
            //     'showDataHeaders' => true,
            //     'headerMap' => array(
            // 'org_unit_name' => ($language == 'en' ? $transtable->where('key_name', 'user_of_contact')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'user_of_contact')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Communicator Name',
            // 'proj_name' => ($language == 'en' ? $transtable->where('key_name', 'contact_email')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'contact_email')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Email',
            // 'progress_val' => ($language == 'en' ? $transtable->where('key_name', 'contact_phone_internal')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'contact_phone_internal')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Phone',
            // 'proj_status_name' => ($language == 'en' ? $transtable->where('key_name', 'org_unit')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'org_unit')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Org Unit',
            // 'proj_perf' => ($language == 'en' ? $transtable->where('key_name', 'kpi_symbol')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'kpi_symbol')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")),//'Next Reading Date',

            //     ),


            //     'cssClass' => array(
            //         'rowHeader' => function ($value, $cellInfo) {

            //             if (($cellInfo['fieldName'] === "proj_name") || ($cellInfo['fieldName'] === "progress_val") || ($cellInfo['fieldName'] === "proj_status_name") || ($cellInfo['fieldName'] === "proj_perf"))
            //                 // <script>$(this).find('.fa-circle').removeClass('fa-circle')</script>

            //                 return "fa1";
            //         }
            //     ),

            // ));

            ?>
        </div>
    </div>
    <script type="text/javascript">
        KoolReport.load.onDone(function () {
            // var s=$('#inactive');
            $("#value_gt").on('change', function () {
                $('#form1').submit();
                console.log(s.val());

            });

            $("#value_lt").on('change', function () {
                $('#form1').submit();
                console.log(s.val());

            });
        });

        function myFunction() {
            var x = document.getElementById("myDIV");
            if (x.style.display === "none") {
                $(x).show('slow');
            } else {
                $(x).hide('slow');
            }
        };

        //
        function linktokpi() {
            var m = $('#sector').val()
            var n = $('#section').val()
            console.log(n);

            // history.replaceState('data to be passed', 'NAJAH', '/kpilist');

            //document.cookie = "sectorname="+m+"; path=/; ";
            document.cookie = "sectorname=" + m + ";domain=.najah.online; path=/; ";

            if (n != null)
                document.cookie = "orgname=" + n + ";domain=.najah.online; path=/; ";

            //document.cookie = "orgname="+n+"; path=/; ";
//

            var frontlink = localStorage.getItem('front_link');
            const front_link = new URL(frontlink);
            top.window.location.href = front_link.origin + "/kpilist";
            // top.window.location.href= "http://localhost:8080/kpilist";

        };

        function printDiv() {
            var divElements = document.getElementById('pivot').innerHTML;
            var heading = "<?php echo $translation = ($language == 'en' ? $transtable->where('key_name', 'title')->where('key_pos', 'top_worth_project_report')->get(0, "value_en") : $transtable->where('key_name', 'title')->where('key_pos', 'top_worth_project_report')->get(0, "value_ar")); ?>";
            var siz = "";
            var test = '<?php echo $language;?>';
            if (test == 'en') {
                lang = 'ltr';
            } else {
                lang = 'rtl';
            }

            data2 = "<div style=\"text-align:center;font-size:9px;border: 0px !important;\"><h4>" + heading + "</h4>" + divElements + "</div>";


            document.getElementById('printarea').style.display = "block";

            document.getElementById('printarea').innerHTML = data2;
            document.getElementById('printissue').style.display = "none";

            window.print(); // call print

            document.getElementById('printissue').style.display = "block";
            document.getElementById('printarea').style.display = "none";


        }

        function expandPivot() {
            $('#form2').submit();

        }

    </script>

</div>
</body>
</html>
