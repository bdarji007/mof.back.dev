<?php

use \koolreport\widgets\koolphp\Table;
use \koolreport\processes\CalculatedColumn;
use \koolreport\inputs\BSelect;
use \koolreport\inputs\Select;
use \koolreport\processes\Sort;
use \koolreport\inputs\Select2;
use \koolreport\datagrid\DataTables;
use \koolreport\sparklines;
use \koolreport\inputs\DateTimePicker;
use \koolreport\inputs\CheckBoxList;
use Modules\ClientApp\Reports\ProjectRiskReport;
use \koolreport\processes\Filter;

$language = '';
if (isset($this->params['language']) && !empty($this->params['language'])) {
    $language = $this->params['language'];
}

//-------------------------generating datastore for section------------------------------
if ($this->params["sector"] != null) {

    // ----------------------------------------selecting roles corresponding to this sector--------------------------------------------

    if ($this->datastore('section111')->count() > 0) {
        if ($this->params["oid"] != "null") {

            $role_section_id = $this->dataStore("section111")->only("id")->data();
            $j = 0;
            foreach ($role_section_id as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v != NULL) {
                        // echo "Key=" . $k . ", Value=" . $v;
                        // echo "<br>";
                        $r_section[$j] = $v;
                        $j = $j + 1;
                    }
                }
            }
            $store = [];


            foreach ($r_section as $key => $v) {
                $section_name = "sect" . $v;
                $store = array_merge($store, $this->dataStore($section_name)->data());
            }
        }
    }

    if ($this->datastore('section222')->count() > 0) {
        if ($this->params["oid"] != "null") {
            $store = $this->dataStore("section222")
                ->filter(function ($row) {
                    return $row["id"] != NULL;
                });

        }
    }

    if ($this->params["oid"] == "null") {
        $store = $this->dataStore("section1")
            ->filter(function ($row) {
                return $row["id"] != NULL;
            });

    }
}
//----------------------------------------------------------------------------------
$sector_name = $this->dataStore('sector_name');
$sector11 = $sector_name->get(0, "name");
$transtable = $this->dataStore('translation');
//var_dump('hi');
?>

<!DOCTYPE html>
<?php if ($language == 'ar')
    $dir = "rtl";

else
    $dir = "ltr";
?>
<html dir="<?php echo $dir; ?>">
<!-- <html dir="rtl" onchange="console.log('value of name field is changed')"> -->
<head>
    <meta charset="utf-8">
    <title>project risk report
    </title>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

</head>
<style>
    .buttons-print {
        background-color: #ffffff;
        boder: none;
    }

    table {
        width: 100%;
        table-layout: fixed;
    }


    .color {
        border: 0px solid black;
    }

    .insideBorder {
        border: 10px solid white;
    }

    .line {
        width: 1320px;
        border-bottom: 1px solid black;
        position: absolute;
    }

    .dataTables_filter input {
        width: 450px
    }

    .cssHeader {
        background-color: #73818f;
        color: #fff;
        text-align: <?php echo $language=='ar'?'right':'left' ;?>;
        font-size: 12px;
    }

    .cssItem {
        background-color: #fdffe8;
        font-size: 12px;
    }

    .container {
        display: flex;
        width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .container1 {
        display: inline-flex;
        column-width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .header img {
        float: left;
        width: 100px;
        height: 100px;
        background: #555;
    }

    .select {
        margin: 10px 10px 0px 325px;
    }

    .checkbox input[type="checkbox"]
        /* input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"]{ */
    {
        position: relative;
        margin-left: 0px;
        float: right;
        font-weight: 700;
    }

    .checkbox label {

        /* float:right; */
        font-weight: 300;
        text-align: left;
        padding: 10px;
        /* margin-top:10px; */
        margin-bottom: 10px;
        /* margin-left:20px; */

    }

    /* #example_wrapper{
        padding-right:0px;
        padding-left:0px;
    } */
</style>
<body>
<div
    style="background-color:#ffffff;margin-left:10px;margin-right:5px;margin-top:0px;margin-bottom:30px;padding-top:30px;padding-right:10px;padding-left:10px;">
    <h4 class="mb-0 pt-2" style="text-align:center;color: #20a8d8;font-size: 20px;font-weight: normal;">
        <?php $textbit = 'title';
        $translation = $language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_ar");

        echo $translation ?>
    </h4>
    <scan class="form-group"
          style="float:<?php echo $language == 'ar' ? 'left' : 'right'; ?>;padding-right:150px;padding-left:150px;">
        <script> document.write(new Date().toDateString()); </script>
    </scan>

    <?php if ($language == 'ar') {
        $dir = "rtl";
        $lin = "left";
    } else {
        $dir = "ltr";
        $lin = "right";
    }
    ?>
    <div style="background-color:#ffffff;border: 1px solid #a5aeb7;position:relative;" class="col-md-12"
         style="float:right;">
        <div id="button1" dir="rtl" style="float:<?php echo $lin; ?>">
            <button onclick="myFunction();" style="float:left;border:none;background-color: #ffffff;"><i
                    style="padding-top:5px;color:#a9a9a9;;" class="fa fa-angle-up 4x"></i></button>
        </div>
    </div>
    <br/>
    <?php $new = $this->dataStore('project_details') ?>
    <?php $style = "";

    if (empty($_POST)) {
        $style = 'display:none !important;';
    } else {
        $style = 'display:block !important;';
    } ?>
    <?php if ($language == 'ar') {
        $dir = "rtl";
        $lin = "right";
    } else {
        $dir = "ltr";
        $lin = "left";
    }
    ?>

    <div class="col-md-12" id="myDIV"
         style="overflow:auto;width:100%;background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;<?php echo $style; ?>">
        <form id="form1" method="post">
            <div class="col-md-3 form-group" style="float:<?php echo $lin; ?>">
                <strong>
                    <!-- sector -->
                    <?php $textbit = 'sector';
                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_ar"));
                    echo $translation ?>                </strong>
                <?php
                select2::create(array(
                    // "multiple"=>false,
                    "name" => "sector",
                    "defaultOption" => array("--" => ""),
                    "dataStore" => $this->dataStore("sector1"),
                    "dataBind" => array(
                        "text" => "name",
                        "value" => "id",
                    ),
                    "attributes" => array(
                        "class" => "col-md-4 form-control"
                    ),
                ));
                ?>
            </div>
            <div class="col-md-3 form-group" style="float: <?php echo $lin; ?>">
                <strong>
                    <!-- org_unit -->
                    <?php $textbit = 'org_unit';
                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_ar"));
                    echo $translation ?>                </strong>
                <?php

                Select2::create(array(
                    // "multiple"=>true,
                    "name" => "section",
                    "defaultOption" => array("--" => ""),
                    "dataStore" => $store,
                    "dataBind" => array(
                        "text" => "name",
                        "value" => "id",
                    ),
                    "attributes" => array(
                        "class" => "col-md-4 form-control"
                    ),
                ));
                ?>
            </div>
            <div class="col-md-3 form-group" style="float: <?php echo $lin; ?>">
                <strong>
                    <!-- project_symbol -->
                    <?php $textbit = 'project_symbol';
                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_ar"));
                    echo $translation ?>                </strong>
                <?php
                select2::create(array(
                    // "multiple"=>true,
                    "name" => "project_symbol",
                    "defaultOption" => array("--" => ""),
                    "dataStore" => $this->dataStore("project_symbol"),
                    "dataBind" => array(
                        "text" => "symbol",
                        "value" => "symbol",
                    ),
                    "attributes" => array(
                        "class" => "col-md-4 form-control"
                    ),
                ));
                ?>
            </div>
            <div class="col-md-3 form-group" style="float:<?php echo $lin; ?>">
                <strong>
                    <!-- project_name -->
                    <?php $textbit = 'project_name';
                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_report')->get(0, "value_ar"));
                    echo $translation ?>                </strong>
                <?php
                select2::create(array(
                    // "multiple"=>true,
                    "name" => "project_name",
                    "dataStore" => $this->dataStore("project_name"),
                    "defaultOption" => array("--" => ""),
                    "dataBind" => array(
                        "text" => "name",
                        "value" => "name",
                    ),
                    "attributes" => array(
                        "class" => "col-md-4 form-control"
                    ),
                ));
                ?>
            </div>
            <!-- </div> -->

            <!-- <br> -->
        </form>
    </div>
    <!-- <div class="card"> -->
    <div
        style="overflow:auto;width:100%;background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:10px;padding-right:10px;">
        <br>
        <br>
        <br>
        <?php
        DataTables::create(array(
            "dataSource" => $new,
            "name" => "example",
            "columns" => array(
                "symbol" => array(
                    "label" => $language == 'en' ? $transtable->where('key_name', 'project_symbol')->where('key_pos', 'project_risk_report')->get(0,
                        "value_en") : $transtable->where('key_name', 'project_symbol')->where('key_pos', 'project_risk_report')->get(0, "value_ar")
                    // "رمز",
                ),
                "name_short" => array(
                    "label" => $language == 'en' ? $transtable->where('key_name', 'project_name')->where('key_pos', 'project_risk_report')->get(0,
                        "value_en") : $transtable->where('key_name', 'project_name')->where('key_pos', 'project_risk_report')->get(0, "value_ar"),
                    //"الاسم",

                ),
                "org_unit_name" => array(
                    "label" => $language == 'en' ? $transtable->where('key_name', 'org_unit')->where('key_pos', 'project_risk_report')->get(0,
                        "value_en") : $transtable->where('key_name', 'org_unit')->where('key_pos', 'project_risk_report')->get(0, "value_ar"),
                    //"org unit",
                    // "formatValue" => function ($value, $row) {
                    //     $org = $this->dataStore('org_name')->where('id', $value)->get(0, "name");
                    //     return "<div>$org</div>";


                    // }

                ),

                "start_dt_actual" => array(
                    "label" => $language == 'en' ? $transtable->where('key_name', 'start_dt_actual')->where('key_pos', 'project_risk_report')->get(0,
                        "value_en") : $transtable->where('key_name', 'start_dt_actual')->where('key_pos', 'project_risk_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",

                ),
                "end_dt_base" => array(
                    "label" => $language == 'en' ? $transtable->where('key_name', 'end_dt_base')->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', 'end_dt_base')->where('key_pos', 'project_risk_report')->get(0, "value_ar"),//"communication officer",
                    // "cssStyle" => "overflow-wrap:beak-word;"

                ),
                "status_operational" => array(
                    "label" => $language == 'en' ? $transtable->where('key_name', 'status_operational')->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', 'status_operational')->where('key_pos', 'project_risk_report')->get(0, "value_ar"),
                    "formatValue" => function ($value, $row) {
                        $language = $this->params['language'];
                        $transtable = $this->dataStore('translation');
                        if ($value == 0) {
                            $term = $language == 'en' ? $transtable->where('key_name', 'planning')->where('key_pos', 'kpi_status_report')->get(0, "value_en") : $transtable->where('key_name', 'planning')->where('key_pos', 'project_risk_report')->get(0, "value_ar");
                            return "<div>$term</div>";
                        }
                        if ($value == 1) {
                            $term = $language == 'en' ? $transtable->where('key_name', 'progressing')->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', 'progressing')->where('key_pos', 'project_risk_report')->get(0, "value_ar");
                            return "<div>$term</div>";
                        }
                        if ($value == 2) {
                            $term = $language == 'en' ? $transtable->where('key_name', 'onhold')->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', 'on_hold')->where('key_pos', 'project_risk_report')->get(0, "value_ar");
                            return "<div>$term</div>";
                        }
                        if ($value == 3) {
                            $term = $language == 'en' ? $transtable->where('key_name', 'closed')->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', 'closed')->where('key_pos', 'project_risk_report')->get(0, "value_ar");
                            return "<div>$term</div>";
                        }

                    } //"تاريخ القراءة التالية",
                ),


                "proj_status_name" => array(
                    "label" => $language == 'en' ? $transtable->where('key_name', 'proj_status')->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', 'proj_status')->where('key_pos', 'project_risk_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",


                ),
                // ),
                "open_risk_count" => array(
                    "label" => $language == 'en' ? $transtable->where('key_name', 'risk_issues_count')->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', 'risk_issues_count')->where('key_pos', 'project_risk_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                    // "value_type" => array(
                    //     "label" =>get_text('value_type',$language),
                ),
                "show_details" => array(
                    "label" => $language == 'en' ? $transtable->where('key_name', 'show_details')->where('key_pos', 'project_risk_report')->get(0, "value_en") : $transtable->where('key_name', 'show_details')->where('key_pos', 'project_risk_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                    //  "Next Value Date"  ,
                    "formatValue" => function ($value, $row) {
                        $language1 = $this->params['language'];
                        $proj_id1 = $row['id'];
                        $Mixed = array("proj_id" => $proj_id1);
                        $url = $language1 . "/ProjectRiskSubReport/" . http_build_query(array(
                                "array" => $Mixed
                            ));

                        return "<a href=" . $url . "><span class='glyphicon glyphicon-arrow-right'></span>
                        </a>";
                    }
                ),
            ),
            "cssClass" => array(
                "table" => "table table-striped table-bordered color  ",
                "th" => "cssHeader insideBorder ",
                "tr" => "cssItem color",
                "td" => "insideBorder"
            ),
            "options" => array(
                // "columnDefs" => array(
                //     array("width" => 50, "targets" => 0),
                //     array("width" => 120, "targets" => 1),
                //     array("width" => 100, "targets" => 2),
                //     array("width" => 80, "targets" => 3),
                //     array("width" => 50, "targets" => 4),
                //     array("width" => 60, "targets" => 5),
                //     array("width" => 60, "targets" => 6),
                //     array("width" => 50, "targets" => 7),
                //     array("width" => 50, "targets" => 8),
                //     array("width" => 30, "targets" => 9),
                //     array("width" => 60, "targets" => 10),
                //     array("width" => 30, "targets" => 11),
                //     array("width" => 30, "targets" => 12),
                //     array("width" => 30, "targets" => 13),
                //     array("width" => 40, "targets" => 14),
                //     array("width" => 40, "targets" => 15),
                //     array("width" => 50, "targets" => 16),
                //     array("width" => 50, "targets" => 17),
                // ),
                "searching" => true,
                "paging" => true,
                "orders" => array(
                    array(0, "asc")
                )
            )


        ));
        ?>
    </div>

    <script type="text/javascript">
        KoolReport.load.onDone(function () {
            var locale = '<?php echo $language;?>';

            var table = $('#example').DataTable({
                destroy: true,
                "pageLength": 50,
                "language": {
                    "sProcessing": locale == 'ar' ? "جارٍ التحميل..." : "Processing...",
                    "sLengthMenu": locale == 'ar' ? "اعرض _MENU_ سجلات" : "Show _MENU_ entries",
                    "sZeroRecords": locale == 'ar' ? "لم يعثر على أية سجلات" : "No matching records found",
                    "sInfo": locale == 'ar' ? "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل" : "Showing _START_ to _END_ of _TOTAL_ entries",
                    "sInfoEmpty": locale == 'ar' ? "يعرض 0 إلى 0 من أصل 0 سجل" : "Showing 0 to 0 of 0 entries",
                    "sInfoFiltered": locale == 'ar' ? "(منتقاة من مجموع _MAX_ مُدخل)" : "(filtered from _MAX_ total entries)",
                    "sInfoPostFix": "",
                    "sSearch": locale == 'ar' ? "ابحث:" : "Search:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": locale == 'ar' ? "الأول" : "First",
                        "sPrevious": locale == 'ar' ? "السابق" : "Last",
                        "sNext": locale == 'ar' ? "التالي" : "Next",
                        "sLast": locale == 'ar' ? "الأخير" : "Previous",
                    },
                    // buttons: {
                    //     colvisRestore: locale == 'ar' ? " إعادة إظهار الخانات" : "restore"
                    // }
                },
                "buttons": [
                    // {
                    //     extend: 'colvis',
                    //     text: '<i class="fa fa-align-justify" style="color:#a9a9a9;"></i>',
                    //     postfixButtons: ['colvisRestore'],
                    //     titleAttr: 'إظهار/إخفاء الخانات',
                    //     columns: ':not(.noVis)',
                    // },
                    {
                        extend: 'print',
                        text: '<i style="color:#a9a9a9;" class="fa fa-print"></i>',
                        titleAttr: 'طباعة',
                        autoPrint: true,
                        cssClass: 'printButton',
                        exportOptions: {
                            columns: ':visible',
                        },
                        customize: function (win) {
                            $(win.document.body).find('table').addClass('display').css('font-size', '9px');
                            $(win.document.body).find('table').addClass('display').css('direction', '<?php echo $language == 'en' ? "ltr" : "rtl";?>');
                            $(win.document.body).find('tr:nth-child(odd) td').each(function (index) {
                                $(this).css('background-color', '#D0D0D0');
                            });
                            $(win.document.body).find('h1').css('text-align', 'center');

                            $(win.document.body).prepend('<div style="text-align:center;"><?php echo $sector11; ?></div>'); //before the table
                            $(win.document.body).find('th').css('background-color', '#2f353a');
                            $(win.document.body).find('th').css('color', '#fff');
                        }
                    },
                ],
                // responsive: true,
                // "columnDefs": [{
                //     "searchable": true,
                //     "orderable": true,
                //     "targets": 0
                // },
                //     {
                //         'visible': false,
                //         'targets': 4,
                //         // className: 'noVis'
                //     },
                //     {
                //         'visible': false,
                //         'targets': 5,
                //         // className: 'noVis'
                //     },
                //     {
                //         'visible': false,
                //         'targets': 6,
                //         // className: 'noVis'
                //     },
                //     {
                //         'visible': false,
                //         'targets': 7,
                //         // className: 'noVis'
                //     },
                //     {
                //         'visible': false,
                //         'targets': 8,
                //         // className: 'noVis'
                //     },
                //     {
                //         'visible': false,
                //         'targets': 10,
                //         // className: 'noVis'
                //     },
                //     {
                //         'visible': false,
                //         'targets': 16,
                //         // className: 'noVis'
                //     },
                // ], "order": [[0, 'asc']],
                initComplete: function () {

                    this.api().column([0]).every(function () {
                        var column = this;
                        var select = $('#project_symbol')
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                console.log('^' + val + '$');
                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });
                    });
                    this.api().column([1]).every(function () {
                        var column = this;
                        var select = $('#project_name')
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                console.log('^' + val + '$');
                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });
                    });
                    var s = $('#sector');
                    // var t=  $('#section').val();
                    // console.log(t);
                    s.on('change', function () {
                        // if(t!="")
                        // $('#section').val(t).trigger("change");
                        // else
                        $('#section').val(null).trigger("change");
                        $('#form1').submit();
                    });
                    $('#section').on('change', function () {
                        $('#form1').submit();
                    });
                    // $('#project_symbol').on('change', function () {
                    //     $('#form1').submit();
                    // });
                    // $('project_name').on('change', function () {
                    //     $('#form1').submit();
                    // });

                }
            });
            table.buttons().container().appendTo($('#button1'));
            /*table.on( 'order.dt search.dt', function () {
                table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            }).draw();*/
        });

        function myFunction() {
            var x = document.getElementById("myDIV");
            if (x.style.display === "none") {
                $(x).show('slow');
            } else {
                $(x).hide('slow');
            }
        };

        function linktokpi() {
            var m = $('#sector').val()
// alert(m);
            var n = $('#section').val()
            console.log(n);

            // history.replaceState('data to be passed', 'NAJAH', '/kpilist');

            // document.cookie = "sectorname="+m+"; path=/; ";
            document.cookie = "sectorname=" + m + ";domain=.najah.online; path=/; ";


            if (n != null)

                document.cookie = "orgname=" + n + ";domain=.najah.online; path=/; ";

            //document.cookie = "orgname="+n+"; path=/; ";

            //    top.window.location.href= "http://localhost:8080/kpilist";
            top.window.location.href = "https://dev.najah.online/kpilist";


        };
    </script>
    <style>
        .container1 {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
            display: inline-flex;
            column-width: 100%;
            padding: 0px 0px;
            background: #fff;
        }

        .buttons-print {
            background-color: #ffffff;
            boder: none;
            /*color:black;*/
        }

        button.dt-button, div.dt-button, a.dt-button, a.dt-button:focus {
            border: none !important;
            background-color: #ffffff;
            background: none;
            padding: 0;
        }


        div.dt-button-collection {

            /* top: 19.6166px; */
            left: -98.433px;
            transform: translateX(-98px);
        }

        /* .card-body {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        } */
        .select2 {
            width: 100% !important;
            /*border: 1px solid #e8e8e8;*/
            min-height: 40px;
        }

        button.dt-button {
            font-size: 0.68em;
        }

        /* .select2:after{
            content: '';
            position:absolute;
            left:10px;
            top:15px;
            width:0;
            height:0;
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-top: 5px solid #888;
            direction: rtl;
        }; */
    </style>
</div>
</body>
</html>
