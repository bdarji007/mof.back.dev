<?php

namespace Modules\ClientApp\Reports;

use \koolreport\processes\Filter;
use \koolreport\processes\ColumnMeta;
use \koolreport\processes\CalculatedColumn;
use \koolreport\processes\Custom;
use Modules\ClientApp\User;
use Spatie\Permission\Models\Role;

error_reporting(E_ALL ^ E_NOTICE);

class ProjectBudgetSummaryReport extends \koolreport\KoolReport
{
    use \koolreport\clients\jQuery;
    use \koolreport\clients\Bootstrap;

//  use \koolreport\clients\FontAwesome;
    use \koolreport\laravel\Friendship;
    use \koolreport\inputs\Bindable;
    use \koolreport\inputs\POSTBinding;

    public $sect;

    function __construct(array $params = array())
    {
        $this->sect = $this->params['sect'];
        $this->org = $this->params['org'];
        $this->sid = $this->params['sid'];
        $this->oid = $this->params['oid'];
        parent::__construct($params);
    }

    protected function defaultParamValues()
    {
        $month =  date('n');
        $currentmtp = \DB::select(\DB::raw("select mtp.id, mtp.name, fys.start_date, curdate(), fye.end_date from mtp , fiscal_year fys, fiscal_year fye where
mtp.tenant_id = 1  and fys.id = mtp.mtp_start and fye.id = mtp.mtp_end and
CURDATE() >= fys.start_date and CURDATE() <= fye.end_date"));
        $currentmtpID = $currentmtp[0]->id;

        $currentmtpstartdate = $currentmtp[0]->start_date;
        $currentmtpenddate = $currentmtp[0]->end_date;
        $getAllYears = \DB::select(\DB::raw("SELECT @rownum:=@rownum+1 as no, f.*  FROM (SELECT @rownum:=0) r, `fiscal_year` as f WHERE start_date >= '$currentmtpstartdate' and start_date <= '$currentmtpenddate'"));

        $currentYear = 1;
        foreach ($getAllYears as $key => $years) {
            if($years->start_date < date('Y-m-d') && $years->end_date > date('Y-m-d')) {
                $currentYear = $years->no;
            }
        }

        $currentYearReal = $currentYear;

        $currentPeriod = '';
        if(in_array($month, [4,5,6])) {
            $currentPeriod = 1;
        } else if(in_array($month, [7,8,9])) {
            $currentPeriod = 2;
        } else if(in_array($month, [10,11,12])) {
            $currentPeriod = 3;
        } else if(in_array($month, [1,2,3])) {
            $currentPeriod = 4;
        }

        $currentPeriodReal = $currentPeriod;

        if($currentYear == 1 && $currentPeriod == 1) {
            $currentmtpID = count($currentmtp) > 0 ? $currentmtpID -1 : $currentmtpID;
            $currentPeriod = 4;
            $currentYear = 3;
        }
        if($currentPeriod == 1 && $currentYear != 1) {
            $currentPeriod = 4;
            $currentYear = $currentYear -1;
        }

        return array(
            "sector" => null,
            "section" => "",
            "parent" => null,
            "project_name" => null,
            "project_symbol" => null,
            // "debug_modeprog"=>false,
            // "debug_modeperf"=>true,
            "tenant" => env('TENANT_ID'),
            "mtp" => ($currentPeriodReal == 1 && $currentYearReal == 1) ? $currentmtpID : $currentmtp[0]->id,
            "fiscal_year" => null,
        );
    }

    protected function bindParamsToInputs()
    {
        return array(
            "sector",
            "section",
            "project_name",
            "project_symbol",
            // "debug_modeprog",
            // "debug_modeperf",
            // "backlink",
            "tenant",
            "mtp",
            "fiscal_year",
            "parent",


        );
    }

    public function settings()
    {
        return array(
            "dataSources" => array(
                "mysql" => array(
                    'host' => env('DB_HOST'),
                    'username' => env('DB_USERNAME'),
                    'password' => env('DB_PASSWORD'),
                    'dbname' => env('DB_DATABASE'),
                    'charset' => 'utf8',
                    'class' => "\koolreport\datasources\MySQLDataSource",
                ),
            )
        );
    }

    function setup()
    {


//-----------------------------------------------------------------------------------

        if (empty($_POST['sector']))
            $this->params['sector'] = $this->params['sid'];

        if (empty($_POST['section']))
            $this->params['section'] = $this->params['oid'];

        if ($this->params['sector'] == "" || $this->params['sector'] == "null")
            $this->params['sector'] = null;

        if ($this->params['section'] == "" || $this->params['sector'] == "null")
            $this->params['section'] = null;

        if ($this->params['sector'] != 'null' && $this->params['sector'] != '') {
            if ($this->params['sector']) {
                $id = $this->params['sector'];

                $ddd = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), $id, '', CONCAT(id, '') from subtenant where parent_id = $id) select id, name from cte order by path"));
                $sectorKeys = [];
                foreach ($ddd as $dd) {
                    if ($dd->id) {
                        $sectorKeys[] = $dd->id;
                    }
                }
            }

            if (!empty($_POST['sector']) && (int)$_POST['sector'] != (int)$this->params['sid']) {
                if (!in_array($_POST['section'], $sectorKeys)) {
                    $this->params['section'] = "";
                }
                //$this->params['section'] = "";
            } else {
                if (!in_array($this->params['section'], $sectorKeys)) {
                    $this->params['section'] = "";
                } else {
                    $this->params['section'] = $this->params['oid'];
                }
            }

            if($this->params['sid'] == $this->params['sector']) {
                if(!empty($_POST['section'])) {
                    $this->params['section'] = (!in_array($this->params['section'], $sectorKeys)) ? $this->params['oid']
                        : $_POST['section'];
                }
            }

            if(!empty($_POST['sector']) && $_POST['sector'] == 2) {
                $this->params['section'] = "";
            }
        }

        if (isset($this->params['sector']) && !empty($this->params['sector'])) {
            $parent_id = $this->params["sector"];
            if (isset($this->params['section']) && !empty($this->params['section'])) {
                $parent_id = $this->params["section"];
            } else {
                $parent_id = $this->params["sector"];
            }
        } else {
            $parent_id = null;

        }
        // var_dump($this->params);

        $this->params["parent"] = $parent_id;
        //  $sql1=$this->params['sector']!=null?"sector_id=$this->params['sector']":"1=1".($this->params["sector"]!=null?""sector_id=$this->params['sector']":"1=1")." ;
        $this->src("mysql")
            ->query("select 	p.id as id, p.symbol as symbol, p.name_short, sub.name as resp_org_unit, p.sector_id, p.subtenant_id,
            /**p.start_dt_actual, p.end_dt_base, pvs.pd_planned_duration, pv.progress_val,**/
            p.status_operational,
            p.budget_base,
            /**
            if(p.has_budget=1, pvs.b_spi, pvs.spi) as spi, pvs.b_cpi,
            pvs.sv_sched_var_pct, pvs.sv_sched_var_days, pvs.b_sv_sched_var, **/
            p.spending_total as actual_cost, (p.budget_base - ifnull(p.spending_total,0)) as remaining_budget,
            pvs.b_cv_cost_var as cost_variance,
            pvs.b_etc, pvs.b_eac, pvs.b_bac, b_var_at_complete,
            pvs.proj_perf as proj_prog, pvs.proj_status,
            ps.name as proj_status_name, ps.color_code as proj_status_color
from 		mtp, fiscal_year fys, fiscal_year fye, subtenant sub, project p
left join fiscal_year fyf -- fiscay_year_filter
    on	fyf.id = :fiscal_year
left join proj_values pv
        ON	pv.project_id = p.id and
                    pv.period_dt = 	-- last filled reading .. is that correct the last reading?!
                                                                    (select max(pv_i.period_dt) from proj_values pv_i, proj_value_stats pvs_i where
                                                                                            pv_i.project_id = pv.project_id and
                                                                                            pvs_i.id = pv_i.id and
                                                                                            pv_i.progress_val is not null
                                                                    )
left join proj_value_stats pvs
         ON	pvs.id = pv.id
left join proj_status ps
        ON	ps.id = pvs.proj_status
where
        p.has_budget = 1 and
        p.tenant_id = :tenant_id and
        (
                    (:parent_id is null)
                    or
                    (p.sector_id = :parent_id)
                    or
                    (p.subtenant_id = :parent_id)
        ) and
        sub.id = ifnull(p.subtenant_id, p.sector_id) and
        mtp.id = :mtp and
        mtp.mtp_start = fys.id and
        mtp.mtp_end = fye.id and
        (
                    (p.mtp_id = :mtp) -- project is defined for the mtp (in general even if mtp is not the current one
                    or
                    (p.start_dt_actual between fys.start_date and fye.end_date) -- project start date is within the selected mtp
                    or
                    (ifnull(p.end_dt_actual, CURDATE()) between fys.start_date and fye.end_date) -- project end date is within the selected mtp, if it's null assume it's now to reduce the conditions
                    or
                    (p.start_dt_actual < fys.start_date and ifnull(p.end_dt_actual, CURDATE()) > fye.end_date) -- project spanning multiple mtps
        ) and
        (
                (:fiscal_year is null)
                or
                (p.start_dt_actual between fyf.start_date and fyf.end_date) -- project start date is within the selected mtp
                or
                (ifnull(p.end_dt_actual, CURDATE()) between fyf.start_date and fyf.end_date) -- project end date is within the selected mtp, if it's null assume it's now to reduce the conditions
                or
                (p.start_dt_actual < fyf.start_date and ifnull(p.end_dt_actual, CURDATE()) > fyf.end_date) -- project spanning multiple mtps
        )
;")
            ->params(array(":parent_id" => $this->params["parent"], ":tenant_id" => $this->params["tenant"], ":fiscal_year" => $this->params["fiscal_year"], ":mtp" => $this->params["mtp"]))
            ->pipe($this->dataStore('project_details'));


        //-------------------To get work on behalf roles----------------------------------------------------------------------------------

        $userDetails = User::find($this->params["uid"]);
        if ($this->params["uid"] != "null") {

            /*$this->src("mysql")
                ->query("select model_has_roles.role_id, model_has_roles.model_id, role_work_on_behalf_sectors.sector_id, role_work_on_behalf_sectors.subtenant_id from model_has_roles INNER JOIN role_work_on_behalf_sectors ON role_work_on_behalf_sectors.role_id = model_has_roles.role_id")
                ->pipe(new Filter(array(
                    array("model_id", "=", $this->params["uid"])
                )))
                ->pipe($this->dataStore('role1'))->requestDataSending();*/

            $getRole = Role::where('name', $userDetails->currentRole)->pluck('id')->all();
            $this->src("mysql")
                ->query("select model_has_roles.role_id, model_has_roles.model_id, role_work_on_behalf_sectors.sector_id, role_work_on_behalf_sectors.subtenant_id from model_has_roles INNER JOIN role_work_on_behalf_sectors ON role_work_on_behalf_sectors.role_id = model_has_roles.role_id
                where 1=1
            " . (" and model_id IN ('" . $this->params["uid"] . "')") . "
            " . (" and model_has_roles.role_id in ('" . $getRole[0] . "')") . "
            ")->pipe($this->dataStore('role1'))->requestDataSending();


        }

        $role_sector = $this->dataStore("role1")->only("sector_id")->data();
        $role_subtenant = $this->dataStore("role1")->only("subtenant_id")->data();
        $role_id = $this->dataStore("role1")->get(0, "role_id");
        $i = 0;

        // var_dump($role_sector)--fetching role sector;
        foreach ($role_sector as $key => $value) {
            foreach ($value as $k => $v) {
                $r_sect[$i] = $v;
                $i = $i + 1;
            }
        }
        $r_sect[$i] = (int)$this->params["sid"];
        if(in_array(2, $r_sect)) {
            $merge= [2,3,4,5,6,7,8,9,10];
            $r_sect = $merge;//array_diff( $merge, [2] );
        }
        $m = 0;

        foreach ($role_subtenant as $key => $value) {
            foreach ($value as $k => $v) {
                $r_sub[$m] = $v;
                $m = $m + 1;
            }
        }
        $p_sub = $r_sub;
        $p_sub[$m] = (int)$this->params["oid"];

//----------------------------merging sid with role sector-----------------------------------------------------------------------------------

        if ($this->params["sid"] == "null") {

            $this->src("mysql")
                ->query("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)")//and s.subtenant_type_id in (2,3)
                ->pipe($this->dataStore('sector1'));
        }
        if ($this->params["sid"] != "null") {
            $this->src("mysql")
                ->query("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)")
                ->pipe(new Filter(array(
                    array("id", "in", $r_sect)
                )))
                ->saveTo($node);
            $node->pipe($this->dataStore('sector1'));


        }

        //----------------------------merging sid with role subtenant----------------------------------------------------------------------------------
        if ($this->params["sector"] != null) {

// ----------------------------------------selecting roles corresponding to this sector--------------------------------------------
            if ($this->params["oid"] != "null") {
                if (!empty(array_intersect($p_sub, $sectorKeys))) {
                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name, path from cte order by path")
                        ->params(array(":sector_id" => $this->params["sector"]))
                        ->pipe(new Filter(array(
                            array("id", "in", $p_sub)
                        )))
                        ->pipe(new Custom(function ($row) {
                            if ($row["id"] != NULL)
                                return $row;
                        }))
                        ->pipe($this->dataStore('section111'))->requestDataSending(); //sections corresponding to that sector
                } else {
                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name, path from cte order by path")
                        ->params(array(":sector_id" => $this->params["sector"]))
                        ->pipe($this->dataStore('section222'))->requestDataSending(); //sections
                }
            }

            $role_section_id = $this->dataStore("section111")->only("id")->data();
            $j = 0;
            foreach ($role_section_id as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v != NULL) {
                        $r_section[$j] = $v;
                        $j = $j + 1;
                    }
                }
            }
//-----------------------------generating each org unit datastore------------------------------
            if ($this->datastore('section111')->count() > 0) {
                foreach ($r_section as $key => $v) {

                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where id = :section_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '>', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :section_id, '', CONCAT(id, '') from subtenant where id = :section_id) select id,  name from cte order by path")
                        ->params(array(":section_id" => $v))
                        ->saveTo($node_test);

                    $section_name = "sect" . $v;
                    $node_test->pipe($this->dataStore($section_name));

                }


            }


            //-------------------------------------------------------------------------------------------------------------------------------
            if ($this->params["oid"] == "null") {

                $this->src("mysql")
                    ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name from cte order by path")
                    ->params(array(":sector_id" => $this->params["sector"]))
                    ->pipe($this->dataStore('section1'));
            }

        }
        $this->src("mysql")
            ->query("select id,name from mtp")
            ->pipe($this->dataStore('mtp1'));
        $this->src("mysql")
            ->query("select id,name,status_operational from project")
            // ->pipe(new Filter(array(
            //     array("status_operational", "!=", 3),
            //
            // )))
            ->pipe($this->dataStore('project_name'));

        $this->src("mysql")
            ->query("select id,symbol,status_operational from project")
            ->pipe($this->dataStore('project_symbol'));

        $this->src("mysql")
            ->query(" SELECT project_id, COUNT(*) as count
            FROM proj_risks
            GROUP BY project_id;")
            ->pipe($this->dataStore('risk_details'));


        $this->src("mysql")
            ->query("select * from trans_table")
            ->pipe($this->dataStore('translation'))->requestDataSending();
        $this->src("mysql")
            ->query("select id,name from subtenant")
            ->pipe($this->dataStore('org_name'))->requestDataSending();
    }
}
