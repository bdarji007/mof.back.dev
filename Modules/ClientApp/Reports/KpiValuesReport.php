<?php

namespace Modules\ClientApp\Reports;
error_reporting(E_ALL ^ E_NOTICE);

class KpiValuesReport extends \koolreport\KoolReport
{
    use \koolreport\inputs\Bindable;
    use \koolreport\inputs\POSTBinding;

    protected function defaultParamValues()
    {
        $month =  date('n');
        $currentmtp = \DB::select(\DB::raw("select mtp.id, mtp.name, fys.start_date, curdate(), fye.end_date from mtp , fiscal_year fys, fiscal_year fye where
mtp.tenant_id = 1  and fys.id = mtp.mtp_start and fye.id = mtp.mtp_end and
CURDATE() >= fys.start_date and CURDATE() <= fye.end_date"));
        $currentmtpID = $currentmtp[0]->id;

        $currentmtpstartdate = $currentmtp[0]->start_date;
        $currentmtpenddate = $currentmtp[0]->end_date;
        $getAllYears = \DB::select(\DB::raw("SELECT @rownum:=@rownum+1 as no, f.*  FROM (SELECT @rownum:=0) r, `fiscal_year` as f WHERE start_date >= '$currentmtpstartdate' and start_date <= '$currentmtpenddate'"));

        $currentYear = 1;
        foreach ($getAllYears as $key => $years) {
            if($years->start_date < date('Y-m-d') && $years->end_date > date('Y-m-d')) {
                $currentYear = $years->no;
            }
        }

        $currentYearReal = $currentYear;

        $currentPeriod = '';
        if(in_array($month, [4,5,6])) {
            $currentPeriod = 1;
        } else if(in_array($month, [7,8,9])) {
            $currentPeriod = 2;
        } else if(in_array($month, [10,11,12])) {
            $currentPeriod = 3;
        } else if(in_array($month, [1,2,3])) {
            $currentPeriod = 4;
        }

        $currentPeriodReal = $currentPeriod;

        if($currentYear == 1 && $currentPeriod == 1) {
            $currentmtpID = count($currentmtp) > 0 ? $currentmtpID -1 : $currentmtpID;
            $currentPeriod = 4;
            $currentYear = 3;
        }
        if($currentPeriod == 1 && $currentYear != 1) {
            $currentPeriod = 4;
            $currentYear = $currentYear -1;
        }

        return array(
            "sector" => "",
            "section" => "",
            "mtp" => ($currentPeriodReal == 1 && $currentYearReal == 1) ? $currentmtpID : $currentmtp[0]->id,
            "periodicity" => 3,
            "kpi_category" => 0,
            "kpi_activation_status" => -1,
            "kpi_status" => -1,
        );
    }

    protected function bindParamsToInputs()
    {
        return array(
            "sector",
            "section",
            "mtp",
            "periodicity",
            "kpi_category",
            "kpi_activation_status",
            "kpi_status",
        );
    }

    public function settings()
    {
        return array(
            "dataSources" => array(
                "mysql" => array(
                    'host' => env('DB_HOST'),
                    'username' => env('DB_USERNAME'),
                    'password' => env('DB_PASSWORD'),
                    'dbname' => env('DB_DATABASE'),
                    'charset' => 'utf8',
                    'class' => "\koolreport\datasources\MySQLDataSource",
                ),
            )
        );
    }

    function setup()
    {
        $this->src("mysql")
            // ->query("set @kpi_cat = :kpi_category_id;")
            ->query("select kv.target_date as scheduled_date, kv.actual_value as value, kv.actual_numerator as value_numerator, kv.actual_denominator as value_denominator
            , kd.value_type, kd.numerator_name, kd.denominator_name, kd.rounding_decimals
            , kv.actual_date as value_date, notes
            , pt.id as value_explanation_id, pt.name as value_explanation_name
            from kpi_values kv, kpi_target kt, kpi_def kd, kpi_performance_type pt where
            kv.kpi_target_id = kt.id and
            kt.kpi_id = kd.id and
            pt.id = kd.value_explanation and
            kt.kpi_id = :kpi and
            kt.mtp_id = :mtp
            order by kv.target_year, kv.target_month;")
            ->params(array(
                ":kpi" => $this->params["kpi"],
                ":mtp" => $this->params["mtp"]
            ))
            ->pipe($this->dataStore('user_details'));

        $this->src("mysql")
            ->query("select * from trans_table")
            ->pipe($this->dataStore('translation'))->requestDataSending();
        $this->src("mysql")
            ->query("select id,name from mtp")
            ->pipe($this->dataStore('mtp1'));
    }
}
