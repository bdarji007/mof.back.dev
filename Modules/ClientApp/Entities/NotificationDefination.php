<?php

namespace Modules\ClientApp\Entities;
use OwenIt\Auditing\Contracts\Auditable;

class NotificationDefination extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $fillable = ['description', 'channel', 'event_id', 'content', 'status_active', 'start_dt', 'end_dt', 'notif_time', 'is_recur', 'recur_period', 'recur_dow', 'recur_dom', 'recur_m_condition', 'recur_q_condition', 'recur_qe_diff_days'] ;
    protected $table = "notif_def" ;

    protected $auditInclude = ['description', 'channel', 'event_id', 'content', 'status_active', 'start_dt', 'end_dt', 'notif_time', 'is_recur', 'recur_period', 'recur_dow', 'recur_dom', 'recur_m_condition', 'recur_q_condition', 'recur_qe_diff_days'];

    protected $auditEvents = [
        'created',
        'updated',
        'deleted',
        'restored',
    ];
}
