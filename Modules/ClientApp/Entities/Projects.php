<?php

namespace Modules\ClientApp\Entities;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Projects extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;


    protected $table = "project" ;
    protected $primaryKey = "id" ;
    protected $fillable = ['tenant_id', 'sector_id', 'subtenant_id' , 'name','name_short','symbol ','description','status_approval','status_operational','project_type','project_category','value_period','start_dt_base','start_dt_actual','end_dt_base','end_dt_projected','end_dt_actual','progress_total','has_budget','budget_base','budget_projected','spending_total','importance','has_forecasting','has_risks','proj_mgmt_by','proj_mgmt_by_ext','proj_owner','proj_mgr','proj_mgr_ext','proj_coord_by','proj_comm_officer','has_team','team_permanent','proj_mgr_util','proj_coord_util','proj_comm_officer_util','proj_mgr_perf','proj_coord_perf','proj_comm_officer_perf','mtp_id'];
    // ...

    protected $auditInclude = ['tenant_id', 'sector_id', 'subtenant_id ' , 'name','name_short','symbol ','description','status_approval','status_operational','project_type','project_category','value_period','start_dt_base','start_dt_actual','end_dt_base','end_dt_projected','end_dt_actual','progress_total','has_budget','budget_base','budget_projected','spending_total','importance','has_forecasting','has_risks','proj_mgmt_by','proj_mgmt_by_ext','proj_owner','proj_mgr','proj_mgr_ext','proj_coord_by','proj_comm_officer','has_team','team_permanent','proj_mgr_util','proj_coord_util','proj_comm_officer_util','proj_mgr_perf','proj_coord_perf','proj_comm_officer_perf','mtp_id'];

    protected $auditEvents = [
        'created',
        'updated',
        'deleted',
        'restored',
    ];


}
