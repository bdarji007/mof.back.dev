<?php

namespace Modules\ClientApp\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Fiscalyear extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = "fiscal_year" ;

    protected $fillable = ['id', 'start_date' , "end_date"];
    protected $auditInclude = ['id', 'start_date' , "end_date"];
    protected $primaryKey = "id";
    protected $auditEvents = [
        'created',
        'updated',
        'deleted',
        'restored',
    ];
}
