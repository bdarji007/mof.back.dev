<?php

namespace Modules\ClientApp\Entities;
use OwenIt\Auditing\Contracts\Auditable;

class NotificationMessage extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $fillable = ['user' , 'userImage', 'message', 'status'] ;
    protected $table = "notification_messages" ;

    protected $auditInclude = [
        'user' , 'userImage', 'tenant_id' , 'message', 'status'
    ];

    protected $auditEvents = [
        'created',
        'updated',
        'deleted',
        'restored',
    ];
}
