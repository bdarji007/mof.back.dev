<?php

namespace Modules\ClientApp\Entities;
use OwenIt\Auditing\Contracts\Auditable;

class Notification extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $fillable = ['notif_def_id' , 'content', 'created_by'];
    protected $table = "notif" ;

    protected $auditInclude = [
        'notif_def_id' , 'content', 'created_by'
    ];

    protected $auditEvents = [
        'created',
        'updated',
        'deleted',
        'restored',
    ];
}
