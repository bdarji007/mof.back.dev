<?php

namespace Modules\ClientApp\Http\Controllers;

use App\Jobs\ProcessNotificationEmail;
use App\Notifications\MyNotificationChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Redis;
use Maher\Counters\Facades\Counters;
use Maher\Counters\Models\Counter;
use Modules\ClientApp\Dynamic;
use Modules\ClientApp\Entities\NotificationMessage;
use Modules\ClientApp\Entities\Projects;
use Modules\ClientApp\Entities\Projectscost;
use DateTime;
use Modules\ClientApp\User;
use Spatie\Permission\Models\Role;


class projectsController extends Controller
{
    //
    function __construct()
    {
        $this->user = \JWTAuth::parseToken()->authenticate() ;
        $this->middleware('permission:projects-view|projects-create|projects-edit|projects-delete', ['only' => ['projectlist', 'index', 'show']]);
        $this->middleware('permission:projects-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:projects-edit|projects-readings', ['only' => ['projectlistbyId', 'store']]);
        $this->middleware('permission:projects-delete', ['only' => ['destroy', 'removeprojectPermanently']]);
        $this->middleware('permission:projects-team', ['only' => ['loadprojectteamgetbyid', 'saveprojectteam']]);
        $this->middleware('permission:projects-updation', ['only' => ['projectnextupdateddate', 'projectsvaluesstore']]);
        $this->middleware('permission:projects-readings', ['only' => ['readingslist']]);
        $this->middleware('permission:projects-quickview', ['only' => ['getprojectreadigdetails']]);

        $this->middleware('permission:projects-approve', ['only' => ['projectApproveReject']]);
        $this->middleware('permission:projects-reject', ['only' => ['projectApproveReject']]);
        $this->middleware('permission:projects-operationalstatus', ['only' => ['projectoperationalstatus']]);

    }
    public function index()
    {


    }

    public function loadTenantsministry()
    {
        /*$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3 || s.subtenant_type_id=2"));*/
        $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }

    public function projectlistbyId($id)
    {
        $projectlist = \DB::select(\DB::raw("select *,ceiling(datediff(project.end_dt_base, project.start_dt_base) * 5/7) as duration from  project where id=$id"));
        $progressvaluedata = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $id and progress_val!='' ORDER BY id DESC LIMIT 1"));
        $progress_total = 0;
        if ($progressvaluedata) {
            $progress_total = $progressvaluedata[0]->progress_val;
        }
        if ($projectlist) {
            $i = 0;
            foreach ($projectlist as $list) {
//                $start_dt_base = new DateTime($list->start_dt_base);
                // $projectlist[$i]->budget_base = number_format($list->budget_base, 2, '.', ',');
                // $projectlist[$i]->spending_total = number_format($list->spending_total, 2, '.', ',');

                $projectlist[$i]->progress_total = $progress_total * 100;
//                $projectlist[$i]->end_dt_base = $end_dt_base->format('d/m/Y');
            }

            return response()->json([
                "code" => 200,
                "projectlist" => $projectlist
            ]);
        }
    }

    public function risklistbyId($id)
    {
        $risklist = \DB::select(\DB::raw("select 	count(1) as risk_count,
						sum(case when pr.status <> 0 then 1 else 0 end) as open_count,
						sum(case when pr.status = 0 then 1 else 0 end) as closed_count,
						sum(case when pr.propability * pr.impact in (1,2) then 1 else 0 end) as very_low_count,
						sum(case when pr.propability * pr.impact in (3,4) then 1 else 0 end) as low_count,
						sum(case when pr.propability * pr.impact in (5,6) then 1 else 0 end) as mid_count,
						sum(case when pr.propability * pr.impact in (7,8) then 1 else 0 end) as high_count,
						sum(case when pr.propability * pr.impact in (9) then 1 else 0 end) as very_high_count
from proj_risks pr
where pr.project_id=$id"));
        if ($risklist) {

            return response()->json([
                "code" => 200,
                "risklist" => $risklist
            ]);
        }
    }

    public function projectlist()
    {

        //if (auth()->user()->hasRole(['Admin'])) {
        if(auth()->user()->currentRole == 'Admin') {
            $id = 1;
        } else {
            $getSectors = [];
            $getSec = User::select('subtenant_id')->where('id', auth()->user()->id)->get();
            $getSectors[] = ($getSec[0]['subtenant_id']);
            $roles = [];
            foreach (auth()->user()->roles as $role) {
                $roles[$role->id] = $role->id;
            }
            sort($roles);

            $getRole = Role::where('name', auth()->user()->currentRole)->pluck('id')->all();
            /*$workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $roles[0] . "'"));*/
            $workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $getRole[0] . "'"));
            $getBehalfSector = [];
            if (count($workbelf)) {
                foreach ($workbelf as $behalf) {
                    $getBehalfSector[] = $behalf->subtenant_id;
                }
            }

            $id = implode(",", array_merge($getSectors, $getBehalfSector));
        }

        $projectlist = \DB::select(\DB::raw("
WITH RECURSIVE cte ( l1_id, id, parent_id, subtenant_type ) AS (
	SELECT 	id, id, parent_id, subtenant_type_id FROM subtenant WHERE
								id in ($id)
	UNION ALL
	SELECT c.l1_id, s.id, s.parent_id, s.subtenant_type_id FROM subtenant s
		INNER JOIN 	cte c
								ON 	s.parent_id = c.id
	)
SELECT	@rownum := @rownum + 1 Num,
							sub.NAME AS subtenant_name,
							project.*,
case when project.status_operational = 0 then project.start_dt_base
	else
	(select max(pv.period_dt) from proj_values pv where pv.project_id = project.id and project.status_operational <> 3 /*closed*/)
 end as next_reading_dt
FROM cte, ( SELECT @rownum := 0 ) r, project
	INNER JOIN 	subtenant sub
							ON 	sub.id = project.subtenant_id
WHERE
		project.subtenant_id = cte.id
ORDER BY Num ASC, CAST( project.symbol AS UNSIGNED ) DESC"));


//        $projectlist = \DB::select(\DB::raw("select project.*,ceiling(datediff(project.end_dt_base, project.start_dt_base) * 5/7) as duration,
//case when project.status_operational = 0 then project.start_dt_base
//	else
//	(select max(pv.period_dt) from proj_values pv where pv.project_id = project.id and project.status_operational <> 3 /*closed*/)
// end as next_reading_dt
//from project where project.status_operational <> 3 $whereorgunit;"));


        $mytime = Carbon::now();
        $mytime->toDateTimeString();
//        echo "ddd".$mytime."dsdf";
//        die();
        $redcross = false;
        $greencheck = false;
        if ($projectlist) {
            $i = 0;
            foreach ($projectlist as $list) {
                $readingslist = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $list->id and progress_val!='' and justification !=''  order  by period_dt desc "));

                $start_dt_base = new DateTime($list->start_dt_base);
                $end_dt_base = new DateTime($list->end_dt_base);
                $next_reading_dt = new DateTime($list->next_reading_dt);
                //  $projectlist[$i]->duration = $this->duration($list->start_dt_base, $list->end_dt_base);

                $projectlist[$i]->duration = $this->duration($list->start_dt_base, $list->end_dt_base);

                $projectlist[$i]->value_count = count($readingslist);
                $projectlist[$i]->start_dt_base = $start_dt_base->format('d/m/Y');
                $projectlist[$i]->end_dt_base = $end_dt_base->format('d/m/Y');
                $projectlist[$i]->next_reading_dtformatted = $next_reading_dt->format('d/m/Y');
                //$projectlist[$i]->next_reading_dt = $next_reading_dt;
                if ($projectlist[$i]->next_reading_dt >= $mytime) {
                    $greencheck = true;
                    $projectlist[$i]->greencheck = $greencheck;
                } else {
                    $redcross = true;
                    $projectlist[$i]->redcross = $redcross;

                }
                if ($list->proj_owner) {

                    $projectlist[$i]->projteamdefined = 1;
                } else {
                    $projectlist[$i]->projteamdefined = 0;
                }


                if (empty($list->subtenant_id) || !isset($list->subtenant_id) || $list->subtenant_id === 0) {
                    $subid = $list->sector_id;

                } else {
                    $subid = $list->subtenant_id;
                }
                $subid = \DB::select(\DB::raw("SELECT name  FROM subtenant  WHERE id=$subid"));
                $subtenant_name = $subid[0]->name;
                $projectlist[$i]->subtenant_name = $subtenant_name;

                $i++;
            }
        }
        return response()->json([
            "code" => 200,
            "projectlist" => $projectlist
        ]);
    }

    public
    function duration($date1, $date2)
    {
        $date1_ts = strtotime($date1);
        $date2_ts = strtotime($date2);
        $diff = $date2_ts - $date1_ts;
        return round($diff / 86400);
    }

    public
    function loadprojectearch(Request $request)
    {
        $orgunit = '';
        if (isset($request->val[0]['sortBySubTenant'])) {
            $orgunit = $request->val[0]['sortBySubTenant'];
        }
        $projectcat = $request->val[0]['category'];
        $projectop = $request->val[0]['operation'];
        $projectapprove = $request->val[0]['approve'];
        $neeupdate = $request->val[0]['update'];
        $projecttype = $request->val[0]['type'];
        $mtp = $request->val[0]['sortByMtp'];
        $project_start_date = $request->val[0]['project_start_date'];
        $project_end_date = $request->val[0]['project_end_date'];
        $projectlistarraynew = [];
        $projectlistarray = [];
        $wherecat = "";
        $whereop = "";
        $whereorgunit = "";
        $wheresector = "";
        $whereupdate = "";
        $whereapprove = "";
        $wheretype = "";
        $wheremtp = "";
        $wherestartdate = "";
        $whereenddate = "";
        if (!empty($project_start_date)) {
            // echo "in";
            $project_start_date = new DateTime($project_start_date);

            $project_start_date = $project_start_date->format('Y-m-d');

            $wherestartdate = " and  project.start_dt_base>='$project_start_date'";
        }
        if (!empty($project_end_date)) {
            // $project_end_date = new DateTime($project_end_date);
            //$project_end_date=  $project_end_date->format('Y-m-d');
            $project_end_date = strtotime($project_end_date);
            //$project_end_date = date("Y-m-d", strtotime("+1 day", $project_end_date));
            $project_end_date = date("Y-m-d", $project_end_date);

            $whereenddate = " and  project.end_dt_base <= '$project_end_date'";
        }

        /*if (!empty($orgunit)) {
            $subtypes = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$orgunit"));
            $subtype = $subtypes[0]->subtenant_type_id;
            if ($subtype == 2 or $subtype == 3) {
                $whereorgunit = " and   project.sector_id in ($orgunit)";
            } else {
                $whereorgunit = " and  project.subtenant_id in ($orgunit)";

            }
        } else {
            if (auth()->user()->hasRole(['Admin'])) {
                $orgunit = 1;
                $whereorgunit = "";
            } else {
                $getSectors = [];
                $getSec = User::select('subtenant_id')->where('id', auth()->user()->id)->get();
                $getSectors[] = ($getSec[0]['subtenant_id']);
                $roles = [];
                foreach (auth()->user()->roles as $role) {
                    $roles[$role->id] = $role->id;
                }
                sort($roles);
                $workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $roles[0] . "'"));
                $getBehalfSector = [];
                if (count($workbelf)) {
                    foreach ($workbelf as $behalf) {
                        $getBehalfSector[] = $behalf->subtenant_id;
                    }
                }

                if (count(array_merge($getSectors, $getBehalfSector)) == 1) {
                    $orgunit = implode(",", array_merge($getSectors, $getBehalfSector));
                    $whereorgunit = " and (project.sector_id in ($orgunit) OR project.subtenant_id in ($orgunit))";
                } else {
                    $orgunit = implode(",", array_merge($getSectors, $getBehalfSector));
                    $whereorgunit = " and (project.sector_id in ($orgunit) OR project.subtenant_id in ($orgunit))";

                }
            }
        }*/
//
//        if (!empty($sector)) {
//            // echo "in";
//            $wheresector = " and  project.sector_id=$sector";
//        }
        if (!empty($mtp)) {
            // echo "in";
            $wheremtp = " and  project.mtp_id=$mtp";
        }
//        if (!empty($orgunit)) {
//            // echo "in";
//            $whereorgunit = " and  project.subtenant_id=$orgunit or project.sector_id=$orgunit";
//        }


        if ($projectcat != null || $projectcat === 0) {
            $projectcat = implode(',', $projectcat);
            $wherecat = " and  project.project_category IN($projectcat)";
        }
        if ($projectop != null || $projectop === 0) {
            $projectop = implode(',', $projectop);
            $whereop = " and  project.status_operational IN($projectop)";
        }
        if ($projectapprove != null || $projectapprove === 0) {
            $projectapprove = implode(',', $projectapprove);
            $whereapprove = " and  project.status_approval IN($projectapprove)";

        }
        if ($projecttype != null || $projectapprove === 0) {
            $projecttype = implode(',', $projecttype);
            $wheretype = " and  project.project_type IN($projecttype)";

        }
        if (!empty($orgunit)) {
            $id= $orgunit;
        } else {
            //if (auth()->user()->hasRole(['Admin'])) {
            if(auth()->user()->currentRole == 'Admin') {
                $id = 1;
            } else {
                $getSectors = [];
                $getSec = User::select('subtenant_id')->where('id', auth()->user()->id)->get();
                $getSectors[] = ($getSec[0]['subtenant_id']);
                $roles = [];
                foreach (auth()->user()->roles as $role) {
                    $roles[$role->id] = $role->id;
                }
                sort($roles);
                $workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $roles[0] . "'"));
                $getBehalfSector = [];
                if (count($workbelf)) {
                    foreach ($workbelf as $behalf) {
                        $getBehalfSector[] = $behalf->subtenant_id;
                    }
                }

                $id = implode(",", array_merge($getSectors, $getBehalfSector));
            }
        }
        $projectlist = \DB::select(\DB::raw("
WITH RECURSIVE cte ( l1_id, id, parent_id, subtenant_type ) AS (
	SELECT 	id, id, parent_id, subtenant_type_id FROM subtenant WHERE
								id in ($id)
	UNION ALL
	SELECT c.l1_id, s.id, s.parent_id, s.subtenant_type_id FROM subtenant s
		INNER JOIN 	cte c
								ON 	s.parent_id = c.id
	)
SELECT	@rownum := @rownum + 1 Num,
							sub.NAME AS subtenant_name,
							project.*,
case when project.status_operational = 0 then project.start_dt_base
	else
	(select max(pv.period_dt) from proj_values pv where pv.project_id = project.id and project.status_operational <> 3 /*closed*/)
 end as next_reading_dt
FROM cte, ( SELECT @rownum := 0 ) r, project
	INNER JOIN 	subtenant sub
							ON 	sub.id = project.subtenant_id
WHERE
		project.subtenant_id = cte.id
		$wherecat $whereop $wheresector  $whereapprove $wheretype $wheremtp $wherestartdate $whereenddate
ORDER BY Num ASC, CAST( project.symbol AS UNSIGNED ) DESC"));

        /*$projectlist = \DB::select(\DB::raw("select project.*,
case when project.status_operational = 0 then project.start_dt_base
	else
	(select max(pv.period_dt) from proj_values pv where pv.project_id = project.id and project.status_operational <> 3)
 end as next_reading_dt
from project where 1 $wherecat $whereop $whereorgunit $wheresector  $whereapprove $wheretype $wheremtp $wherestartdate $whereenddate"));*/

        $redcross = false;
        $greencheck = false;
        if ($projectlist) {
            $i = 0;
            $j = 0;
            $k = 0;
            foreach ($projectlist as $list) {
                $readingslist = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $list->id and progress_val!='' and justification !=''  order  by period_dt desc "));
                $projectlist[$i]->value_count = count($readingslist);

                $projectlist[$i]->duration = $this->duration($list->start_dt_base, $list->end_dt_base);

                $start_dt_base = new DateTime($list->start_dt_base);
                $end_dt_base = new DateTime($list->end_dt_base);
                $next_reading_dt = new DateTime($list->next_reading_dt);


                $projectlist[$i]->start_dt_base = $start_dt_base->format('d/m/Y');
                $projectlist[$i]->end_dt_base = $end_dt_base->format('d/m/Y');
                $projectlist[$i]->next_reading_dtformatted = $next_reading_dt->format('d/m/Y');


                $mytime = Carbon::now();
                $mytime->toDateTimeString();
                $mytime->format('d/m/Y');
                if ($projectlist[$i]->next_reading_dt >= $mytime) {
                    $greencheck = true;
                    $projectlist[$i]->greencheck = $greencheck;
                } else {
                    $redcross = true;
                    $projectlist[$i]->redcross = $redcross;

                }

                if ($list->proj_owner) {

                    $projectlist[$i]->projteamdefined = 1;
                } else {
                    $projectlist[$i]->projteamdefined = 0;
                }

                if (empty($list->subtenant_id) || !isset($list->subtenant_id) || $list->subtenant_id === 0 || $list->subtenant_id == NULL) {
                    $subid = $list->sector_id;

                } else {
                    $subid = $list->subtenant_id;
                }
                $subid = \DB::select(\DB::raw("SELECT name  FROM subtenant  WHERE id=$subid"));
                $subtenant_name = $subid[0]->name;
                $projectlist[$i]->subtenant_name = $subtenant_name;
                if ($neeupdate) {

                    if (($next_reading_dt) <= $mytime) {
                        $projectlistarray[$j] = $projectlist[$i];
                        $j++;

                    } else {
                        $projectlistarraynew[$k] = $projectlist[$i];
                        $k++;

                    }
                }


                $i++;
            }
            if ($neeupdate) {
                if ($neeupdate[0] == 1) {
                    $projectlist = $projectlistarraynew;
                }
                if ($neeupdate[0] == 0) {
                    $projectlist = $projectlistarray;
                }


            }


        }


        if ($projectlist) {
            return response()->json([
                "code" => 200,
                "projectlist" => $projectlist
            ]);
        } else {


            return response()->json(["code" => 400]);
        }


        // echo $sector."=".$orgunit."=".$users."=".$screen."=".$datetime;
        //die();
    }

    public
    function loadprojectcostsearch(Request $request)
    {

        $projectcostrole = $request->val[0]['sortByrole'];
        $projectcostsscheme = $request->val[0]['sortBysalaryscheme'];

        $wherecostrole = "";
        $wherecostsscheme = "";

        if ($projectcostrole != null || $projectcostrole === 0) {
            $wherecostrole = " and  role_cost.role_id IN($projectcostrole)";
        }
        if ($projectcostsscheme != null || $projectcostsscheme === 0) {

            $wherecostsscheme = " and  role_cost.salary_scheme IN($projectcostsscheme)";
        }


        $projectcostlist = \DB::select(\DB::raw("SELECT role_cost.*,roles.name  FROM role_cost  INNER JOIN roles on roles.id = role_cost.role_id where 1 $wherecostrole $wherecostsscheme"));


        if ($projectcostlist) {

            $i = 0;
            foreach ($projectcostlist as $costlist) {
                if ($costlist->salary_scheme == 1)
                    $projectcostlist[$i]->salary_scheme = "General";
                elseif ($costlist->salary_scheme == 2) {
                    $projectcostlist[$i]->salary_scheme = "Financial";
                }

                $projectcostlist[$i]->salary_average = "KWD " . $costlist->salary_average;
                $projectcostlist[$i]->bonus_cost = "KWD " . $costlist->bonus_cost;


                $i++;
            }

            return response()->json([
                "code" => 200,
                "projectcostlist" => $projectcostlist
            ]);

        }

        return response()->json(["code" => 400]);


        // echo $sector."=".$orgunit."=".$users."=".$screen."=".$datetime;
        //die();
    }

    public
    function getprojecttypes()
    {
        $project_types = \DB::select(\DB::raw("select * from proj_type"));
        if ($project_types) {
            return response()->json([
                "code" => 200,
                "project_types" => $project_types
            ]);
        }
    }

    public
    function getprojectlist()
    {
        $project_list = \DB::select(\DB::raw("select * from project "));
        if ($project_list) {
            return response()->json([
                "code" => 200,
                "project_list" => $project_list
            ]);
        }
    }

    public
    function getuserroles()
    {
        $userroles = \DB::select(\DB::raw("select * from roles"));
        if ($userroles) {
            return response()->json([
                "code" => 200,
                "user_roles" => $userroles
            ]);
        }
    }

    public
    function loadTenants()
    {
        /*$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3"));*/
        $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }

    public
    function store(Request $request)
    {
//        $TenantId = \Auth::user()->idTenants;
//
        $subtypes = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$request->sortBySubTenant"));
        $subtype = $subtypes[0]->subtenant_type_id;
        if ($subtype == 2 or $subtype == 3) {
            $sectorid = $request->sortBySubTenant;
            //$subtenantid = NULL;
            $subtenantid = $request->sortBySubTenant;
        } else {
            $subtenantid = $request->sortBySubTenant;
            $parentids = \DB::select(\DB::raw("SELECT parent_id  FROM subtenant  WHERE id=$request->sortBySubTenant"));
            $parentid1 = $parentids[0]->parent_id;
            $subtypes1 = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$parentid1"));
            $subtype = $subtypes1[0]->subtenant_type_id;
            if ($subtype == 2 or $subtype == 3) {
                $sectorid = $parentid1;

            } else {
                $parentids2 = \DB::select(\DB::raw("SELECT parent_id  FROM subtenant  WHERE id=$parentid1"));
                $parentid2 = $parentids2[0]->parent_id;
                $subtypes2 = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$parentid2"));
                $subtype = $subtypes2[0]->subtenant_type_id;
                if ($subtype == 2 or $subtype == 3) {
                    $sectorid = $parentid2;

                } else {
                    $parentids3 = \DB::select(\DB::raw("SELECT parent_id  FROM subtenant  WHERE id=$parentid2"));
                    $parentid3 = $parentids3[0]->parent_id;
                    $subtypes3 = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$parentid3"));
                    $subtype = $subtypes3[0]->subtenant_type_id;
                    if ($subtype == 2 or $subtype == 3) {
                        $sectorid = $parentid3;

                    }
                }
            }
        }

        if (empty($request->id) || !isset($request->id) || $request->id === 0) {


            $request['subtenant_id'] = $sectorid;
            $getValCount = \DB::select(\DB::raw("select sub.id, sub.name, concat('P', sub.code) as kpi_symbol_prefix from subtenant sub where sub.id = '" . $request['subtenant_id'] . "'"));


            if (count($getValCount) > 0 && $getValCount[0]->kpi_symbol_prefix != null) {
                $checkCounter = \DB::select(\DB::raw("select * from counters where `key` = '" . $getValCount[0]->kpi_symbol_prefix . "'"));

                if (count($checkCounter) > 0) {
                    $count = Counters::increment($getValCount[0]->kpi_symbol_prefix);
                    $counter = Counters::getValue($getValCount[0]->kpi_symbol_prefix);

                } else {
                    Counter::create([
                        'key' => $getValCount[0]->kpi_symbol_prefix,
                        'name' => 'kpi_symbol_counter',
                        'initial_value' => 0,
                        'step' => 1
                    ]);
                    $count = Counters::increment($getValCount[0]->kpi_symbol_prefix);
                    $counter = Counters::getValue($getValCount[0]->kpi_symbol_prefix);
                }


            }
            $request['symbol'] = $getValCount[0]->kpi_symbol_prefix . '-' . sprintf("%04d", $counter);

            $projects = new Projects();

//echo $request->sortByTenant;
//die();


            $projects->tenant_id = 1;
            $projects->created_by = $request->created_by;
            //$startdate = DateTime::createFromFormat("Y-m-d", $request->project_start_date);
//        $startdateyear= Carbon::createFromFormat('Y-m-d', $request->project_start_date)->year;
//
//        $projects->symbol = "P-".$startdateyear."-1/".$request->sortByTenant;
            $projects->symbol = $request->symbol;


            // $projectlist[$i]->start_dt_base = $start_dt_base->format('d/m/Y');
            $projects->sector_id = $sectorid;
            $projects->subtenant_id = $subtenantid;
            $projects->name = $request->project_name;
            $projects->name_short = $request->project_short_name;

            $projects->description = $request->project_description;
            $projects->status_approval = $request->approval_status;
            $projects->status_operational = $request->status_operational;
            $projects->project_type = $request->project_type;
            $projects->project_category = $request->project_category;
            $projects->value_period = $request->project_update_interval;
            $projects->start_dt_base = date("Y-m-d", strtotime("$request->project_start_date"));
            if ($request->actual_start) {
                $projects->start_dt_actual = date("Y-m-d", strtotime("$request->actual_start"));
            }
            $projects->end_dt_base = date("Y-m-d", strtotime("$request->project_end_date"));
//            $projects->end_dt_projected =$request->end_dt_projected;
            $projects->end_dt_actual = $request->actual_closure;
            $projects->progress_total = $request->project_completion;
            $projects->has_budget = $request->project_has_budget;
            $projects->budget_base = $request->planned_budget;
            $projects->budget_projected = $request->budget_projected;
            $projects->spending_total = 0;
            $projects->importance = $request->project_importance;
            $projects->has_forecasting = $request->project_forcasting;
            $projects->has_risks = $request->project_has_risk;
            $projects->mtp_id = $request->mtp;


            if ($projects->save()) {

                $messageData['id'] = $request->symbol;
                $messageData['user'] = auth()->user()->name;
                $messageData['userImage'] = auth()->user()->file_name;
                $messageData['kpiname'] = '';//$model->name;
                $messageData['textshow'] = 'data inserted successfully';
                $messageData['time'] = date('Y-m-d H:i:s');
                //$messageData['allowusers'] = $kpidefdata;

                $notification = new NotificationMessage();
                $notification->user = auth()->user()->name;
                $notification->userImage = auth()->user()->file_name;
                $notification->message = json_encode($messageData, true);
                $notification->save();

                $token = array(
                    'USER_NAME' => auth()->user()->name,
                    'PROJ_SYMBOL' => $request->symbol,
                    'KPI_NAME' => '',//$model->name,
                    'MESSAGE' => null,
                    'DATE_TIME' => date('Y-m-d H:i:s'),
                );
                $pattern = '[%s]';
                foreach ($token as $key => $val) {
                    $varMap[sprintf($pattern, $key)] = $val;
                }

                $sendMail = false;

                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=16 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    //$sendMail = true;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=16 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $request->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=16 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $request->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $messageRedis = [];
                $messageRedis['id'] = $notification->id;
                $messageRedis['user'] = auth()->user()->name;
                $messageRedis['userImage'] = auth()->user()->file_name;
                $messageRedis['message'] = $messageData;

                $redis = Redis::connection();
                $redis->publish('projectupdate', json_encode($messageRedis, true));

                return response()->json([
                    "code" => 200,
                    "msg" => "project_created_successfully"
                ]);
            }
        } else {

            $projectsnew = new Projects($request->all());
            $projectsnew->setTable("project");
            $id = $request->get('id');

            $query = $projectsnew->find($id);

            $updates["sector_id"] = $sectorid;
            $updates["subtenant_id"] = $subtenantid;
            $updates["name"] = $request->project_name;
            $updates["name_short"] = $request->project_short_name;

            $updates["description"] = $request->project_description;
            $updates["status_approval"] = 0;
            $updates["status_operational"] = 0;
            $updates["project_type"] = $request->project_type;
            $updates["project_category"] = $request->project_category;

            $updates["value_period"] = $request->project_update_interval;
            $updates["start_dt_base"] = $request->project_start_date;
            $updates["start_dt_actual"] = date("Y-m-d", strtotime("$request->actual_start"));

            $olddateactual = $query->start_dt_actual;
            $newdateactual = $updates["start_dt_actual"];
            $oldupdateinterval = $query->value_period;
            $newupdateinterval = $updates["value_period"];

            if ($olddateactual != $newdateactual || $oldupdateinterval != $newupdateinterval) {
                $this->projectreadingsdelete($id);
            }
            $updates["end_dt_base"] = $request->project_end_date;
            $updates["end_dt_actual"] = $request->actual_closure;
            $updates["progress_total"] = $request->project_completion;
            $updates["has_budget"] = $request->project_has_budget;
            $updates["budget_base"] = $request->planned_budget;
            $updates["budget_projected"] = $request->budget_projected;
            $updates["spending_total"] = $request->actual_cost;
            $updates["importance"] = $request->project_importance;
            $updates["has_forecasting"] = $request->project_forcasting;
            $updates["has_risks"] = $request->project_has_risk;
            $updates["mtp_id"] = $request->mtp;
            if ($query->update($updates)) {

                $messageData['id'] = $query->symbol;
                $messageData['user'] = auth()->user()->name;
                $messageData['userImage'] = auth()->user()->file_name;
                $messageData['kpiname'] = '';//$model->name;
                $messageData['textshow'] = 'data updated successfully';
                $messageData['time'] = date('Y-m-d H:i:s');
                //$messageData['allowusers'] = $kpidefdata;

                $notification = new NotificationMessage();
                $notification->user = auth()->user()->name;
                $notification->userImage = auth()->user()->file_name;
                $notification->message = json_encode($messageData, true);
                $notification->save();

                $token = array(
                    'USER_NAME' => auth()->user()->name,
                    'PROJ_SYMBOL' => $query->symbol,
                    'KPI_NAME' => '',//$model->name,
                    'MESSAGE' => null,
                    'DATE_TIME' => date('Y-m-d H:i:s'),
                );
                $pattern = '[%s]';
                foreach ($token as $key => $val) {
                    $varMap[sprintf($pattern, $key)] = $val;
                }

                $sendMail = false;

                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=17 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    //$sendMail = true;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=17 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=17 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $messageRedis = [];
                $messageRedis['id'] = $notification->id;
                $messageRedis['user'] = auth()->user()->name;
                $messageRedis['userImage'] = auth()->user()->file_name;
                $messageRedis['message'] = $messageData;

                $redis = Redis::connection();
                $redis->publish('projectupdate', json_encode($messageRedis, true));

                return response()->json([
                    "code" => 200,
                    "msg" => "project_information_updated_successfully"
                ]);
            }
        }

        return response()->json(["code" => 400]);
    }

    public
    function projectreadingsdelete($projectid)
    {
        $projectreadingscheck = \DB::select(\DB::raw("select *  from proj_values where progress_val is not null and project_id=$projectid"));
        if (count($projectreadingscheck) == 0) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            $targetdel = \DB::select(\DB::raw(" Delete from proj_values where project_id=$projectid"));
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }

    }

    public
    function projectvaluechangecheck($projectid)
    {
        $projectreadingscheck = \DB::select(\DB::raw("select *  from proj_values where progress_val is not null and project_id=$projectid"));
        if (count($projectreadingscheck) == 0) {
            return [
                "code" => 200,
                'data' => true,

            ];
        } else {
            return [
                "code" => 400,
                'data' => false,
            ];
        }

    }


    public
    function projectApproveReject(Request $request)
    {

        /////// For Audit Purpose
        //$projects = new Projects();
        $projects = new Projects($request->all());
        $projects->setTable("project");
        $id = $request->get('projectid');
        $query = $projects->find($id);

        $updates["status_approval"] = $request->get('status');
	if($request->get('status') == 1) {
	    $updates["status_operational"] = 1; //$request->get('status');
	}
        // $updates["reject_reason"] = $request->get('rejectreason');
        if ($query->update($updates)) {
            $status = $request->get('status');

            $messageData['id'] = $query->symbol;
            $messageData['user'] = auth()->user()->name;
            $messageData['userImage'] = auth()->user()->file_name;
            $messageData['kpiname'] = '';//$query->name;
            $messageData['textshow'] = ($status == 1) ? 'stp approve successfully' : 'stp reject successfully';
            $messageData['time'] = date('Y-m-d H:i:s');
            //$messageData['allowusers'] = $kpidefdata;

            $notification = new NotificationMessage();
            $notification->user = auth()->user()->name;
            $notification->userImage = auth()->user()->file_name;
            $notification->message = json_encode($messageData, true);
            $notification->save();

            $token = array(
                'USER_NAME' => auth()->user()->name,
                'PROJ_SYMBOL' => $query->symbol,
                'KPI_NAME' => '',//$query->name,
                'MESSAGE' => null,
                'DATE_TIME' => date('Y-m-d H:i:s'),
            );
            $pattern = '[%s]';
            foreach ($token as $key => $val) {
                $varMap[sprintf($pattern, $key)] = $val;
            }

            if ($status == 1) {
                if ($query->start_dt_actual) {
                    $currentdate = $query->start_dt_actual;
                    // $currentdate= date("Y-m-d", strtotime($currentdate));
                    $updates["start_dt_actual"] = $currentdate;
                } else {
                    $currentdate = date("Y-m-d");
                    if ($query->start_dt_base < $currentdate) {
                        $updates["start_dt_actual"] = $currentdate;
                    } else {
                        $updates["start_dt_actual"] = $query->start_dt_base;
                    }
                }
                if ($query->end_dt_projected) {
                    $query->end_dt_base = $query->end_dt_projected;
                }
                // echo "progress";
                $updates["status_operational"] = 1;

                $query->update($updates);
                $value_periodval = \DB::select(\DB::raw("select value_period from project where `id` =$id"));
                $value_period = $value_periodval[0]->value_period;
                if ($value_period == 'D') {

                    $time = strtotime($currentdate);
                    $nextreadingdate = date("Y-m-d", strtotime("+1 day", $time));
                    if ($nextreadingdate > $query->end_dt_base) {
                        $nextreadingdate = $query->end_dt_base;
                    }

                }
                if ($value_period == 'W') {
                    $time = strtotime($currentdate);
                    $nextreadingdate = date("Y-m-d", strtotime("+1 week", $time));
                    if ($nextreadingdate > $query->end_dt_base) {
                        $nextreadingdate = $query->end_dt_base;
                    }

                }
                if ($value_period == 'M') {
//                        if( date('d') == 31 || (date('m') == 1 && date('d') > 28)){
//                            $date = strtotime('last day of next month');
//                        } else {
//                            $date = strtotime('+1 months');
//                        }
//
//
                    $time = strtotime($currentdate);
                    $nextreadingdate = date("Y-m-d", strtotime("+1 month", $time));
                    if ($nextreadingdate > $query->end_dt_base) {
                        $nextreadingdate = $query->end_dt_base;
                    }


                }
                if ($value_period == 'Q') {
                    $time = strtotime($currentdate);

                    $nextreadingdate = date('Y-m-d', strtotime('+3 months', $time));
//                        echo "end date".$query->end_dt_base;
//                        echo "====".$nextreadingdate;
                    if ($nextreadingdate > $query->end_dt_base) {
                        $nextreadingdate = $query->end_dt_base;
                    }


                }
                if ($value_period == 'Y') {
                    $nextreadingdate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($currentdate)) . " + 1 year"));
                    if ($nextreadingdate > $query->end_dt_base) {
                        $nextreadingdate = $query->end_dt_base;
                    }

                }

		$CheckProjValue = \DB::select(\DB::raw("SELECT * from proj_values where project_id=$id"));
//                    echo $nextreadingdate;
//                    die();
		if (count($CheckProjValue) == 0) {
	                DB::insert("INSERT INTO `proj_values` (`project_id`, `period_dt`,`period_no`, `created_by` ) values ( $id,'$nextreadingdate',1, '" . auth()->user()->id . "')");
		}

                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=18 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    $sendMail = false;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=18 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=18 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }
            } else if($status == 0 ) {
                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=19 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    //$sendMail = false;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=19 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {

                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    //echo $userString;
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;

                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];
                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=19 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {

                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    //echo $userString;
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;

                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];
                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }
            }

            $messageRedis = [];
            $messageRedis['id'] = $notification->id;
            $messageRedis['user'] = auth()->user()->name;
            $messageRedis['userImage'] = auth()->user()->file_name;
            $messageRedis['message'] = $messageData;

            $redis = Redis::connection();
            $redis->publish('projectupdate', json_encode($messageRedis, true));

//            $redis = Redis::connection();
//            $redis->publish('message', '');

            /* $redis1 = Redis::connection();
             $redis1->publish('dashboard', '');*/

            return response()->json([
                "code" => 200,
                "msg" => ($status == 1) ? 'project_approved' : 'project_rejected'
            ]);
        }
        return response()->json([
            "code" => 400,
            "msg" => 'failed to update status'
        ]);
    }

    public
    function projectoperationalstatus(Request $request)
    {

        /////// For Audit Purpose
        //$projects = new Projects();
        $projects = new Projects($request->all());
        $projects->setTable("project");
        $id = $request->get('projectid');
        $query = $projects->find($id);

        $updates["status_operational"] = $request->get('status');

        if($request->get('status') == '3') {
            $updates['end_dt_actual'] = date('Y-m-d');
        }
        if ($query->update($updates)) {
            $status = $request->get('status');

            $messageData['id'] = $query->symbol;
            $messageData['user'] = auth()->user()->name;
            $messageData['userImage'] = auth()->user()->file_name;
            $messageData['kpiname'] = '';//$query->name;
            $messageData['textshow'] = (($status == 1) ? 'project_switched_in_progress' : ($status == 2 ? 'project_switched_on_hold' : ($status == 3 ? 'project_closed' : '')));
            $messageData['time'] = date('Y-m-d H:i:s');
            //$messageData['allowusers'] = $kpidefdata;

            $notification = new NotificationMessage();
            $notification->user = auth()->user()->name;
            $notification->userImage = auth()->user()->file_name;
            $notification->message = json_encode($messageData, true);
            $notification->save();

            $token = array(
                'USER_NAME' => auth()->user()->name,
                'PROJ_SYMBOL' => $query->symbol,
                'KPI_NAME' => '',//$query->name,
                'MESSAGE' => null,
                'DATE_TIME' => date('Y-m-d H:i:s'),
            );
            $pattern = '[%s]';
            foreach ($token as $key => $val) {
                $varMap[sprintf($pattern, $key)] = $val;
            }

//            $redis = Redis::connection();
//            $redis->publish('message', '');

            /* $redis1 = Redis::connection();
             $redis1->publish('dashboard', '');*/
            if ($status == 2) {
                $msg = "project_switched_on_hold";
                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=21 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    $sendMail = false;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=21 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=21 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }
            }
            if ($status == 3) {
                $msg = "project_closed";
                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=22 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    $sendMail = false;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=22 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=22 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }
            }
            if ($status == 1) {
                $msg = "project_switched_in_progress";
                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=20 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    $sendMail = false;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=20 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=20 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }
            }

            $messageRedis = [];
            $messageRedis['id'] = $notification->id;
            $messageRedis['user'] = auth()->user()->name;
            $messageRedis['userImage'] = auth()->user()->file_name;
            $messageRedis['message'] = $messageData;

            $redis = Redis::connection();
            $redis->publish('projectupdate', json_encode($messageRedis, true));


            return response()->json([
                "code" => 200,
                "msg" => $msg

            ]);
        }
        return response()->json([
            "code" => 400,
            "msg" => 'failed to update status'
        ]);
    }

    public
    function projectaluesupdatelast($projectid)
    {
        // $projectid = $request->get('projectid');
        $projvaluescount = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $projectid"));
        $countvaluesdata = count($projvaluescount);
//        echo $countvaluesdata;
//        die();
        if ($countvaluesdata > 1) {

            $projvalueslist = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $projectid and progress_val!='' ORDER BY id DESC LIMIT 1"));
            //  $projvalueslist = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $projectid  ORDER BY id DESC LIMIT 1"));
        } else {
            $projvalueslist = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $projectid  ORDER BY id DESC LIMIT 1"));
        }
        if ($projvalueslist) {


            return response()->json([
                "code" => 200,
                "projvalueslist" => $projvalueslist
            ]);
        }
    }

    public
    function projectnextupdateddate($projectid)
    {

        $projnextdate = \DB::select(\DB::raw("SELECT period_dt FROM proj_values WHERE  project_id= $projectid  ORDER BY id DESC LIMIT 1"));
        $nextdate = $projnextdate[0]->period_dt;
        if ($nextdate) {


            return response()->json([
                "code" => 200,
                "nextdate" => $nextdate
            ]);
        }
    }

    public
    function readingslist($projectid)
    {

        $readingslist = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $projectid and progress_val!=''"));

        if ($readingslist) {
            $i = 0;
            foreach ($readingslist as $readlist) {
                $readingslist[$i]->period_dt = $readlist->period_dt;
                $dt = new \DateTime($readingslist[$i]->period_dt);
                $readingslist[$i]->period_dt = $dt->format('d-m-Y');
                $readingslist[$i]->progress_val = $this->round_special($readlist->progress_val * 100) . "%";
                if ($readingslist[$i]->spending_val == 0) {
                    $readingslist[$i]->spending_val = '';
                }


                $i++;
            }
            return response()->json([
                "code" => 200,
                "readingslist" => $readingslist
            ]);
        } else {
            return response()->json([
                "code" => 200,
                "readingslist" => []
            ]);
        }
    }

    public function remarkreadingslist($projectid)
    {
        $readingslist = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $projectid and progress_val!='' and justification != '' order  by period_dt desc "));

        if ($readingslist) {
            $i = 0;
            foreach ($readingslist as $readlist) {
                $createUpdate = '';
                $userInfoCreated = \DB::select(\DB::raw("SELECT * FROM `users` WHERE id='$readlist->created_by'"));
                $userInfoUpdated = \DB::select(\DB::raw("SELECT * FROM `users` WHERE id='$readlist->updated_by'"));

                if ($userInfoUpdated) {
                    $createUpdate = $userInfoUpdated[0]->name . ' ' . $userInfoUpdated[0]->last_name;;
                } else if ($userInfoCreated) {
                    $createUpdate = $userInfoCreated[0]->name . ' ' . $userInfoCreated[0]->last_name;
                }
                $readingslist[$i]->createUpdate = $createUpdate;
                $readingslist[$i]->justification = strip_tags($readlist->justification);

                $readingslist[$i]->period_dt = $readlist->period_dt;
                $dt = new \DateTime($readingslist[$i]->period_dt);
                $readingslist[$i]->period_dt = $dt->format('d-m-Y');
                $readingslist[$i]->progress_val = $this->round_special($readlist->progress_val * 100) . "%";
                if ($readingslist[$i]->spending_val == 0) {
                    $readingslist[$i]->spending_val = '';
                }


                $i++;
            }
            return response()->json([
                "code" => 200,
                "readingslist" => $readingslist
            ]);
        } else {
            return response()->json([
                "code" => 200,
                "readingslist" => []
            ]);
        }
    }

    public
    function round_special($x)
    {
        if ($x == 0) return 0;

        $rounded = round($x, 2);
        $minValue = 0.01;

        if ($rounded < $minValue) {
            return number_format($minValue, 2);
        } else {
            return number_format($rounded, 2);
        }
    }

    public
    function projectcostlist()
    {

        $projectcostlist = \DB::select(\DB::raw("SELECT role_cost.*,roles.name  FROM role_cost  INNER JOIN roles on roles.id = role_cost.role_id"));


        if ($projectcostlist) {

            $i = 0;
            foreach ($projectcostlist as $costlist) {
                if ($costlist->salary_scheme == 1)
                    $projectcostlist[$i]->salary_scheme = "General";
                elseif ($costlist->salary_scheme == 2) {
                    $projectcostlist[$i]->salary_scheme = "Financial";
                }

                $projectcostlist[$i]->salary_average = "KWD " . $costlist->salary_average;
                $projectcostlist[$i]->bonus_cost = "KWD " . $costlist->bonus_cost;


                $i++;
            }

            return response()->json([
                "code" => 200,
                "projectcostlist" => $projectcostlist
            ]);

        }
    }

    public
    function projectsvaluesstore(Request $request)
    {


//        $projectsnew = new Dynamic($request->all());
//        $projectsnew->setTable("proj_values");
        $projectid = $request->get('id');

        $data = \DB::select(\DB::raw("SELECT * FROM proj_values WHERE  project_id= $projectid  ORDER BY id DESC LIMIT 1"));
        $valueid = $data[0]->id;
        $valuedate = $data[0]->period_dt;
        $dataproject = \DB::select(\DB::raw("SELECT * FROM project WHERE  id= $projectid"));

        $dataprojectprogress_total = $dataproject[0]->progress_total;
        $dataprojectspending_total = $dataproject[0]->spending_total;
        $dataprojectbudget_projected = $dataproject[0]->budget_projected;
        if ($dataproject[0]->end_dt_projected) {
            $dataprojectendate = $dataproject[0]->end_dt_projected;
        } else {
            $dataprojectendate = $dataproject[0]->end_dt_base;
        }
        $request->curent_progress = $request->curent_progress / 100;
        $projectstab = new Projects($request->all());
        $projectstab->setTable("project");
        $projectid = $request->get('id');
        $queryproject = $projectstab->find($projectid);
        $dataprojectprogress_total = $dataproject[0]->progress_total;
        if ($dataprojectprogress_total == 1) {
            return response()->json(["code" => 400]);
        }

//
        // $updates["last_reported_progress"] = $request->last_reported_progress;
        //$updates["progress_val"] = $request->curent_progress+$request->last_reported_progress;
        $change_justification = null;
        $spending_det = "NULL";
        $spending_val = "NULL";
        $projected_budget = "NULL";
        $spending_det = "NULL";
        $projected_end_date = "NULL";
        $change_justification = "NULL";
        $spending_det = "NULL";
        $periodno = $request->periodno;
        $progress_change = $request->progress_change;
        $duration_change = $request->duration_change;
        if ($request->spending_val) {
            $spending_val = $request->spending_val;
        }

        $updates["progress_val"] = $request->curent_progress;
        if ($request->change_justification) {
            $change_justification = "'" . $request->change_justification . "'";
        }
        if ($request->projected_end_date) {
            $projected_end_date = $request->projected_end_date;
            $dataprojectendate = date("Y-m-d", strtotime("$projected_end_date"));
            $projected_end_date = "'" . date("Y-m-d", strtotime("$projected_end_date")) . "'";

        }
        if ($request->spending_details) {
            $spending_det = "'" . $request->spending_details . "'";
        }
        if ($request->projected_budget) {
            $projected_budget = "'" . $request->projected_budget . "'";
        }

        $inconsistentprogress = $request->inconsistentprogress;
        $Tabledataresult = \DB::select(\DB::raw("update proj_values set cost_projected_val=$projected_budget,end_dt_projected_val=$projected_end_date,progress_val=$request->curent_progress,spending_val=$spending_val,spending_det=$spending_det,justification=$change_justification,progress_inconsistent=$inconsistentprogress, updated_by = '" . auth()->user()->id . "' where id=$valueid"));

        $spending_total = $request->spending_total + $request->spending_val;

        //$progress_total = $request->curent_progress + $dataprojectprogress_total;
        $progress_total = $request->curent_progress;

        $dataprojectspending_total = $dataproject[0]->spending_total;
        $dataprojectbudget_projected = $dataproject[0]->budget_projected;

        // $updatesproj["progress_total"] = $dataprojectprogress_total+$progress_total;
        $updatesproj["progress_total"] = $progress_total;
        $updatesproj["spending_total"] = $spending_total;
        if ($request->projected_budget) {
            $projected_budget = $request->projected_budget;
            $updatesproj["budget_projected"] = $projected_budget + $dataprojectbudget_projected;

        }
        if ($request->projected_end_date) {
            $projected_end_date = $request->projected_end_date;
            $projected_end_date = date("Y-m-d", strtotime("$projected_end_date"));
            $updatesproj["end_dt_projected"] = $projected_end_date;

        }
        if ($request->spending_val) {
            $spending_val = $request->spending_val;
            $updatesproj["spending_total"] = $spending_val + $dataprojectspending_total;
        }


        if ($queryproject->update($updatesproj)) {

            $value_periodval = \DB::select(\DB::raw("select value_period,status_operational from project where `id` =$projectid"));
            $value_period = $value_periodval[0]->value_period;
            $status_operational = $value_periodval[0]->status_operational;

            if ($status_operational != 3 && $progress_total < 1) {
                //$carbon = Carbon::today();

                // $timestamp = date('Y-m-d');
                $timestamp = $valuedate;

                if ($value_period == 'D') {

                    $time = strtotime($timestamp);
                    $nextreadingdate = date("Y-m-d", strtotime("+1 day", $time));
                    if ($nextreadingdate > $dataprojectendate) {
                        $nextreadingdate = $dataprojectendate;
                    }

                }
                if ($value_period == 'W') {
                    $time = strtotime($timestamp);
                    $nextreadingdate = date("Y-m-d", strtotime("+1 week", $time));
                    if ($nextreadingdate > $dataprojectendate) {
                        $nextreadingdate = $dataprojectendate;
                    }

                }
                if ($value_period == 'M') {
                    $time = strtotime($timestamp);
                    $nextreadingdate = date("Y-m-d", strtotime("+1 month", $time));
                    if ($nextreadingdate > $dataprojectendate) {
                        $nextreadingdate = $dataprojectendate;
                    }
                }
                if ($value_period == 'Q') {
                    $time = strtotime($timestamp);

                    $nextreadingdate = date('Y-m-d', strtotime('+3 months', $time));
                    if ($nextreadingdate > $dataprojectendate) {
                        $nextreadingdate = $dataprojectendate;
                    }

                }
                if ($value_period == 'Y') {
                    $nextreadingdate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($timestamp)) . " + 1 year"));
                    if ($nextreadingdate > $dataprojectendate) {
                        $nextreadingdate = $dataprojectendate;
                    }

                }

                $periodnoval = \DB::select(\DB::raw("SELECT period_no,period_dt FROM proj_values WHERE  project_id= $projectid  ORDER BY id DESC LIMIT 1"));
                $period_no = $periodnoval[0]->period_no + 1;
                $period_date = $periodnoval[0]->period_dt;
                $period_date = date("Y-m-d", strtotime($period_date));

                if ($period_date != $nextreadingdate) {
                    DB::insert("INSERT INTO `proj_values` (`project_id`, `period_dt`,`period_no`, `created_by` ) values ( $projectid,'$nextreadingdate',$period_no, '" . auth()->user()->id . "')");
                }
            }
            return response()->json([
                "code" => 200,
                "msg" => "data updated successfully"
            ]);
        }


        return response()->json(["code" => 400]);

    }


    public
    function subtenanttree($id)
    {

        $tenants1 = SubTenant::with('tree')->Where('parent_id', '<>', 0)->Where('parent_id', '<>', 1)->Where('parent_id', '<>', 2)->Where('parent_id', '<>', 3)->Where('parent_id', '<>', null)->whereNotNull('id')->Where('parent_id', $id)->get();


        $tenants = SubTenant::with('children')->orWhere('parent_id', $id)
            ->get();
        $i = 0;
        $j = 0;
        if ($tenants) {

            foreach ($tenants as $tenant) {
                $tenants[$i]['id'] = $tenant->id;
                $tenants[$i]['label'] = $tenant->name;

                $i = $i + 1;
            }
            $j = 0;
//            foreach ( $tenants1 as $k=>$v )
//            {
//                $tenants1[$k] ['tree'] = $tenants1[$k] ['children'];
//                unset($tenants1[$k]['tree']);
//            }
//            if ($tenants1) {

//                foreach($tenants1 as $tenant1){
//                    $tenants1[$j]['id']=$tenant1->id;
//                    $tenants1[$j]['label']=$tenant1->name;
//                   // $tenantsarray[][$j]['label']= $tenants1[$j]['label'];
//                    $tenants1[$j]['children']=$tenant1->tree;
////                    unset($tenants1[$j]['tree']);
//
//
//                    // $tenantsarray[][]['children']=$tenants1[$j]['children'];
//
//                    $j=$j+1;
//                }
//            }
//var_dump($tenantsarray);
//            die();
            $j = 0;
            foreach ($tenants1 as $tenant12) {
                $tenants1[$j]['id'] = $tenant12->id;
                $tenants1[$j]['label'] = $tenant12->name;

                $j = $j + 1;
            }
//            $tenants1 = array_map('array_filter', $tenants1);
//            $tenants1 = array_filter($tenants1);
            //    $tenants1=array_filter($tenants1);

            return response()->json([
                "code" => 200,
                "subTenants" => $tenants1,
                "subTenantsdept" => $tenants
            ]);
        }

        return response()->json(["code" => 400]);
    }


    public
    function processList(array $list)
    {
        $listResult = ['keepValue' => false, // once set true will propagate upward
            'value' => []];

        foreach ($list as $name => $item) {
            if (is_null($item)) { // see is_scalar test
                continue;
            }

            if (is_scalar($item)) { // keep the value?
                if (!empty($item) || strlen(trim($item)) > 0) {
                    $listResult['keepValue'] = true;
                    $listResult['value'][$name] = $item;
                }

            } else { // new list... recurse

                $itemResult = processList($item);
                if ($itemResult['keepValue']) {
                    $listResult['keepValue'] = true;
                    $listResult['value'][$name] = $itemResult['value'];
                }
            }
        }
        return $listResult;
    }

    public
    function loadprojectmanagerDefaultData(Request $request)
    {


        $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = 2 UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), 2, '', CONCAT(id, '') from subtenant where id = 2) select id, name as label, name, parent_id from cte order by path"));
        $result = array_map(function ($value) {
            return (array)$value;
        }, $rows);

        // Group by parent id
        $treeArrayGroups = [];
        foreach ($result as $record) {
            $treeArrayGroups[$record['parent_id']][] = $record;
        }
        // Get the root
        $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
        // Transform the data
        $outputTree = $this->transformTree($treeArrayGroups, $rootArray);
        $dom = [];
        for ($i = 1; $i <= 31; $i++) {
            $dom[] = $i;
        }
        $data = [];
        $data[] = $outputTree;
        $roles = Role::get();
        return response()->json([
            "code" => 200,
            "roles" => $roles,
            "dom" => $dom,
            "sectorspm" => $data
        ]);
    }

    function transformTree($treeArrayGroups, $rootArray)
    {
        // Read through all nodes where parent is root array
        foreach ($treeArrayGroups[$rootArray['id']] as $child) {
            //echo $child['id'].PHP_EOL;
            // If there is a group for that child, aka the child has children
            if (isset($treeArrayGroups[$child['id']])) {
                // Traverse into the child
                $newChild = $this->transformTree($treeArrayGroups, $child);
            } else {
                $newChild = $child;
            }

            if ($child['id'] != '') {
                // Assign the child to the array of children in the root node
                $rootArray['tree'][] = $newChild;
            }
        }
        return $rootArray;
    }

    public
    function loadstrategicDefaultData(Request $request)
    {
        $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = 2 UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), 2, '', CONCAT(id, '') from subtenant where id = 2) select id, name as label, name, parent_id from cte order by path"));
        $result = array_map(function ($value) {
            return (array)$value;
        }, $rows);
        $roles = Role::get();

        // Group by parent id
        $treeArrayGroups = [];
        foreach ($result as $record) {
            $treeArrayGroups[$record['parent_id']][] = $record;
        }
        // Get the root
        $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
        // Transform the data
        $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

        $data = [];
        $data[] = $outputTree;

        return response()->json([
            "code" => 200,
            "roles" => $roles,
            "sectorsstr" => $data
        ]);
    }

    public
    function loadKpiOrgUserspm($orgUnit, $sector = null)
    {
        if ($orgUnit != 'null' && $orgUnit != 'undefined') {

            $users = \DB::select(\DB::raw("WITH RECURSIVE cte (level1_id, id, parent_id, subtenant_type, name, path) AS (
	-- This is end of the recursion: Select low level
	select id, id, parent_id, subtenant_type_id, name, concat( cast(id as char(200)), '_')
		from subtenant where
        id = $orgUnit -- set your arg here
	UNION ALL
    -- This is the recursive part: It joins to cte
    select c.level1_id, s.id, s.parent_id, s.subtenant_type_id, s.name, CONCAT(c.path, ',', s.id)
		from subtenant s
        inner join cte c on s.parent_id = c.id
	)
	-- select id, name, subtenant_type, parent_id
	--  cte.level1_id, cte.id, cte.parent_id, cte.subtenant_type,
	select cte.name as subname, u.*
	from cte, users u where
	u.subtenant_id = cte.id
	and u.deleted_at is null
	order by path, u.name;"));
        } else {
            $users = [];
        }
        return response()->json([
            "code" => 200,
            "data" => $users
        ]);
    }

    public
    function loadKpiOrgUsersstr($orgUnit, $sector = null)
    {
        if ($orgUnit != 'null' && $orgUnit != 'undefined') {

            $users = \DB::select(\DB::raw("WITH RECURSIVE cte (level1_id, id, parent_id, subtenant_type, name, path) AS (
	-- This is end of the recursion: Select low level
	select id, id, parent_id, subtenant_type_id, name, concat( cast(id as char(200)), '_')
		from subtenant where
        id = $orgUnit -- set your arg here
	UNION ALL
    -- This is the recursive part: It joins to cte
    select c.level1_id, s.id, s.parent_id, s.subtenant_type_id, s.name, CONCAT(c.path, ',', s.id)
		from subtenant s
        inner join cte c on s.parent_id = c.id
	)
	-- select id, name, subtenant_type, parent_id
	--  cte.level1_id, cte.id, cte.parent_id, cte.subtenant_type,
	select cte.name as subname, u.*
	from cte, users u where
	u.subtenant_id = cte.id
	and u.deleted_at is null
	order by path, u.name;"));
        } else {
            $users = [];
        }
        return response()->json([
            "code" => 200,
            "data" => $users
        ]);
    }

    public
    function getorgunitname($orgUnit)
    {
        $orgnameval = \DB::select(\DB::raw("select name from subtenant where `id` =$orgUnit"));
        $orgname = $orgnameval[0]->name;
        return response()->json([
            "code" => 200,
            "orgname" => $orgname,

        ]);
    }

    public
    function saveprojectteam(Request $request)
    {


        $projectsnew = new Projects($request->all());
        $projectsnew->setTable("project");
        $id = $request->get('project_id');

        $query = $projectsnew->find($id);
        if ($request->project_managed_by == 0) {
            $request->project_managed_by = "i";
        }
        if ($request->project_managed_by == 1) {
            $request->project_managed_by = "o";
        }

        $updates["proj_mgmt_by"] = $request->project_managed_by;
        $updates["proj_owner"] = $request->project_owner;

        if ($request->project_managed_by == 'i') {
            $updates["proj_mgr"] = $request->project_manager;
            $updates["proj_mgr_ext"] = "NULL";
        }
        if ($request->project_managed_by == 'o') {
            $updates["proj_mgr_ext"] = $request->proj_mgr_ext;
            // $updates["proj_mgr"]="";
        }
        $updates["proj_coord_by"] = $request->project_coordinator;
        $updates["proj_comm_officer"] = $request->communication_officer;
        $updates["has_team"] = $request->project_has_team;
        $updates["proj_mgr_util"] = $request->proj_mgr_util;
        $updates["proj_coord_util"] = $request->proj_coord_util;
        $updates["proj_comm_officer_util"] = $request->proj_comm_officer_util;
        $updates["proj_mgr_perf"] = $request->proj_mgr_perf;
        $updates["proj_coord_perf"] = $request->proj_coord_perf;
        $updates["proj_comm_officer_perf"] = $request->proj_comm_officer_perf;


        if ($query->update($updates)) {

            if ($request->rowDatateam) {
                foreach ($request->rowDatateam as $projteam) {

                    $projectid = $request->get('project_id');
                    $projectname = $projteam['team_name'];
                    if ($projteam['team_id']) {
                        $team_id = $projteam['team_id'];
                    }
                    $work_description = $projteam['work_description'];
                    $from_date = date('yy-m-d', strtotime($projteam['from_date']));
                    $to_date = date('yy-m-d', strtotime($projteam['to_date']));
                    $team_approval = $projteam['team_approval'];
                    $team_approval_ref = $projteam['team_approval_ref'];
                    $proj_team_perf = $projteam['proj_team_perf'];
                    if ($team_approval == 'Yes' || $team_approval == 'نعم') {
                        $team_approval = 1;
                    }
                    if ($team_approval == 'No' || $team_approval == 'لا') {
                        $team_approval = 0;
                    }

                    $created_by = $request->created_by;

                    // DB::insert("INSERT INTO `proj_team` (by_user,project_id,name,work_description,
                    //status_approval,approval_ref) values ( $created_by,$projectid,'$projectname','$work_description',$team_approval,'$team_approval_ref')");
                    if ($projteam['team_id']) {
                        $teamexists = \DB::select(\DB::raw("select id from proj_team where `id` ='$team_id' and project_id=$projectid"));
                        {
                            if (count($teamexists) != 0) {
                                $teamexistsid = $teamexists[0]->id;
                            }
                        }
                    }
                    if (isset($teamexistsid)) {
                        $teamid = $teamexistsid;
                        $updateteam = \DB::select(\DB::raw("update proj_team set name='$projectname',work_description='$work_description',status_approval=$team_approval,approval_ref='$team_approval_ref' where project_id=$projectid and `id` =$team_id"));
                        $updateteamdurationids = \DB::select(\DB::raw("select id from  proj_team_duration  where team_id=$teamid"));
                        $teamdurationid = $updateteamdurationids[0]->id;
                        $updateteamduration = \DB::select(\DB::raw("update proj_team_duration set start_dt='$from_date',end_dt='$to_date',team_perf='$proj_team_perf' where id=$teamdurationid"));
                        $rolecost = \DB::table("proj_team_member")->Where('team_duration_id', $teamdurationid)->delete();
                    } elseif (!$projteam['team_id'] && !isset($teamexistsid)) {

                        $teamid = DB::table('proj_team')->insertGetId(['by_user' => $created_by, 'project_id' => $projectid, 'name' => $projectname, 'work_description' => $work_description, 'status_approval' => $team_approval, 'approval_ref' => $team_approval_ref]);
                        $teamdurationid = DB::table('proj_team_duration')->insertGetId(['team_id' => $teamid, 'start_dt' => $from_date, 'end_dt' => $to_date, 'team_perf' => $proj_team_perf]);

                    }
                }
//                var_dump($request->rowDatateammembers);
//                die();
                foreach ($request->rowDatateammembers as $projteammember) {
                    $which_team = $projteammember['which_team'];
                    $user_ext = "NULL";
                    $user = "NULL";
                    $userrole = "NULL";
                    $user_utilization = 0;
                    $teamval = \DB::select(\DB::raw("select id from proj_team where `name` ='$which_team' and project_id=$projectid"));
                    //$teamid = $teamval[0]->id;

                    $user = $projteammember['user'];
                    $userrole = $projteammember['role'];
                    if ($user_utilization) {
                        $user_utilization = $projteammember['user_utilization'];
                    }
                    $engagement_description = $projteammember['engagement_description'];
                    $efforts = $projteammember['efforts'];
                    $userid = $projteammember['userid'];
                    $user_performance = $projteammember['user_performance'];


                    DB::insert("INSERT INTO `proj_team_member` (team_duration_id,user,user_ext,user_role,role_description,user_utilization,user_perf) values ( $teamdurationid,$userid,'$user_ext','$userrole','$engagement_description',$user_utilization,$user_performance)");


                }
            }
            return response()->json([
                "code" => 200,
                "msg" => "data updated successfully"
            ]);
        }


    }

    public
    function projectcoststore(Request $request)
    {

        if (empty($request->id) || !isset($request->id) || $request->id === 0) {


            $projectscost = new Projectscost();

            $projectscost->role_id = $request->userrole;
            $projectscost->salary_scheme = $request->salaryscheme;
            $projectscost->salary_average = $request->average_salary;
            $projectscost->bonus_cost = $request->bonus_cost;


            if ($projectscost->save()) {
                return response()->json([
                    "code" => 200,
                    "msg" => "data inserted successfully"
                ]);
            }
        } else {

            $projectsnewcost = new Projectscost($request->all());
            $projectsnewcost->setTable("role_cost");
            $id = $request->get('id');

            $query = $projectsnewcost->find($id);

            $updates["salary_scheme"] = $request->salaryscheme;
            $updates["salary_average"] = $request->average_salary;
            $updates["bonus_cost"] = $request->bonus_cost;


            if ($query->update($updates)) {

                return response()->json([
                    "code" => 200,
                    "msg" => "data updated successfully"
                ]);
            }
        }

        return response()->json(["code" => 400]);
    }

    public
    function projectcostbyId($id)
    {

        $projectcostlist = \DB::select(\DB::raw("SELECT role_cost.*,roles.id as roleid  FROM role_cost  INNER JOIN roles on roles.id = role_cost.role_id where role_cost.role_id=$id"));
        $i = 0;
        if ($projectcostlist) {
            foreach ($projectcostlist as $costlist) {

                $projectcostlist[$i]->salary_average = "KWD " . $costlist->salary_average;
                $projectcostlist[$i]->bonus_cost = "KWD " . $costlist->bonus_cost;


                $i++;
            }

            return response()->json([
                "code" => 200,
                "projectcostlist" => $projectcostlist
            ]);
        }
    }

    public
    function loadprojectteamgetbyid($id)
    {
        $projectteamlist = $projectteammagrlevel = $projectteamdurationlist = $projectteammeberslist = [];

        $projectteamlist = \DB::select(\DB::raw("SELECT proj_team.*,proj_team_duration.start_dt,proj_team_duration.end_dt FROM proj_team inner join proj_team_duration on  proj_team_duration.team_id=proj_team.id where proj_team.project_id=$id"));

        $projectteammagrlevel = \DB::select(\DB::raw("SELECT * from project where id=$id"));


        $i = 0;
        if ($projectteammagrlevel) {
            foreach ($projectteammagrlevel as $managerdata) {
                $projectteammagrlevel[$i]->project_ownerid = $managerdata->proj_owner;
                $projectteammagrlevel[$i]->project_owner = $this->getusernames($managerdata->proj_owner);

                if ($projectteammagrlevel[$i]->proj_mgmt_by === "i") {
                    $projectteammagrlevel[$i]->proj_mgmt_by = 0;
                }

                if ($projectteammagrlevel[$i]->proj_mgmt_by === "o") {
                    $projectteammagrlevel[$i]->proj_mgmt_by = 1;
                }

                if ($projectteammagrlevel[$i]->proj_mgmt_by == 1) {
//                    $projectteammagrlevel[$i]->proj_mgr_extid = $managerdata->proj_mgr_ext;
                    $projectteammagrlevel[$i]->proj_mgr = "";
                    $projectteammagrlevel[$i]->proj_mgr_ext = $managerdata->proj_mgr_ext;
                }
                if ($managerdata->proj_coord_by) {
                    $projectteammagrlevel[$i]->proj_coord_byid = $managerdata->proj_coord_by;
                    $projectteammagrlevel[$i]->proj_coord_by = $this->getusernames($managerdata->proj_coord_by);
                }

                $projectteammagrlevel[$i]->proj_comm_officerid = $managerdata->proj_comm_officer;
                $projectteammagrlevel[$i]->proj_comm_officer = $this->getusernames($managerdata->proj_comm_officer);

                if ($projectteammagrlevel[$i]->proj_mgmt_by == 0) {
                    $projectteammagrlevel[$i]->proj_mgr_ext = "";
                    $projectteammagrlevel[$i]->proj_mgrid = $managerdata->proj_mgr;
                    $projectteammagrlevel[$i]->proj_mgr = $this->getusernames($managerdata->proj_mgr);
                }
                $i++;
            }
        }

        if ($projectteamlist) {
            foreach ($projectteamlist as $projectteamlistval) {
                // echo $projectteamlistval->id;
                $projectteamdurationlist = \DB::select(\DB::raw("SELECT * from  proj_team_duration where team_id=$projectteamlistval->id"));

//                echo "SELECT * from  proj_team_duration where team_id=$projectteamlistval->id";
//                echo "----";
            }
        }
        // var_dump($projectteamdurationlist);
        if ($projectteamdurationlist) {
            foreach ($projectteamdurationlist as $projectteamdurationlistval) {
                $projectteammeberslist = \DB::select(\DB::raw("SELECT * from  proj_team_member where team_duration_id=$projectteamdurationlistval->id"));

                $projectteamidval = \DB::select(\DB::raw("SELECT team_id from  proj_team_duration where id=$projectteamdurationlistval->id"));
                $which_team = $projectteamidval[0]->team_id;


            }
        }
        $j = 0;

        if ($projectteammeberslist) {
            foreach ($projectteammeberslist as $projectteammbr) {
                $projectteammeberslist[$j]->username = $this->getusernames($projectteammbr->user);
                //$projectteammeberslistdata=$this->selectUserteamdata($projectteammbr->user);
                $user = User::join('subtenant', 'users.subtenant_id', '=', 'subtenant.id')->select('users.id', 'users.name', 'users.last_name', 'subtenant.name as sub_name', 'users.sector_id', 'users.salary_scheme', 'users.subtenant_id', 'users.email')->where('users.id', $projectteammbr->user)->get();
                $i = 0;


                $getRole = \DB::table('model_has_roles')->select('role_id')->where('model_id', $projectteammbr->user)->get();

                $user[$i]->roleid = $getRole[0]->role_id;
                $getcost = \DB::table('role_cost')->select('salary_average', 'bonus_cost')->where('role_id', $user[$i]->roleid)->where('salary_scheme', $user[$i]->salary_scheme)->get();

                $projectteammeberslist[$j]->salary_average = $getcost[0]->salary_average;
                $projectteammeberslist[$j]->bonus_cost = $getcost[0]->bonus_cost;
                $projectteammeberslist[$j]->orgname = $user[$i]->sub_name;
                $projectteammeberslist[$j]->salary_scheme = $user[$i]->salary_scheme;
                $projectteammeberslist[$j]->orgunitname = $user[$i]->sub_name;
                $projectteammeberslist[$j]->which_team = $which_team;


                $j++;
            }
        }

        if ($projectteamlist || $projectteammagrlevel) {


            return response()->json([
                "code" => 200,
                "projectteamlist" => $projectteamlist,
                "projectteammagrlevel" => $projectteammagrlevel,
                "projectteammeberslist" => $projectteammeberslist
            ]);
        }
    }

    public
    function getnames($id)
    {
        $subnamevalue = \DB::select(\DB::raw("SELECT name from subtenant where id=$id"));
        $subname = $subnamevalue[0]->name;
        return $subname;

    }

    public
    function getusernames($id)
    {
        if ($id) {
            $usernamevalue = \DB::select(\DB::raw("SELECT name,last_name from users where id=$id"));
            $username = $usernamevalue[0]->name . ' ' . $usernamevalue[0]->last_name;
            return $username;
        }
        return false;

    }

    public
    function loadprojectteammembergetbyid($id)
    {

        $projectteamlist = \DB::select(\DB::raw("SELECT * from project_team where project_id=$id"));

        if ($projectteamlist) {

            return response()->json([
                "code" => 200,
                "projectteamlist" => $projectteamlist
            ]);
        }
    }

    public
    function destroy($id)

    {
        $rolecost = \DB::table("role_cost")->Where('role_id', $id)->delete();
        // $prctype = \DB::table("objective")->Where('id', $id)->delete();
// var_dump($id);
        if (!$rolecost) {
            return response()->json([
                "code" => 404,
                // "msg" => "not deleted"//"لا يمكن حذف طلب الإجازة"
            ]);
        }

        // $prctype->delete();

        return response()->json([
            "code" => 200,
            // "msg" =>"deleted" //"تم حذف طلب الإجازة"
        ]);


    }


    public
    function selectUserteamdata($id)
    {
        $user = User::join('subtenant', 'users.subtenant_id', '=', 'subtenant.id')->select('users.id', 'users.name', 'users.last_name', 'subtenant.name as sub_name', 'users.sector_id', 'users.salary_scheme', 'users.subtenant_id', 'users.email')->where('users.id', $id)->get();
        $i = 0;
        foreach ($user as $use) {
            $userIds[] = $use->id;
            $user[$i]->is_id = $use->id;
            $user[$i]->noti_id = $use->id;
            $user[$i]->subtenant_id = (int)$use->subtenant_id;
            $user[$i]->outOfRole = true;
            $getSub = \DB::table('subtenant')->where('id', $use->sector_id)->get();
            $user[$i]->sector_name = $getSub[0]->name;
            $getRole = \DB::table('model_has_roles')->select('role_id')->where('model_id', $use->id)->get();
            $user[$i]->roleid = $getRole[0]->role_id;
            $getcost = \DB::table('role_cost')->select('salary_average', 'bonus_cost')->where('role_id', $user[$i]->roleid)->where('salary_scheme', $use->salary_scheme)->get();
            $user[$i]->salary_average = $getcost[0]->salary_average;
            $user[$i]->bonus_cost = $getcost[0]->bonus_cost;

            $i++;
        }
        return response()->json([
            "code" => 200,
            "user" => $user
        ]);
    }

    public
    function selectUserteam($id)
    {

        $user = User::join('subtenant', 'users.subtenant_id', '=', 'subtenant.id')->select('users.id', 'users.name', 'users.last_name', 'subtenant.name as sub_name', 'users.sector_id', 'users.salary_scheme', 'users.subtenant_id', 'users.email')->where('users.id', $id)->get();
        $i = 0;
        foreach ($user as $use) {
            $userIds[] = $use->id;
            $user[$i]->is_id = $use->id;
            $user[$i]->noti_id = $use->id;
            $user[$i]->subtenant_id = (int)$use->subtenant_id;
            $user[$i]->outOfRole = true;
            $getSub = \DB::table('subtenant')->where('id', $use->sector_id)->get();
            $user[$i]->sector_name = $getSub[0]->name;
            $getRole = \DB::table('model_has_roles')->select('role_id')->where('model_id', $use->id)->get();
//            var_dump($getRole);
            $user[$i]->roleid = $getRole[0]->role_id;
            $getcost = \DB::table('role_cost')->select('salary_average', 'bonus_cost')->where('role_id', $user[$i]->roleid)->where('salary_scheme', $use->salary_scheme)->get();
//           var_dump($getcost);
//           die();
            $user[$i]->salary_average = $getcost[0]->salary_average;
            $user[$i]->bonus_cost = $getcost[0]->bonus_cost;
            $i++;
        }

        return response()->json([
            "code" => 200,
            "user" => $user
        ]);

    }

    public
    function checkteamname($teamname)
    {

        $projectteamname = \DB::select(\DB::raw("SELECT *   FROM proj_team   where name='$teamname'"));

        if (count($projectteamname) == 0) {

            $count = 0;
        } else {
            $count = 1;
        }

        return response()->json([
            "code" => 200,
            "count" => $count
        ]);

    }

    public
    function projectdashboardreport(Request $request)
    {
        $stp = NULL;
        $filscalyear = '';
        $project_list = $projectcat = $orgunit = $projecttype = $projecthasbuget = '';
        $projectcat = $request->my_search['category'];

        $orgunit = $request->my_search['sortBySubTenant'];
        $mtp = $request->my_search['sortByMtp'];
        if ($request->my_search['sortByStp'] != null) {
            $stp = $request->my_search['sortByStp'];

        }

        $projectop = $request->my_search['sortByprojectop'];
        $projectschedulearray = $request->my_search['sortByprojectschedule'];
        $projectsbudgetarray = $request->my_search['sortByprojectbudgetstatus'];

        $budgetstatus = $request->my_search['sortByprojectbudgetstatus'];
        $projecttype = $request->my_search['type'];
        $projecthasbuget = $request->my_search['sortByhasbuget'];
        if (!empty($stp)) {
            $fiscaldata = \DB::select(\DB::raw("select id,start_date,end_date from fiscal_year where id=$stp"));
            $fiscalstart = $fiscaldata[0]->start_date;
            $fiscalend = $fiscaldata[0]->end_date;
//        $fiscalstart->format('d/m/Y');
            $fiscalstart = date("d/m/Y", strtotime($fiscalstart));
            $fiscalend = date("d/m/Y", strtotime($fiscalend));
            $filscalyear = $fiscalstart . "   -   " . $fiscalend;
        }
        // else {
//            $filscalyear = "all";
//        }

        $wherecat = "";
        $whereorgunit = "";
        $wherehasbudget = "";
        $wherestp = "";
        $wheretype = "";
        $whereprojectop = "";
        $whereprojectschedule = "";
        $wherebudgetstatus = "";
        $totalprojectcount = $performingprojectcount = $nonperformingprojectcount = $projectbestperforming = $projectleastperforming = 0;
        $debugprog = \DB::select(\DB::raw("select value  from system_vars where name='show_<0_prog'"));
        $debugperf = \DB::select(\DB::raw("select value  from system_vars where name='show_>100_perf'"));

        $debugmodeprog = $debugprog[0]->value;
        $debugmodeperf = $debugperf[0]->value;
        /*if (!empty($orgunit) && $orgunit != null && $orgunit != 'undefined') {
            $subtypes = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$orgunit"));
            $subtype = $subtypes[0]->subtenant_type_id;
            if ($subtype == 2 or $subtype == 3) {
                $whereorgunit = " and   p.sector_id=$orgunit";
            } else {
                $whereorgunit = " and  p.subtenant_id=$orgunit";

            }
        }*/


        if (!empty($orgunit)) {
            $subtypes = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$orgunit"));
            $subtype = $subtypes[0]->subtenant_type_id;
            if ($subtype == 2 or $subtype == 3) {
                $whereorgunit = " and   p.sector_id in ($orgunit)";
            } else {
                $whereorgunit = " and  p.subtenant_id in ($orgunit)";

            }
        } else {
            //if (auth()->user()->hasRole(['Admin'])) {
            if(auth()->user()->currentRole == 'Admin') {
                $orgunit = 1;
                $whereorgunit = "";
            } else {
                $getSectors = [];
                $getSec = User::select('subtenant_id')->where('id', auth()->user()->id)->get();
                $getSectors[] = ($getSec[0]['subtenant_id']);
                $roles = [];
                foreach (auth()->user()->roles as $role) {
                    $roles[$role->id] = $role->id;
                }
                sort($roles);
                $getRole = Role::where('name', auth()->user()->currentRole)->pluck('id')->all();
                /*$workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $roles[0] . "'"));*/
                $workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $getRole[0] . "'"));
                $getBehalfSector = [];
                if (count($workbelf)) {
                    foreach ($workbelf as $behalf) {
                        $getBehalfSector[] = $behalf->subtenant_id;
                    }
                }

                /*if (count(array_merge($getSectors, $getBehalfSector)) == 1) {
                    $orgunit = implode(",", array_merge($getSectors, $getBehalfSector));
                    $whereorgunit = " and p.subtenant_id in ($orgunit)";
                } else {
                    $orgunit = implode(",", array_merge($getSectors, $getBehalfSector));
                    $whereorgunit = " and p.subtenant_id in ($orgunit)";

                }*/
                if (count(array_merge($getSectors, $getBehalfSector)) == 1) {
                    $orgunit = implode(",", array_merge($getSectors, $getBehalfSector));
                    $whereorgunit = " and (p.sector_id in ($orgunit) OR p.subtenant_id in ($orgunit))";
                } else {
                    $orgunit = implode(",", array_merge($getSectors, $getBehalfSector));
                    $whereorgunit = " and (p.sector_id in ($orgunit) OR p.subtenant_id in ($orgunit))";

                }
            }
        }


        if ($projectcat != null || $projectcat === 0) {
            $projectcat = implode(',', $projectcat);
            $wherecat = " and  p.project_category IN($projectcat)";
        }


        if ($projecttype != null || $projecttype === 0) {
            $projecttype = implode(',', $projecttype);
            $wheretype = " and  p.project_type IN($projecttype)";

        }
//        if ($project_list != null || $project_list === 0) {
//            $project_list = implode(',', $project_list);
//            $wherelist = " and  p.id IN($project_list)";
//
//        }
        if ($projecthasbuget != null || $projecthasbuget === 0) {

            $wherehasbudget = " and  p.has_budget =$projecthasbuget";

        }

        $whereprojectop = "";
        $whereprojectschedule = "";
        $wherebudgetstatus = "";
        if ($projectop != null || $projectop === 0) {
            $projectop = implode(',', $projectop);
            $whereprojectop = " and  p.status_operational IN($projectop)";

        }
        if ($projectschedulearray != null || $projectschedulearray === 0) {

            foreach ($projectschedulearray as $projectscheduleval) {

                if ($projectscheduleval == 0) {

                    $whereprojectschedule = " and  pvs.b_sv_sched_var =0";
                }

                if ($projectscheduleval == 1) {

                    $whereprojectschedule = " and  pvs.b_sv_sched_var >0";
                }
                if ($projectscheduleval == 2) {

                    $whereprojectschedule = " and  pvs.b_sv_sched_var <0";
                }


            }


        }
        if ($projectsbudgetarray != null || $projectsbudgetarray === 0) {

            foreach ($projectsbudgetarray as $projectsbudgetval) {

                if ($projectsbudgetval == 0) {

                    $wherebudgetstatus = " and  pvs.b_cv_cost_var =0";
                }

                if ($projectsbudgetval == 1) {

                    $wherebudgetstatus = " and  pvs.b_cv_cost_var >0";
                }
                if ($projectsbudgetval == 2) {

                    $wherebudgetstatus = " and  pvs.b_cv_cost_var <0";
                }


            }


        }


        $ministrycount = $developmentcount = 0;


        if (!isset($stp)) {
            $stp = 'null';
        }

        // echo $whereprojectop;

        $statement = \DB::statement("SET  @tenant_id = 1");
        $statement = \DB::statement("SET  @mtp = $mtp");
        $statement = \DB::statement("SET  @fiscal_year = $stp");
        $statement = \DB::statement("SET  @subtenant_id = null");


//        $projectlisttable = \DB::select(\DB::raw("select p.*,p.id as id, p.symbol as symbol, p.name_short, sub.name as resp_org_unit, p.sector_id, p.subtenant_id,
//						p.start_dt_actual, p.end_dt_base, pvs.pd_planned_duration, pv.progress_val as proj_prog,
//						p.budget_base,
//						if(p.has_budget=1, pvs.b_spi, pvs.spi) as spi, pvs.b_cpi,
//						pvs.sv_sched_var_pct, pvs.sv_sched_var_days, pvs.b_sv_sched_var, p.spending_total as actual_cost,
//						pvs.b_cv_cost_var as cost_variance,
//						pvs.proj_perf as proj_perf, pvs.proj_status,
//						ps.name as proj_status_name, ps.color_code as proj_status_color
//from 		mtp, fiscal_year fys, fiscal_year fye, subtenant sub, project p
//left join fiscal_year fyf -- fiscay_year_filter
//				on	fyf.id = @fiscal_year
//left join proj_values pv
//					ON	pv.project_id = p.id and
//    pv.period_dt = 	-- last filled reading .. is that correct the last reading?!
//    (select max(pv_i.period_dt) from proj_values pv_i, proj_value_stats pvs_i where
//																										pv_i.project_id = pv.project_id and
//                                                                                                        pvs_i.id = pv_i.id and
//                                                                                                        pv_i.progress_val is not null
//																				)
//left join proj_value_stats pvs
//	 				ON	pvs.id = pv.id
//left join proj_status ps
//					ON	ps.id = pvs.proj_status
//where
//					p.tenant_id = @tenant_id and
//                    (
//                    (@subtenant_id is null)
//								or
//								(p.sector_id = @subtenant_id)
//                                or
//                                (p.subtenant_id = @subtenant_id)
//					) and
//					sub.id = ifnull(p.subtenant_id, p.sector_id) and
//                    mtp.id = @mtp and
//                    mtp.mtp_start = fys.id and
//                    mtp.mtp_end = fye.id and
//                    (
//                    (p.mtp_id = @mtp) -- project is defined for the mtp (in general even if mtp is not the current one
//    or
//    (p.start_dt_actual between fys.start_date and fye.end_date) -- project start date is within the selected mtp
//    or
//    (ifnull(p.end_dt_actual, CURDATE()) between fys.start_date and fye.end_date) -- project end date is within the selected mtp, if it's null assume it's now to reduce the conditions
//    or
//    (p.start_dt_actual < fys.start_date and ifnull(p.end_dt_actual, CURDATE()) > fye.end_date) -- project spanning multiple mtps
//					) and
//					(
//                    (@fiscal_year is null)
//							or
//							(p.start_dt_actual between fyf.start_date and fyf.end_date) -- project start date is within the selected mtp
//    or
//    (ifnull(p.end_dt_actual, CURDATE()) between fyf.start_date and fyf.end_date) -- project end date is within the selected mtp, if it's null assume it's now to reduce the conditions
//    or
//    (p.start_dt_actual < fyf.start_date and ifnull(p.end_dt_actual, CURDATE()) > fyf.end_date) -- project spanning multiple mtps
//					) $wherecat   $wheretype $whereorgunit $wherehasbudget $whereprojectschedule $whereprojectop $wherebudgetstatus ;"));
        $projectlisttable = \DB::select(\DB::raw("select p.*,p.id as id, p.symbol as symbol, p.name_short, sub.name as resp_org_unit, p.sector_id, p.subtenant_id,
						p.start_dt_actual, p.end_dt_base, pvs.pd_planned_duration, pv.progress_val as proj_prog,
						p.budget_base,
						if(p.has_budget=1, pvs.b_spi, pvs.spi) as spi, pvs.b_cpi,
						pvs.sv_sched_var_pct, pvs.sv_sched_var_days, pvs.b_sv_sched_var, p.spending_total as actual_cost,
						pvs.b_cv_cost_var as cost_variance,
						pvs.proj_perf as proj_perf, pvs.proj_status,
						ps.name as proj_status_name, ps.color_code as proj_status_color
from 		mtp, fiscal_year fys, fiscal_year fye, subtenant sub, project p
left join fiscal_year fyf -- fiscay_year_filter
				on	fyf.id = @fiscal_year
left join proj_values pv
					ON	pv.project_id = p.id and
								pv.period_dt = 	-- last filled reading .. is that correct the last reading?!
																				(select max(pv_i.period_dt) from proj_values pv_i, proj_value_stats pvs_i where
																										pv_i.project_id = pv.project_id and
																										pvs_i.id = pv_i.id and
																										pv_i.progress_val is not null
																				)
left join proj_value_stats pvs
	 				ON	pvs.id = pv.id
left join proj_status ps
					ON	ps.id = pvs.proj_status
where
					p.tenant_id = @tenant_id and
					p.status_approval = 1 and -- 1 for approved
					p.status_operational != 0 and -- 0 for planning
					(
								(@subtenant_id is null)
								or
								(p.sector_id = @subtenant_id)
								or
								(p.subtenant_id = @subtenant_id)
					) and
					sub.id = ifnull(p.subtenant_id, p.sector_id) and
					mtp.id = @mtp and
					mtp.mtp_start = fys.id and
					mtp.mtp_end = fye.id and
					(
								(p.mtp_id = @mtp) -- project is defined for the mtp (in general even if mtp is not the current one
								or
								(p.start_dt_actual between fys.start_date and fye.end_date) -- project start date is within the selected mtp
								or
								(ifnull(p.end_dt_actual, CURDATE()) between fys.start_date and fye.end_date) -- project end date is within the selected mtp, if it's null assume it's now to reduce the conditions
								or
								(p.start_dt_actual < fys.start_date and ifnull(p.end_dt_actual, CURDATE()) > fye.end_date) -- project spanning multiple mtps
					) and
					(
							(@fiscal_year is null)
							or
							(p.start_dt_actual between fyf.start_date and fyf.end_date) -- project start date is within the selected mtp
							or
							(ifnull(p.end_dt_actual, CURDATE()) between fyf.start_date and fyf.end_date) -- project end date is within the selected mtp, if it's null assume it's now to reduce the conditions
							or
							(p.start_dt_actual < fyf.start_date and ifnull(p.end_dt_actual, CURDATE()) > fyf.end_date) -- project spanning multiple mtps
					)
 $wherecat   $wheretype $whereorgunit $wherehasbudget $whereprojectschedule $whereprojectop $wherebudgetstatus ;"));


        $totalprojectcount = count($projectlisttable);
        $progressval = $perfval = 0;
        if ($projectlisttable) {
            $i = 0;
            foreach ($projectlisttable as $tableval) {
                $projectlisttable[$i]->no = $i + 1;
                $startdate = new DateTime($tableval->start_dt_base);
                $enddate = new DateTime($tableval->end_dt_base);
                $projectlisttable[$i]->spi = round($tableval->spi, 3);
                $projectlisttable[$i]->spi = number_format($projectlisttable[$i]->spi, 3);
                $projectlisttable[$i]->b_cpi = round($tableval->b_cpi, 3);
                $projectlisttable[$i]->b_cpi = number_format($projectlisttable[$i]->b_cpi, 3);
                $projectlisttable[$i]->start_dt_basenew = $startdate->format('d/m/Y');
                $projectlisttable[$i]->end_dt_basenew = $enddate->format('d/m/Y');
//                if (!isset($tableval->proj_prog)) {
//                    $tableval->progress_val = 0;
//                }
                $progressval += $tableval->proj_prog;

                $projectlisttable[$i]->proj_prog = number_format($tableval->proj_prog * 100, 2) . "%";

                if ($debugmodeperf == 'false') {

                    if ($tableval->proj_perf * 100 > 100) {
                        $tableval->proj_perf = 100;
                    } else {
                        $tableval->proj_perf = $tableval->proj_perf * 100;
                    }
                    if ($tableval->proj_perf * 100 < 0) {
                        $tableval->proj_perf = 0;
                    }
                }

                $perfval += $tableval->proj_perf;
                $projectperfarray[] = $tableval->proj_perf;
                if ($tableval->proj_perf * 100 >= 50) {

                    $performingprojectcount = $performingprojectcount + 1;
                }
                if ($tableval->proj_perf * 100 < 50) {

                    $nonperformingprojectcount = $nonperformingprojectcount + 1;
                }
                if (!$tableval->proj_status_name) {
                    $tableval->proj_status_name = "no_status";
                }

                if ($tableval->proj_status_name) {

                    $tableval->proj_status_name = strtolower($tableval->proj_status_name);
                    if (!$tableval->proj_status_name) {
                        $tableval->proj_status_name = "no_status";
                    }
                }


                $projectlist[$i] = $tableval;
                $i++;
            }

            $projectbestperforming = max($projectperfarray);
            $projectbestperformingval = $projectbestperforming;
            //  $projectbestperforming = (number_format($projectbestperforming * 100, 2));

            $projectleastperforming = min($projectperfarray);

            //  $projectleastperforming = (number_format($projectleastperforming * 100, 2));
            if ($debugmodeperf == 'false') {
                if ($projectbestperforming > 100) {
                    $projectbestperformingval = 100;
                }
                if ($projectleastperforming > 100) {
                    $projectleastperforming = 100;
                }
                if ($projectleastperforming < 0) {
                    $projectleastperforming = 0;
                }
                if ($projectbestperforming < 0) {
                    $projectbestperformingval = 0;
                }
            }

            if ($projectlist) {
                $i = 0;

                foreach ($projectlist as $list) {
                    if ($list->proj_status == NULL) {
                        $proj_perf = 'NULL';

                    }
                    $start_dt_base = new DateTime($list->start_dt_base);
                    $end_dt_base = new DateTime($list->end_dt_base);
//                       $start_dt_base = $list->start_dt_base;
//                       $end_dt_base = $list->end_dt_base;
                    //  $next_reading_dt = new DateTime($list->next_reading_dt);
                    $projectlist[$i]->duration = $this->duration($list->start_dt_base, $list->end_dt_base);
                    if ($list->project_category == 0) {
                        $ministrycount = $ministrycount + 1;
                    } else {
                        $developmentcount = $developmentcount + 1;
                    }

//                        $projectlist[$i]->start_dt_base = $start_dt_base->format('d/m/Y');
//                        $projectlist[$i]->end_dt_base = $end_dt_base->format('d/m/Y');
                    //$projectlist[$i]->next_reading_dt = $next_reading_dt->format('d/m/Y');


                    if (empty($list->subtenant_id) || !isset($list->subtenant_id) || $list->subtenant_id === 0) {
                        $subid = $list->sector_id;

                    } else {
                        $subid = $list->subtenant_id;
                    }
                    $subid = \DB::select(\DB::raw("SELECT name  FROM subtenant  WHERE id=$subid"));
                    $subtenant_name = $subid[0]->name;
                    $projectlist[$i]->subtenant_name = $subtenant_name;
                    if ($list->proj_status != NULL) {
                        $proj_perf = $list->proj_perf;
//                        echo $list->proj_perf;
                        if ($debugmodeperf == 'false') {
                            if ($list->proj_perf > 100) {
                                $proj_perf = 100;
                            }
                            if ($list->proj_perf < 0) {
                                $proj_perf = 0;
                            }
                        }
                    }
                    if ($proj_perf != 'NULL') {
                        if ($proj_perf < 0) {
                            $proj_perf = 0;
                        }

                        $proj_perf = number_format($proj_perf, 2);
                    }

                    $projectshowdata[] = [
                        'name' => $list->name_short,
                        'orgunit' => $subtenant_name,
                        'performance' => $proj_perf,
                        'performancetxt' => ($proj_perf == 'NULL') ? ' ' : $proj_perf,
                        'progress' => $list->proj_prog,
                        'symbol' => $list->symbol,
                        'description' => $list->description,
                        'status_approval' => $list->status_approval,
                        'status_operational' => $list->status_operational,
                        'project_type' => $list->project_type,
                        'project_category' => $list->project_category,
                        'value_period' => $list->value_period,
                        'start_dt_base' => $list->start_dt_base,
                        'end_dt_base' => $list->end_dt_base,
                        'has_budget' => $list->has_budget,
                        "budget_base" => $list->budget_base,
                        "importance" => $list->importance,
                        "has_forecasting" => $list->has_forecasting,
                        "has_risks" => $list->has_risks,
                        "progress_total" => $list->progress_total,
//                    "link" => $list->link,
                        "workingdays" => $projectlist[$i]->duration,
                        "viewlinkar" => '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=projectview/' . $projectlist[$i]->id . '?query=fromprojectdashboard' . '>معاينة المشروع</a></div>',
                        "viewlink" => '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=projectview/' . $projectlist[$i]->id . '?query=fromprojectdashboard' . '>Project View</a></div>'

                    ];

                    $i++;
                }

            }
            $progressvalavg = $progressval / $i;

            $perfvalavg = $perfval / $i;
//            echo $perfvalavg;
//             die();
            return response()->json([
                "code" => 200,
                "projectlisttable" => $projectlisttable,
                "projectshowdata" => $projectshowdata,
                "progressvalavg" => (number_format(($progressvalavg * 100), 2)),
                "perfvalavg" => (number_format($perfvalavg, 2)),
                "ministrycount" => $ministrycount,
                "developmentcount" => $developmentcount,
                "totalprojectcount" => $totalprojectcount,
                "performingprojectcount" => $performingprojectcount,
                "nonperformingprojectcount" => $nonperformingprojectcount,
                "projectbestperforming" => (number_format($projectbestperformingval, 2)),
                "projectleastperforming" => (number_format($projectleastperforming, 2)),
                "filscalyear" => $filscalyear
            ]);
        }
    }

    public
    function projectviewdetails(Request $request)
    {

        $projectid = $request->projectid;

        $projectgeneraldata = \DB::select(\DB::raw("select p.*,p.name as proj_name,p.mtp_id as mtp,
						p.importance as proj_importance,
						sub.name as resp_unit_name,
						sub.id as resp_unit_id,
						pt.name as proj_type,
						p.project_category as proj_category,
						p.status_operational,
						p.start_dt_base as start_date,
						p.end_dt_base as end_date,
						ceiling(datediff(p.end_dt_base, p.start_dt_base) * 5/7) as duration,
						p.start_dt_actual as actual_start_date,
						p.end_dt_actual as closure_date,
						(select count(1)from proj_risks pr where pr.project_id = p.id and pr.status = 1) as open_risk_count,
						(select round(avg(pr.propability * pr.impact),0) from proj_risks pr where pr.project_id = p.id and pr.status = 1) as risks_priority,
						p.budget_base as budget_planned,
						if	(
									case	when p.status_operational = 0
														then		p.start_dt_base
														else
																			(select max(pv.period_dt) from proj_values pv where pv.project_id = p.id and p.status_operational <> 3 /*closed*/)
									end <= CURDATE()
								, 0, 1) as up_to_date /**1: up to date, 0: not up to date**/
from project p, subtenant sub, proj_type pt where
		sub.id = p.subtenant_id and
		pt.id = p.project_type and
		p.id =$projectid ;"));

        $i = 0;
        foreach ($projectgeneraldata as $projectgeneraldataval) {
            $projectgeneraldata[$i]->start_date = date("d/m/Y", strtotime($projectgeneraldataval->start_date));
            $projectgeneraldata[$i]->end_date = date("d/m/Y", strtotime($projectgeneraldataval->end_date));
            $projectgeneraldata[$i]->actual_start_date = date("d/m/Y", strtotime($projectgeneraldataval->actual_start_date));
            if ($projectgeneraldataval->closure_date) {
                $projectgeneraldata[$i]->closure_date = date("d/m/Y", strtotime($projectgeneraldataval->closure_date));
            }
            if ($projectgeneraldataval->proj_category == 0) {
                $projectgeneraldata[$i]->proj_category = "ministry";
            }
            if ($projectgeneraldataval->proj_category == 1) {
                $projectgeneraldata[$i]->proj_category = "development";
            }

            if ($projectgeneraldataval->proj_importance == 1) {
                $projectgeneraldata[$i]->proj_importance = "weakly_important";
            }
            if ($projectgeneraldataval->proj_importance == 2) {
                $projectgeneraldata[$i]->proj_importance = "averagely_important";
            }
            if ($projectgeneraldataval->proj_importance == 3) {
                $projectgeneraldata[$i]->proj_importance = "important";
            }
            if ($projectgeneraldataval->proj_importance == 4) {
                $projectgeneraldata[$i]->proj_importance = "very_important";
            }
            if ($projectgeneraldataval->proj_importance == 5) {
                $projectgeneraldata[$i]->proj_importance = "critically_important";
            }

            if ($projectgeneraldataval->status_operational == 0) {
                $projectgeneraldata[$i]->status_operational = "planning";
            }
            if ($projectgeneraldataval->status_operational == 1) {
                $projectgeneraldata[$i]->status_operational = "progressing";
            }
            if ($projectgeneraldataval->status_operational == 2) {
                $projectgeneraldata[$i]->status_operational = "onhold";
            }
            if ($projectgeneraldataval->status_operational == 3) {
                $projectgeneraldata[$i]->status_operational = "closed";
            }
            $mtp = $projectgeneraldata[$i]->mtp;
            $mtpdata = $this->loadfiscalfrommtp($mtp);

            //$projectgeneraldata[$i]->mtpenddate=$mtpdata[0]->end_date;


            $mtpdata = \DB::select(\DB::raw("select mtp_start,mtp_end from mtp where id=$mtp"));
            $mtpstart = $mtpdata[0]->mtp_start;
            $mtpend = $mtpdata[0]->mtp_end;

            $i = 0;
            $fiscaldatastart = \DB::select(\DB::raw("select start_date from fiscal_year where id=$mtpstart"));
            $fiscaldataend = \DB::select(\DB::raw("select end_date from fiscal_year where id=$mtpend"));
//            foreach ($fiscaldata as $fiscaldataval) {
//                $mtpstart_date = date("d/m/Y", strtotime($fiscaldataval->start_date));
//                $mtpend_date = date("d/m/Y", strtotime($fiscaldataval->end_date));
//
//            }
            $projectgeneraldata[$i]->mtpstartdate = date("d/m/Y", strtotime($fiscaldatastart[0]->start_date));;
            $projectgeneraldata[$i]->mtpenddate = date("d/m/Y", strtotime($fiscaldataend[0]->end_date));
        }


        $projectreadings = \DB::select(\DB::raw("select pv.id, pv.period_no as reading_no, date(pv.period_dt) as reading_date, pv.progress_val as progress_value -- (%)
		from proj_values pv where
		pv.project_id = $projectid /**Argument = id of the project**/
		order by pv.period_dt;"));

        $j = 0;
        foreach ($projectreadings as $projectreadingsval) {
            $projectreadings[$j]->reading_date = date("d/m/Y", strtotime($projectreadingsval->reading_date));
            $j++;
        }


        $projectreadingslast = \DB::select(\DB::raw("select pv.id, pv.period_no as reading_no, date(pv.period_dt) as reading_date, pv.progress_val as progress_value
		from proj_values pv where
		pv.project_id = $projectid and pv.progress_val is not null
		order by pv.period_dt;"));
//        echo "select pv.id, pv.period_no as reading_no, date(pv.period_dt) as reading_date, pv.progress_val as progress_value
//		from proj_values pv where
//		pv.project_id = $projectid and pv.progress_val!='NULL'
//		order by pv.period_dt";
//        die();
        if ($projectreadingslast) {
            $lastreading = array_slice($projectreadingslast, -1);

            $selectedreadingid = $lastreading[0]->id;
            $selectedreadingdate = $lastreading[0]->reading_date;
            $selectedreadingdate = date("d/m/Y", strtotime($selectedreadingdate));
        }
        if ($projectgeneraldata && $projectreadingslast) {
            return response()->json([
                "code" => 200,
                "projectgeneraldetails" => $projectgeneraldata,
                "projectreadings" => $projectreadings,
                "selectedreadingid" => $selectedreadingid,
                "selectedreadingdate" => $selectedreadingdate

            ]);
        } else {
            return response()->json([
                "code" => 400,
            ]);
        }
    }

    public
    function getprojectreadigdetails($readingid)
    {
        $projectreadingprogress = \DB::select(\DB::raw("select progress_val as progress_value,period_dt from proj_values  where
		id = $readingid"));
        $projprogressval = $projectreadingprogress[0]->progress_value;

        $projectreadingslist = \DB::select(\DB::raw("select 	pvs.sv_sched_var_pct as schedule_variance_pct,
						pvs.sv_sched_var_days as schedule_variance_days,
						pvs.b_sv_sched_var as schedule_variance_amount,
						pvs.spi,
						pvs.b_bac as planned_budget,
						pvs.b_actual_cost,
						pvs.b_bac - pvs.b_actual_cost as remaining_cost,
						pvs.b_pv_planned_val as planned_value,
						pvs.b_cv_cost_var as cost_variance,
						pvs.b_ev_earned_val,
						pvs.b_cpi as cpi,
						pvs.proj_perf as proj_performance,
						pvs.edac_days as edac,
						pvs.eac_pct as eac,
						pvs.b_eac as eac_amount,
						pvs.b_var_at_complete as vac,
						pvs.b_etc as etc,
						case when pvs.b_cpi >= 1 /**under budget**/ then pvs.b_tcpi_bac else pvs.b_tcpi_eac end as tcpi,
						ps.name as proj_status_name, ps.color_code as proj_status_color
		from proj_value_stats pvs, proj_status ps where
		ps.id = pvs.proj_status and
		pvs.id = $readingid; /**Argument = id of the reading, as described in the 2nd Query above**/

		/**
		NB:
		-----
		In the db the values are rounded to 8 digits
		In the screen they should be rounded to 0 for money, and days and 2 digits for percentages (ex: 81.25% i.e. 0.8125)
		**/"));
        $i = 0;
        $spendingratio = 0;
        $pvratio = 0;
        foreach ($projectreadingslist as $projectreadingslistval) {
//            $projectreadingslist[$i]->proj_status_name = strtolower($projectreadingslistval->proj_status );
            if ($projectreadingslistval->proj_performance < 0) {
                $projectreadingslistval->proj_performance = 0;
            }
            $projectreadingslist[$i]->proj_performance = number_format($projectreadingslistval->proj_performance * 100, 2);
            $projectreadingslist[$i]->cpi = number_format($projectreadingslistval->cpi, 3);
            $projectreadingslist[$i]->spi = number_format($projectreadingslistval->spi, 3);
            $projectreadingslist[$i]->tcpi = number_format($projectreadingslistval->tcpi, 3);
            $projectreadingslist[$i]->schedule_variance = number_format($projectreadingslistval->schedule_variance_pct * 100, 2) . "%";
            $projectreadingslist[$i]->eac = $projectreadingslistval->eac;
            $projectreadingslist[$i]->vac = $projectreadingslistval->vac;
            $projectreadingslist[$i]->etc = $projectreadingslistval->etc;
            $projectreadingslist[$i]->edac = round($projectreadingslistval->edac);
            $projectreadingslist[$i]->costratio = "0.00%";
            if ($projectreadingslistval->b_actual_cost) {
                $spendingratio = ($projectreadingslistval->b_actual_cost) / ($projectreadingslistval->planned_budget);
                $costratio = ($projectreadingslistval->b_actual_cost) / ($projectreadingslistval->planned_budget);
                $projectreadingslist[$i]->costratio = number_format($costratio * 100, 2) . "%";
            }
            if ($spendingratio) {
                $projectreadingslist[$i]->spendingratio = number_format($spendingratio * 100, 2);

            }
            if ($projectreadingslistval->b_actual_cost) {

                $pvratio = ($projectreadingslistval->b_actual_cost) / ($projectreadingslistval->planned_value);
            }
            if ($pvratio) {
                $projectreadingslist[$i]->pvratio = number_format($pvratio * 100, 2);
            }
            $projectreadingslist[$i]->schedule_variancenopct = number_format($projectreadingslistval->schedule_variance_pct * 100, 2);


//            if($projectreadingslist[$i]->eac!=0) {
//                $projectreadingslist[$i]->tcpi = $projectreadingslist[$i]->bac / $projectreadingslist[$i]->eac;
//            }
//            else{
//                $projectreadingslist[$i]->tcpi=0;
//            }
            $projectreadingslist[$i]->projprogressval = number_format($projprogressval * 100, 2) . "%";
            $projectreadingslist[$i]->last_readingdate = date("d/m/Y", strtotime($projectreadingprogress[0]->period_dt));

        }

        if ($projectreadingslist) {
            return response()->json([
                "code" => 200,
                "projectreadingslist" => $projectreadingslist,


            ]);
        }


    }

    public
    function removeprojectPermanently($id)
    {
        $teammembersid = $teamid = $teamdatadurationid = '';

        $statement = \DB::statement("SET FOREIGN_KEY_CHECKS=0;");
//        \DB::select(\DB::raw("delete  from  proj_value_stats where  project_id = $id"));
        /**delete kpi_values_stats**/
        \DB::select(\DB::raw("delete from proj_values where project_id = $id"));

        /**delete kpi_values**/

        $teamdata = \DB::select(\DB::raw("select id from proj_team where project_id = $id"));
        if ($teamdata) {
            $teamid = $teamdata[0]->id;
        }
        if ($teamdata) {
            $teamdataduration = \DB::select(\DB::raw("select id from proj_team_duration where team_id = $teamid"));
            if ($teamdataduration) {
                $teamdatadurationid = $teamdataduration[0]->id;
            }
            if ($teamdatadurationid) {
                $teammembers = \DB::select(\DB::raw("select id from proj_team_member where team_duration_id = $teamdatadurationid"));
                if ($teammembers) {
                    $teammembersid = $teammembers[0]->id;
                }
                if ($teammembersid) {
                    $statement = \DB::statement("SET FOREIGN_KEY_CHECKS=0;");
                    \DB::select(\DB::raw("delete  from  proj_team_member where  id = $teammembersid"));
                }
                $statement = \DB::statement("SET FOREIGN_KEY_CHECKS=0;");
                \DB::select(\DB::raw("delete  from  proj_team_duration where  id = $teamdatadurationid"));

            }
            $statement = \DB::statement("SET FOREIGN_KEY_CHECKS=0;");
            \DB::select(\DB::raw("delete  from proj_team where project_id = $id"));

        }
        /**delete kpi_target**/


        /**delete kpi_def**/
        if (\DB::delete(\DB::raw("delete from project where id = $id"))) {
            $statement = \DB::statement("SET FOREIGN_KEY_CHECKS=1;");
            return response()->json([
                "code" => 200,
                "msg" => "project_deleted"
            ]);
        }

        return response()->json([
            "code" => 400,
            "msg" => "error deleting the data"
        ]);
    }

    public
    function loadfiscalfrommtp($mtp)
    {

        $mtpdata = \DB::select(\DB::raw("select mtp_start,mtp_end from mtp where id=$mtp"));
        $mtpstart = $mtpdata[0]->mtp_start;
        $mtpend = $mtpdata[0]->mtp_end;
        $i = 0;
        $fiscaldata = \DB::select(\DB::raw("select id,start_date,end_date from fiscal_year where id>=$mtpstart and id<=$mtpend"));
        foreach ($fiscaldata as $fiscaldataval) {
            $fiscaldata[$i]->start_date = date("d/m/Y", strtotime($fiscaldataval->start_date));
            $fiscaldata[$i]->end_date = date("d/m/Y", strtotime($fiscaldataval->end_date));
            $i++;
        }

        if ($fiscaldata) {
            return response()->json([
                "code" => 200,
                "fiscaldata" => $fiscaldata
            ]);
        }
    }

}
