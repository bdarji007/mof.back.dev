<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Modules\ClientApp\Entities\Fiscalyear;
use Illuminate\Http\Request;

class FiscalyearController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:mtp-view|mtp-create|mtp-edit|mtp-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:mtp-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:mtp-edit|mtp-view', ['only' => ['edit', 'update', 'show']]);
        $this->middleware('permission:mtp-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loadmtp = Fiscalyear::all();
        if ($loadmtp) {
            return response()->json([
                "code" => 200,
                "mtpdata" => $loadmtp
            ]);
        }

        return response()->json(["code" => 400]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fiscalyear = new Fiscalyear();
        $fiscalyear->start_date = $request->start_date;
        $fiscalyear->end_date = $request->end_date;

        $lastFy = DB::table('fiscal_year')->latest('id')->first();

        $fiscalyear = Fiscalyear::create(
            [
                'id' => $lastFy->id+1,
                'start_date' => date("Y-m-d", strtotime($request->start_date)),
                'end_date' => date("Y-m-d", strtotime($request->end_date))
            ]
        );

        if ($fiscalyear->save()) {
            return response()->json([
                "code" => 200,
                "msg" => "data inserted successfully"
            ]);
        }

        return response()->json(["code" => 400]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fiscalyear  $fiscalyear
     * @return \Illuminate\Http\Response
     */
    public function show(Fiscalyear $fiscalyear, $id)
    {
        $fiscalyear = Fiscalyear::Where('id', $id)->first();

        if ($fiscalyear) {
            return response()->json([
                "code" => 200,
                "data" => $fiscalyear
            ]);
        }

        return response()->json([
            "code" => 404,
            "msg" => "data not found"
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fiscalyear  $fiscalyear
     * @return \Illuminate\Http\Response
     */
    public function edit(Fiscalyear $fiscalyear)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fiscalyear  $fiscalyear
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fiscalyear $fiscalyear, $id)
    {
        $fiscalyear = Fiscalyear::find($id);

        if (!$fiscalyear) {
            return response()->json([
                "code" => 404,
                "msg" => "data not found"
            ]);
        } else {
            $fiscalyear->start_date = date("Y-m-d", strtotime($request->start_date));
            $fiscalyear->end_date = date("Y-m-d", strtotime($request->end_date));
            if ($fiscalyear->update())  {
                return response()->json([
                    "code" => 200,
                    "msg" => "data updated successfully"
                ]);
            }
        }

        return response()->json([
            "code" => 400,
            "msg" => "error updating the data"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fiscalyear  $fiscalyear
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fiscalyear $fiscalyear, $id)
    {
        $query = Fiscalyear::find($id);
        if (!$query) {
            return response()->json([
                "code" => 404,
                "msg" => "data not found"
            ]);
        }
        if ($query->delete()) {

            return response()->json([
                "code" => 200,
                "msg" => "deleted the record"
            ]);
        }

        return response()->json(["code" => 400]);
    }
}
