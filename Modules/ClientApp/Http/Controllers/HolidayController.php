<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Goutte;

class HolidayController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:holidays-view|holidays-create|holidays-edit|holidays-delete', ['only' => ['index',
            'show']]);
        $this->middleware('permission:holidays-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:holidays-edit', ['only' => ['edit', 'update', 'show']]);
        $this->middleware('permission:holidays-delete', ['only' => ['destroy']]);
        $this->middleware('permission:holidays-importweb', ['only' => ['scrape']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $holiday_info = \DB::table("holiday")
            // ->join('holiday_name', 'holiday.name_id', '=', 'holiday_name.id')
            ->select('holiday.*')
            // ->orderBy('sort_no', 'asc')
            ->get();

        return response()->json([
            "code" => 200,
            "data" => $holiday_info
        ]);
    }

    public function holiday_name(Request $request)
    {
        $holiday_name = \DB::select(\DB::raw("select id, name from holiday_name"));
        return response()->json([
            "code" => 200,
            "data" => $holiday_name
        ]);
    }

    public function allholidays(Request $request)
    {
        $holiday_info = \DB::select(\DB::raw("select id, name from holiday_name"));
        return response()->json([
            "code" => 200,
            "data" => $holiday_info
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function years()
    {
        $YEAR = date('Y');
        $year = array($YEAR - 1, $YEAR, $YEAR + 1);
        if ($year) {
            return response()->json([
                "code" => 200,
                "data" => $year,
            ]);
        } else {
            return response()->json([
                "code" => 404,
                "msg" => "not found"
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hol_name = \DB::table("holiday_name")
            ->select('name')
            ->where('id', $request->hid)
            ->first();

        $prctype = \DB::table("holiday")->insert(
            [
                'name' => $hol_name->name,
                //'name' => $request->date_from[0],
                'hol_date' => $request->hol_date,
                'year' => $request->year,
                'name_id' => $request->hid
            ]
        );
        if ($prctype) {
            return response()->json([
                "code" => 200,
                "msg" => "تم تسجيل العطلة"
            ]);
        }
        return response()->json(["code" => 400]);
    }

    public function store1(Request $request) {
        $hol_name = \DB::table("holiday_name")
            ->select('name')
            ->where('id', $request->hid)
            ->first();

        $prctype = \DB::table("holiday")->insert(
            [
                'name' => $hol_name->name,
                //'name' => $request->date_from[0],
                'hol_date' => $request->hol_date,
                'year' => $request->year,
                'name_id' => $request->hid
            ]
        );

        if ($prctype) {
            return response()->json([
                "code" => 200,
                "msg" => "تم تسجيل العطلة"
            ]);
        }

        return response()->json(["code" => 400]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_info = \DB::table("holiday")
            ->select('id', 'year', 'name', 'hol_date')
            ->where('id', $id)
            ->first();

        $user = [];
        if ($user_info) {

            $holi = \DB::table("holiday_name")->select('id')->where('name', $user_info->name)->first();
            $user['hid'] = $holi->id;
            $user['id'] = $user_info->id;
            $user['year'] = $user_info->year;
            $user['holiday'] = $user_info->name;
            $user['hol_date'] = $user_info->hol_date;

            return response()->json([
                "code" => 200,
                "data" => $user,
            ]);
        } else {
            return response()->json([
                "code" => 404,
                "msg" => "not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hol_name = DB::table('holiday_name')->where('id', $request->hid)->first();
        $update = DB::table('holiday')->where('id', $id)
            ->update([
                'year' => $request->year,
                'hol_date' => $request->hol_date,
                'name' => $hol_name->name,
                'name_id' => $request->hid,
            ]);
        if ($update) {
            return response()->json([
                "code" => 200,
                "msg" => 'تم تعديل العطلة',
                // "data" => $date_hol,
            ]);
        }
        return response()->json([
            "code" => 400,
            "msg" => 'error'
        ]);
    }

    public function scrape(Request $request)
    {
        $YEAR = $request->year;//date('Y');

        $client = new Goutte\Client();
        $client->setHeader('Accept-Language', 'en-gb');
        $crawler = $client->request('GET', "https://www.timeanddate.com/holidays/kuwait/" . $YEAR);

        $date = $crawler->filterXPath('//table[@id="holidays-table"]')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('th')->each(function ($th, $i) {
                return trim($th->text());
            });
        });
        $content = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        //$json = json_decode(json_encode($content, JSON_FORCE_OBJECT));
        $i = 0;

        foreach ($content as $content1) {
            foreach ($content1 as $content2) { // print_r($json[3][0]);
                $test[$i]['name'] = $content1[1];
                $test[$i]['type'] = $content1[2];
            }
            $i++;
        }
        $j = 0;
        foreach ($date as $date1) {
            foreach ($date1 as $date2) {
                if (isset($date2)) {
                    $unit = $date2;
                    list($day, $month) = sscanf($unit, "%d %s");
                    $test[$j]['month'] = $month;
                    $test[$j]['day'] = $day;
                }
            }
            $j++;
        }
        $holiday_table = \DB::select(\DB::raw("DELETE FROM holiday where year=$YEAR"));
        foreach ($test as $test1) {
            $day = $month = $type = $date_hol1 = $date_hol = '';
            static $id = 1;
            if (isset($test1["name"])) {
                $day = $test1['day'];
                $month = $test1['month'];
                $type = $test1['type'];
                if ($day != NULL || $month != NULL) {
                    $month = date('m', strtotime($month));
                    $date_hol1 = strtotime("$YEAR-$month-$day");
                    $date_hol = date("Y-m-d", $date_hol1);
                } else {
                    $date_hol = "";
                }
                $scrap_name = $test1["name"];
                $newString = strtolower(str_replace(array("'", ' '), "", $scrap_name));
                $newString = preg_replace("/holiday$/", '', $newString);;

                if (($type != "Season") && ($type != "Observance")) {
                    $holiday_name = \DB::select(\DB::raw("select id, name from holiday_name where lower(REPLACE(REPLACE(name_scraping, ' ' , '' ),'\'','')) like '%$newString%'"));
                    if ($holiday_name)
                        $prctype = \DB::table("holiday")->insertGetId(
                            [
                                // 'id'=>$id,
                                'name' => $holiday_name[0]->name,
                                'hol_date' => $date_hol,
                                'year' => $YEAR,
                                'name_id' => $holiday_name[0]->id,
                            ]
                        );

                    if (!$holiday_name)
                        $prctype = \DB::table("holiday")->insertGetId(
                            [
                                // 'id'=>$id,
                                'name' => $scrap_name,
                                'hol_date' => $date_hol,
                                'year' => $YEAR,
                                'name_id' => null,
                            ]
                        );
                }
                $id++;
            }
        }
        $eng_hol = \DB::select(\DB::raw("select distinct name from holiday where name_id IS NULL and  year=$YEAR"));
        if ($prctype)
            return response()->json([
                "code" => 200,
                "data" => $eng_hol
            ]);
    }

    public function getmonth($month)
    {
        $m = ucfirst(strtolower(trim($month)));
        switch ($m) {
            case "January":
            case "Jan":
                $m = 1;
                break;
            case "February":
            case "Feb":
                $m = 2;
                break;
            case "March":
            case "Mar":
                $m = 3;
                break;
            case "April":
            case "Apr":
                $m = 4;
                break;
            case "May":
                $m = 5;
                break;
            case "June":
            case "Jun":
                $m = 6;
                break;
            case "July":
            case "Jul":
                $m = 7;
                break;
            case "August":
            case "Aug":
                $m = 8;
                break;
            case "September":
            case "Sep":
                $m = 9;
                break;
            case "October":
            case "Oct":
                $m = 10;
                break;
            case "November":
            case "Nov":
                $m = 11;
                break;
            case "December":
            case "Dec":
                $m = 12;
                break;
            default:
                $m = false;
                break;
        }
        return $m;
    }

    public function date(Request $request)
    {
        $date = [];
        $hol_name = DB::table('holiday_name')->where('id', $request->id)->first();
        $year = $request->year;
        $day = $hol_name->default_day;
        $month = $hol_name->default_month;
        $year = $request->year;

        if ($day != NULL || $month != NULL) {
            $date_hol1 = strtotime("$year-$month-$day");
            $date_hol = date("Y-m-d", $date_hol1);
        } else {
            $date_hol = "";
        }
        return response()->json([
            "code" => 200,
            "data" => $date_hol
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prctype = DB::table('holiday')->where('id', $id)->delete();

        if (!$prctype) {
            return response()->json([
                "code" => 404,
                "msg" => "holiday not found"
            ]);
        }

        if ($prctype) {
            return response()->json([
                "code" => 200,
                "msg" => "تم حذف العطلة"
            ]);
        }
        return response()->json(["code" => 400]);
    }
}
