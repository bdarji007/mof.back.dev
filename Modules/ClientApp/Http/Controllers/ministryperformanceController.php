<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class ministryperformanceController extends Controller
{
    //
    public function index()
    {

        $perfprog = DB::select(
            "call p_unit_perf_prog(114,4,'Q1',1);"
        );
        foreach ($perfprog as $row) {
            $kpishowdata[] = [
                'KPIdata' => 'Symbol: ' . $row->kpi_symbol,
                'importance' => $row->weight,
                'importancetxt' => 'Importance: ' . $row->weight,
                'performance' => $row->kpi_perf * 100,
                'performancetxt' => 'Performance: ' . (number_format($row->kpi_perf, 2) * 100) . '%',
                'progress' => 'Progress: ' . (number_format($row->kpi_prog, 2) * 100) . '%',
                'KPIname' => $row->kpi_name,
                'adjustedweight' => $row->adjusted_weight,
                'weightedperformance' => $row->adjusted_weight * $row->kpi_perf,


            ];
        }


        header('Content-type: application/json');
        //   echo json_encode($kpishowdata);


        if ($kpishowdata) {
            return response()->json([
                "code" => 200,
                "kpishowdata" => $kpishowdata
            ]);
        }

        return response()->json(["code" => 400]);
    }

    public function loadTenants()
    {
        /*$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3"));*/
        $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }


    public function sectionkpireport($value)
    {
        $debug_mode = 'false';
        $debug_show_icon = 0;
        $debug_show_icon_best = 0;
        $debug_show_icon_least = 0;
        $kpi_perf = 0;
        $debug_progmode = 'false';
        $debug_show_icon_prog = 0;
        $debug_tooltip_prog = 0;
        $valuearray = explode(",", $value);

        $section = $valuearray[0];
        $mtp = $valuearray[1];
        $asofperiod = $valuearray[2];
        $yearno = $valuearray[3];

        $deptid = \DB::select(\DB::raw("select parent_id from subtenant where id=$section"));
        $deptval = $deptid[0]->parent_id;

        $sectorpid = \DB::select(\DB::raw("select parent_id from subtenant where id=$deptval"));
        $sectorpval = $sectorpid[0]->parent_id;
        $sectorid = \DB::select(\DB::raw("select parent_id from subtenant where id=$sectorpval"));
        $sectorval = $sectorid[0]->parent_id;


        if ($sectorval == 2) {
            $sectorval = $sectorpval;
        }
        $perfprog = DB::select(
            "call p_unit_perf_prog($section,$mtp,'$asofperiod',$yearno);"
        );
//       var_dump($perfprog);
//       die();
        $acheivedcount = 0;
        $uptodatecount = 0;
        $unitperformance = 0;
        $unitprogress = 0;
        $nonuptodatecount = 0;
        $performingkpicount = 0;
        $nonperformingkpicount = 0;

        foreach ($perfprog as $row) {
            if ($row->kpi_perf * 100 >= 50) {
                $acheivedcount = $acheivedcount + 1;
                $performingkpicount = $performingkpicount + 1;
            }
            if ($row->kpi_perf * 100 < 50) {

                $nonperformingkpicount = $nonperformingkpicount + 1;
            }

            $weighedperf = $row->adjusted_weight * $row->kpi_perf;
            $weighedprog = $row->adjusted_weight * $row->kpi_prog;
            $unitperformance = $unitperformance + $weighedperf;
            $unitprogress = $unitprogress + $weighedprog;
            if ($row->kpi_up_to_date == 1) {
                $uptodatecount = $uptodatecount + 1;

            }
            if ($row->kpi_up_to_date == 0) {
                $nonuptodatecount = $nonuptodatecount + 1;

            }


            if ($debug_mode == 'false') {
                if ($row->kpi_perf * 100 > 100) {
                    $kpi_perf = 100;
                    $debug_show_icon = 1;
                    $debug_show_icon_perf_tool = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                } else {
                    $kpi_perf = $row->kpi_perf * 100;
                    $debug_show_icon = 0;
                    $debug_show_icon_perf_tool = '';

                }

            }
            if ($debug_progmode == 'false') {
                if ($row->kpi_prog * 100 < 0) {
                    $kpi_prog = 0;
                    $debug_show_icon_prog = 1;
                } else {
                    $kpi_prog = $row->kpi_prog * 100;
                    $kpi_prog = (number_format($kpi_prog, 2));
                    $debug_show_icon_prog = 0;
                }

            }

            $uptodateicon = '';
            $debugicon = '';

            if ($row->kpi_up_to_date == 0) {
                $uptodateicon = '<i class="fas fa-exclamation-circle fa-2x"></i>';
            }
            if ($debug_show_icon == 1 || $debug_show_icon_prog == 1) {
                $debugicon = '<i style="color:#ffffff" class="fas fa-exclamation-triangle fa-2x"></i>';
            }


            $kpishowdata[] = [
                'KPIdata' => $row->kpi_symbol,

                'importance' => $row->weight,
                'importancetxt' => 'Importance: ' . $row->weight,
                'performance' => $kpi_perf,
                'performancetxt' => (number_format($kpi_perf)) . '%',
                'progress' => $kpi_prog . '%',
                'KPIname' => $row->kpi_name,
                'adjustedweight' => (number_format($row->adjusted_weight * 100, 2)) . '%',
                'weightedperformance' => (number_format(($row->adjusted_weight * $row->kpi_perf) * 100, 2)) . '%',
                'kpi_up_to_date' => $row->kpi_up_to_date,
                'debug_show_icon' => $debug_show_icon,
                "link" => "gaugechart/" . $row->kpi_id . "?query=" . $section . "," . $mtp . "," . $asofperiod . "," . $yearno . ",fromsection," . $sectorval,
                "button" => '<div style="color:#ffffff" >' . $uptodateicon . $debugicon


            ];
        }
        $unitperformance = (number_format($unitperformance * 100, 2));
        $unitprogress = (number_format($unitprogress * 100, 2));
        if ($debug_progmode == 'false') {
            if ($unitprogress < 0) {
                $unitprogress = 0;
                $debug_show_icon_prog = 1;
            } else {
                $unitprogress = $unitprogress;
                $debug_show_icon_prog = 0;
            }
        }


        header('Content-type: application/json');
        //   echo json_encode($kpishowdata);


        if ($kpishowdata) {
            return response()->json([
                "code" => 200,
                "kpishowdata" => $kpishowdata,
                "totalkpi" => count($perfprog),
                "acheivedcount" => $acheivedcount,
                "uptodatecount" => $uptodatecount,
                "unitperformance" => $unitperformance,
                "unitprogress" => $unitprogress,
                "nonuptodatecount" => $nonuptodatecount,
                "performingkpicount" => $performingkpicount,
                "nonperformingkpicount" => $nonperformingkpicount,
                "debug_show_icon_prog" => $debug_show_icon_prog


            ]);
        }

        return response()->json(["code" => 400]);
    }

    public function departmentreport($value)
    {

        $debug_mode = 'true';
        $debug_show_icon = 0;
        $debug_show_icon_best = 0;
        $debug_show_icon_least = 0;
        $kpi_color = 0;
        $superlink = '';
        $superlinkar = '';
        $debug_progmode = 'true';
        $debug_show_icon_prog = 0;
        $debug_show_icon_progstrategic = 0;
        $debug_show_icon_progic = '';
        $debug_show_icon_perf_tool = '';
        $deptsuperlinkar = $deptsuperlink = '';
        $debugprog = \DB::select(\DB::raw("select value  from system_vars where name='show_<0_prog'"));
        $debugperf = \DB::select(\DB::raw("select value  from system_vars where name='show_>100_perf'"));

        $debugmodeprog = $debugprog[0]->value;
        $debugmodeperf = $debugperf[0]->value;
        if ($debugmodeperf == 'true') {
            $debug_mode = 'true';
        } else {
            $debug_mode = 'false';
        }
        if ($debugmodeprog == 'true') {
            $debug_progmode = 'true';
        } else {
            $debug_progmode = 'false';
        }


        $valuearray = explode(",", $value);

        $dept = $valuearray[0];
        $mtp = $valuearray[1];
        $asofperiod = $valuearray[2];
        $yearno = $valuearray[3];
        $type = $valuearray[4];
        $kpistrategic_sector = [];

        if ($mtp == 'null') {
            $mtp = 4;
        }
        if ($dept == 'null') {
            $dept = 2;
        }

        $tenantId = env('TENANT_ID') ? env('TENANT_ID') : 1;
        \DB::statement("set @tenant_id := $tenantId");
        \DB::statement("set @mtp_id:= $mtp");
        \DB::statement("set @year_no:= $yearno");
        \DB::statement("set @period_name := '$asofperiod'");

        /*set @tenant_id := 1;
        set @mtp_id:= 4;
        set @year_no:= 1;
        set @period_name := 'Y';*/
        $getKpiPerfProg = \DB::select(\DB::raw("select mtab.*, kgv_base.label as kpi_base_label, kgv_target.label as kpi_target_label, kgv_actual.label as kpi_value_label from (
select
		ob.sort_no,
		ob.icon_name,
		ob.id as target_id,
		ob.name target_name,
		kd.id as kpi_id,
		kd.name as kpi_name,
		kd.symbol as kpi_symbol,
		kd.value_type,
		kd.grade_id,
		f_get_kpi_base(kt.id, @year_no,
				case left(lower(@period_name),1)
						when 'q' then 3
						when 'h' then 6
						when 'y' then 12
						else null
				end
		) as kpi_base,
		case lower(@period_name)
				when 'q1' then kvs.q1_target
				when 'q2' then kvs.q2_target
				when 'q3' then kvs.q3_target
				when 'q4' then kvs.q4_target
				when 'h1' then kvs.h1_target
				when 'h2' then kvs.h2_target
				when 'y' then kvs.y_target
				else null
		end as kpi_target,
		case lower(@period_name)
				when 'q1' then kvs.q1_value
				when 'q2' then kvs.q2_value
				when 'q3' then kvs.q3_value
				when 'q4' then kvs.q4_value
				when 'h1' then kvs.h1_value
				when 'h2' then kvs.h2_value
				when 'y' then kvs.y_value
				else null
		end as kpi_value,
		f_get_kpi_min(kt.id, @year_no,
				case left(lower(@period_name),1)
						when 'q' then 3
						when 'h' then 6
						when 'y' then 12
						else null
				end
		) as kpi_min,
		f_get_kpi_max(kt.id, @year_no,
				case left(lower(@period_name),1)
						when 'q' then 3
						when 'h' then 6
						when 'y' then 12
						else null
				end
		) as kpi_max,
		ifnull(u.short_name, u.name) as unit_name,
		kd.rounding_decimals,
		case lower(@period_name)
				when 'q1' then kvs.q1_perf
				when 'q2' then kvs.q2_perf
				when 'q3' then kvs.q3_perf
				when 'q4' then kvs.q4_perf
				when 'h1' then kvs.h1_perf
				when 'h2' then kvs.h2_perf
				when 'y' then kvs.y_perf
				else null
		end as kpi_perf,
		case lower(@period_name)
				when 'q1' then kvs.q1_prog
				when 'q2' then kvs.q2_prog
				when 'q3' then kvs.q3_prog
				when 'q4' then kvs.q4_prog
				when 'h1' then kvs.h1_prog
				when 'h2' then kvs.h2_prog
				when 'y' then kvs.y_prog
				else null
		end as kpi_prog,
		kpt.formula as perf_formula,
		kpt.formula_mathx_en as perf_formula_mathx,
		kpt.formula_values_en as perf_formula_values,
		kpt.formula_mathx_ar as perf_formula_mathx_ar,
		kpt.formula_values_ar as perf_formula_values_ar,
		kpt.factor_1 as perf_factor_1,
		kpt.factor_2 as perf_factor_2,
		case when (not exists (
				select 1 from kpi_values kv where
						kv.kpi_target_id = kt.id and
						kv.actual_value is null and
						((kv.target_month <=
								(case lower(@period_name)
										when 'q1' then 3
										when 'q2' then 6
										when 'q3' then 9
										when 'q4' then 12
										when 'h1' then 6
										when 'h2' then 12
										when 'y' then 12
								end)
								and kv.target_year = @year_no) or (kv.target_year < @year_no))
						)) then 1 else 0
		end as kpi_up_to_date, /*1: is up_to_date, 0: is not*/
		kd.importance as weight,
		sub.id as sub_id,
		sub.name as sub_name,
		sub.subtenant_type_id,
		kt.mtp_id
		from
		kpi_values_stats kvs, kpi_target kt, kpi_def kd, subtenant sub, objective ob, kpi_unit u, kpi_performance_type kpt
		where
		sub.tenant_id = @tenant_id and
		sub.subtenant_type_id <= 3 and /*for the sector type*/
		sub.id = kd.child_subtenant_id and
		kt.kpi_id = kd.id and
		kvs.kpi_target_id = kt.id and
		u.id = kd.value_unit and
		kpt.id = kd.value_explanation and
		kt.mtp_id = @mtp_id and
		kd.scope_table = 'objective' and
		kd.scope_id = ob.id and
		kvs.year_no = @year_no
		/* order by ob.id, kd.symbol asc */
)
	 as mtab

	 left outer join kpi_grade_values kgv_base
	 on kgv_base.grade_id = mtab.grade_id and
				kgv_base.value = mtab.kpi_base

	 left outer join kpi_grade_values kgv_target
	 on kgv_target.grade_id = mtab.grade_id and
				kgv_target.value = mtab.kpi_target

	left outer join kpi_grade_values kgv_actual
	 -- left outer joing kpi_grade_values kgv_target
	 on kgv_actual.grade_id = mtab.grade_id and
				kgv_actual.value = mtab.kpi_value
	order by sort_no
;"));
        $resultPerfProg = [];
        $visonResultPerfProg = [];
        foreach ($getKpiPerfProg as $perfprog) {
            if ($perfprog->subtenant_type_id == 3) {
                $resultPerfProg[$perfprog->target_id][] = $perfprog;
            } else if (in_array($perfprog->subtenant_type_id, [1, 2])) {
                $visonResultPerfProg[$perfprog->target_id][] = $perfprog;
            }
        }

        $i = 0;
        $valsperfprog = [];
        foreach ($resultPerfProg as $prefprogvalue) {
            $kpiPerfCount = $kpiProgCount = 0;
            $kpiActual = $kpiTarget = $kpiBase = 0;
            $kpiName = '';
            $job = 0;
            $kpiTargetName = [];
            $ddd = '';
            foreach ($prefprogvalue as $prefandprog) {
                $ddd = $prefandprog->sort_no;
                //$kpiTargetName[$job]['sort_no'] = $prefandprog->sort_no;
                //$kpiTargetName[$job]['icon_name'] = $prefandprog->icon_name;
                $kpiTargetName[$job]['kpi_name'] = $prefandprog->kpi_name;
                $kpiTargetName[$job]['kpi_name'] = $prefandprog->kpi_name;

                $kpiTargetName[$job]['kpi_base'] = (($prefandprog->value_type == 2) ? number_format
                    ($prefandprog->kpi_base * 100, $prefandprog->rounding_decimals) . '%' :
                    ($prefandprog->value_type == 5 ? $prefandprog->kpi_base_label : number_format(sprintf("%." . ($prefandprog->rounding_decimals) . "f", $prefandprog->kpi_base))));

                $kpiTargetName[$job]['kpi_target'] = (($prefandprog->value_type == 2) ? number_format
                    ($prefandprog->kpi_target * 100, $prefandprog->rounding_decimals) . '%' :
                    ($prefandprog->value_type == 5 ? $prefandprog->kpi_target_label : number_format(sprintf("%." . ($prefandprog->rounding_decimals) . "f", $prefandprog->kpi_target))));

                $kpiTargetName[$job]['kpi_value'] = (($prefandprog->value_type == 2) ? number_format
                    ($prefandprog->kpi_value * 100, $prefandprog->rounding_decimals) . '%' :
                    ($prefandprog->value_type == 5 ? $prefandprog->kpi_value_label : number_format(sprintf("%." . ($prefandprog->rounding_decimals) . "f", $prefandprog->kpi_value))));


                /*$kpiTargetName[$job]['kpi_value'] = ($prefandprog->value_type == 2) ? number_format($prefandprog->kpi_value * 100, $prefandprog->rounding_decimals) . '%' : number_format(sprintf("%." . ($prefandprog->rounding_decimals) . "f", $prefandprog->kpi_value));*/

                $debug_show_icon_prog = 0;
                if ($debug_progmode == 'false') {
                    if ($prefandprog->kpi_prog < 0) {
                        $kpi_progsum = 0;
                        $debug_show_icon_prog = 1;
                    } else {
                        $kpi_progsum = (number_format($prefandprog->kpi_prog * 100, 2));
                        $debug_show_icon_prog = 0;
                    }
                    $kpiTargetName[$job]['kpi_prog'] = $kpi_progsum;
                    $kpiTargetName[$job]['kpi_prog_flag'] = $debug_show_icon_prog;
                } else {
                    $kpiTargetName[$job]['kpi_prog'] = number_format($prefandprog->kpi_prog * 100, 2);
                    $kpiTargetName[$job]['kpi_prog_flag'] = $debug_show_icon_prog;
                }

                $debug_show_icon_perf = 0;
                if ($debug_mode == 'false') {
                    if ($prefandprog->kpi_perf > 100) {
                        $kpiperforming = 100;
                        $debug_show_icon_perf = 1;
                    } else {
                        $debug_show_icon_perf = 0;
                        $kpiperforming = number_format($prefandprog->kpi_perf * 100, 2);
                    }
                    $kpiTargetName[$job]['kpi_perf'] = $kpiperforming;
                    $kpiTargetName[$job]['kpi_perf_flag'] = $debug_show_icon_perf;
                } else {
                    $kpiTargetName[$job]['kpi_perf'] = number_format($prefandprog->kpi_perf * 100, 2);
                    $kpiTargetName[$job]['kpi_perf_flag'] = $debug_show_icon_perf;
                }

                $kpiTargetName[$job]['unit_name'] = $prefandprog->unit_name;


                $kpiName = $prefandprog->target_name;
                $kpiPerfCount = $kpiPerfCount + $prefandprog->kpi_perf;
                $kpiProgCount = $kpiProgCount + $prefandprog->kpi_prog;

                $kpiActual  = $kpiActual+ $prefandprog->kpi_value;
                $kpiTarget  = $kpiTarget + $prefandprog->kpi_target;
                $kpiBase    = $kpiBase + $prefandprog->kpi_base;
                $mn = $prefandprog->kpi_min;
                $mx = $prefandprog->kpi_max;
                $factor_1 = $prefandprog->perf_factor_1;
                $factor_2 = $prefandprog->perf_factor_2;
                $formula = $prefandprog->perf_formula_values;
                $formulaMathx = $prefandprog->perf_formula_mathx;

                $formulaAr = $prefandprog->perf_formula_values_ar;
                $formulaMathxAr = (string)$prefandprog->perf_formula_mathx_ar;

                $job++;
            }

            $target = $kpiTarget/$job;
            $value = $kpiActual/$job;
            $base = $kpiBase/$job;
            /*$mn = $mn;
            $mx = $mx;
            $factor_1 = $factor_1;
            $factor_2 = $factor_2;
            $formula = $formula;
            $formulaMathx = $formulaMathx;*/

            $token = array(
                '$target' => $target,
                '$value' => $value,
                '$base' => $base,
                '$mn' => $mn,
                '$mx' => $mx,
                '$factor_1' => $factor_1,
                '$factor_2' => $factor_2,
            );
            $pattern = '[%s]';
            foreach ($token as $key => $val) {
                $varMap[sprintf($pattern, $key)] = $val;
            }

            $formula = strtr($formula, $varMap);
            $formulaAr = strtr($formulaAr, $varMap);

            $progFormulaMathxEn = "$$ acc \: value \over target value $$";
            $progFormulaEn = "$$ ([$value]) \\over ([$target]) $$";

            $progFormulaMathxAr = "$$ القيمة \:الفعلية \over المستهدف  $$";
            $progFormulaAr = "$$ ([$value]) \\over ([$target]) $$";

            $valsperfprog[$ddd]['kpi_names'] = $kpiTargetName;
            $valsperfprog[$ddd]['icon_name'] = $prefandprog->icon_name;
            $valsperfprog[$ddd]['target_name'] = $kpiName;

            $debug_show_icon_prog = 0;
            if ($debug_progmode == 'false') {
                if (($kpiProgCount/$job) < 0) {
                    $kpi_progsum = 0;
                    $debug_show_icon_prog = 1;
                } else {
                    $kpi_progsum = (number_format(($kpiProgCount/$job) * 100, 2));
                    $debug_show_icon_prog = 0;
                }
                $valsperfprog[$ddd]['kpi_prog'] = $kpi_progsum;
                $valsperfprog[$ddd]['kpi_prog_flag'] = $debug_show_icon_prog;
            } else {
                $valsperfprog[$ddd]['kpi_prog'] = number_format(($kpiProgCount/$job) * 100, 2);
                $valsperfprog[$ddd]['kpi_prog_flag'] = $debug_show_icon_prog;
            }

            $debug_show_icon_perf = 0;
            if ($debug_mode == 'false') {
                if (($kpiPerfCount/$job) > 100) {
                    $kpiperforming = 100;
                    $debug_show_icon_perf = 1;
                } else {
                    $debug_show_icon_perf = 0;
                    $kpiperforming = number_format(($kpiPerfCount/$job) * 100, 2);
                }
                $valsperfprog[$ddd]['kpi_perf'] = $kpiperforming;
                $valsperfprog[$ddd]['kpi_perf_flag'] = $debug_show_icon_perf;
            } else {
                $valsperfprog[$ddd]['kpi_perf'] = number_format(($kpiPerfCount/$job) * 100, 2);
                $valsperfprog[$ddd]['kpi_perf_flag'] = $debug_show_icon_perf;
            }

           // $valsperfprog[$ddd]['kpi_perf'] = number_format($kpiPerfCount / $job * 100, 2);
            //$valsperfprog[$ddd]['kpi_prog'] = number_format($kpiProgCount / $job * 100, 2);
            $valsperfprog[$ddd]['perfformula'] = $formulaMathx;
            $valsperfprog[$ddd]['perfformulaData'] = $formula;
            $valsperfprog[$ddd]['perfformulaAr'] = $formulaMathxAr;
            $valsperfprog[$ddd]['perfformulaDataAr'] = $formulaAr;

            $valsperfprog[$ddd]['progformulaEn'] = $progFormulaMathxEn;
            $valsperfprog[$ddd]['progformulaDataEn'] = $progFormulaEn;
            $valsperfprog[$ddd]['progformulaAr'] = $progFormulaMathxAr;
            $valsperfprog[$ddd]['progformulaDataAr'] = $progFormulaAr;
            $i++;
        }

        $j = 0;
        $visionvalsperfprog = [];
        foreach ($visonResultPerfProg as $prefprogvalue) {
            $kpiPerfCount = $kpiProgCount = 0;
            $kpiActual = $kpiTarget = $kpiBase = 0;
            $kpiName = '';
            $jobs = 0;
            $kpiTargetNameVision = [];
            foreach ($prefprogvalue as $prefandprog) {
                //$kpiTargetNameVision[$jobs]['sort_no'] = $prefandprog->sort_no;
                $kpiTargetNameVision[$jobs]['kpi_name'] = $prefandprog->kpi_name;

                $kpiTargetNameVision[$jobs]['kpi_base'] = (($prefandprog->value_type == 2) ? number_format($prefandprog->kpi_base * 100, $prefandprog->rounding_decimals) . '%' : ($prefandprog->value_type == 5 ? $prefandprog->kpi_base_label : number_format(sprintf("%." . ($prefandprog->rounding_decimals) . "f", $prefandprog->kpi_base))));

                $kpiTargetNameVision[$jobs]['kpi_target'] = (($prefandprog->value_type == 2) ? number_format($prefandprog->kpi_target * 100, $prefandprog->rounding_decimals) . '%' : ($prefandprog->value_type == 5 ? $prefandprog->kpi_target_label : number_format(sprintf("%." . ($prefandprog->rounding_decimals) . "f", $prefandprog->kpi_target))));

                $kpiTargetNameVision[$jobs]['kpi_value'] = (($prefandprog->value_type == 2) ? number_format($prefandprog->kpi_value * 100, $prefandprog->rounding_decimals) . '%' : ($prefandprog->value_type == 5 ? $prefandprog->kpi_value_label : number_format(sprintf("%." . ($prefandprog->rounding_decimals) . "f", $prefandprog->kpi_value))));

                $kpiTargetNameVision[$jobs]['unit_name'] = $prefandprog->unit_name;

                /*$kpiTargetNameVision[$jobs]['kpi_perf'] = number_format($prefandprog->kpi_perf * 100, 2);
                $kpiTargetNameVision[$jobs]['kpi_prog'] = number_format($prefandprog->kpi_prog * 100, 2);*/

                $debug_show_icon_prog = 0;
                if ($debug_progmode == 'false') {
                    if ($prefandprog->kpi_prog < 0) {
                        $kpi_progsum = 0;
                        $debug_show_icon_prog = 1;
                    } else {
                        $kpi_progsum = (number_format($prefandprog->kpi_prog * 100, 2));
                        $debug_show_icon_prog = 0;
                    }
                    $kpiTargetNameVision[$jobs]['kpi_prog'] = $kpi_progsum;
                    $kpiTargetNameVision[$jobs]['kpi_prog_flag'] = $debug_show_icon_prog;
                } else {
                    $kpiTargetNameVision[$jobs]['kpi_prog'] = number_format($prefandprog->kpi_prog * 100, 2);
                    $kpiTargetNameVision[$jobs]['kpi_prog_flag'] = $debug_show_icon_prog;
                }

                $debug_show_icon_perf = 0;
                if ($debug_mode == 'false') {
                    if ($prefandprog->kpi_perf > 100) {
                        $kpiperforming = 100;
                        $debug_show_icon_perf = 1;
                    } else {
                        $debug_show_icon_perf = 0;
                        $kpiperforming = number_format($prefandprog->kpi_perf * 100, 2);
                    }
                    $kpiTargetNameVision[$jobs]['kpi_perf'] = $kpiperforming;
                    $kpiTargetNameVision[$jobs]['kpi_perf_flag'] = $debug_show_icon_perf;
                } else {
                    $kpiTargetNameVision[$jobs]['kpi_perf'] = number_format($prefandprog->kpi_perf * 100, 2);
                    $kpiTargetNameVision[$jobs]['kpi_perf_flag'] = $debug_show_icon_perf;
                }

                $kpiName = $prefandprog->target_name;
                $kpiPerfCount = $kpiPerfCount + $prefandprog->kpi_perf;
                $kpiProgCount = $kpiProgCount + $prefandprog->kpi_prog;

                $kpiActual  = $kpiActual+ $prefandprog->kpi_value;
                $kpiTarget  = $kpiTarget + $prefandprog->kpi_target;
                $kpiBase    = $kpiBase + $prefandprog->kpi_base;

                $mn = $prefandprog->kpi_min;
                $mx = $prefandprog->kpi_max;
                $factor_1 = $prefandprog->perf_factor_1;
                $factor_2 = $prefandprog->perf_factor_2;
                $formula = $prefandprog->perf_formula_values;
                $formulaMathx = $prefandprog->perf_formula_mathx;

                $formula = $prefandprog->perf_formula_values;
                $formulaMathx = $prefandprog->perf_formula_mathx;

                $formulaAr = $prefandprog->perf_formula_values_ar;
                $formulaMathxAr = $prefandprog->perf_formula_mathx_ar;

                $jobs++;
            }

            $target = $kpiTarget/$job;
            $value = $kpiActual/$job;
            $base = $kpiBase/$job;
            /*$mn = ($prefandprog->kpi_min) ? $prefandprog->kpi_min : 0;
            $mx = ($prefandprog->kpi_max) ? $prefandprog->kpi_max : $target * 1.2;
            $factor_1 = $prefandprog->perf_factor_1;
            $factor_2 = $prefandprog->perf_factor_2;
            $formula = $prefandprog->perf_formula_values;
            $formulaMathx = $prefandprog->perf_formula_mathx;*/

            $token = array(
                '$target' => $target,
                '$value' => $value,
                '$base' => $base,
                '$mn' => $mn,
                '$mx' => $mx,
                '$factor_1' => $factor_1,
                '$factor_2' => $factor_2,
            );
            $pattern = '[%s]';
            foreach ($token as $key => $val) {
                $varMap[sprintf($pattern, $key)] = $val;
            }

            $formula = strtr($formula, $varMap);
            $formulaAr = strtr($formulaAr, $varMap);

            $progFormulaMathxEn = "$$ acc value \over target value $$";
            $progFormulaEn = "$$ ([$value]) \\over ([$target]) $$";

            //(قيمة الأساس - القيمة الفعلية) \over (قيمة الأساس- المستهدف)
            //$progFormulaMathxAr = "$$ المستهدف - قيمة الأساس \over القيمة الفعلية- قيمة الأساس $$";
            $progFormulaMathxAr = "$$ القيمة الفعلية \over المستهدف  $$";
            $progFormulaAr = "$$ ([$value]) \\over ([$target]) $$";

            $visionvalsperfprog[$j]['kpi_names'] = $kpiTargetNameVision;
            $visionvalsperfprog[$j]['icon_name'] = $prefandprog->icon_name;
            $visionvalsperfprog[$j]['target_name'] = $kpiName;


            $debug_show_icon_prog = 0;
            if ($debug_progmode == 'false') {
                if (($kpiProgCount/$jobs) < 0) {
                    $kpi_progsum = 0;
                    $debug_show_icon_prog = 1;
                } else {
                    $kpi_progsum = (number_format(($kpiProgCount/$jobs) * 100, 2));
                    $debug_show_icon_prog = 0;
                }
                $visionvalsperfprog[$j]['kpi_prog'] = $kpi_progsum;
                $visionvalsperfprog[$j]['kpi_prog_flag'] = $debug_show_icon_prog;
            } else {
                $visionvalsperfprog[$j]['kpi_prog'] = number_format(($kpiProgCount/$jobs) * 100, 2);
                $visionvalsperfprog[$j]['kpi_prog_flag'] = $debug_show_icon_prog;
            }

            $debug_show_icon_perf = 0;
            if ($debug_mode == 'false') {
                if (($kpiPerfCount/$jobs) > 100) {
                    $kpiperforming = 100;
                    $debug_show_icon_perf = 1;
                } else {
                    $debug_show_icon_perf = 0;
                    $kpiperforming = number_format(($kpiPerfCount/$jobs) * 100, 2);
                }
                $visionvalsperfprog[$j]['kpi_perf'] = $kpiperforming;
                $visionvalsperfprog[$j]['kpi_perf_flag'] = $debug_show_icon_perf;
            } else {
                $visionvalsperfprog[$j]['kpi_perf'] = number_format(($kpiPerfCount/$jobs) * 100, 2);
                $visionvalsperfprog[$j]['kpi_perf_flag'] = $debug_show_icon_perf;
            }

            /*$visionvalsperfprog[$j]['kpi_perf'] = number_format($kpiPerfCount / $jobs * 100, 2);
            $visionvalsperfprog[$j]['kpi_prog'] = number_format($kpiProgCount / $jobs * 100, 2);*/

            $visionvalsperfprog[$j]['perfformula'] = $formulaMathx;
            $visionvalsperfprog[$j]['perfformulaData'] = $formula;
            $visionvalsperfprog[$j]['perfformulaAr'] = $formulaMathxAr;
            $visionvalsperfprog[$j]['perfformulaDataAr'] = $formulaAr;

            $visionvalsperfprog[$j]['progformulaEn'] = $progFormulaMathxEn;
            $visionvalsperfprog[$j]['progformulaDataEn'] = $progFormulaEn;
            $visionvalsperfprog[$j]['progformulaAr'] = $progFormulaMathxAr;
            $visionvalsperfprog[$j]['progformulaDataAr'] = $progFormulaAr;
            $j++;
        }

        if ($i > 3) {
            $countPerfProgKPI = 3;
        } elseif ($i > 2) {
            $countPerfProgKPI = 4;
        } elseif ($i > 1) {
            $countPerfProgKPI = 6;
        } else {
            $countPerfProgKPI = 12;
        }

        /*$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3"));*/
        $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
        if ($tenants) {
            $tenst = [];
            foreach ($tenants as $tena) {
                $tenst[] = $tena->id;
            }
        }

        $pieChart = \DB::select(\DB::raw("call p_unit_perf_prog_recursive($dept, $mtp, '$asofperiod', $yearno);"));
        $pieChartArray = [];
        $bubbleChartArray = [];
        $kpiBestLeastVals = [];
        $ii = 0;
        $jj = 0;
        foreach ($pieChart as $chartData) {
            if (in_array($chartData->sub_id, $tenst) && $chartData->sub_id != 2 ) {
                if ($chartData->perf_sum_w > 0) {
                    $kpiBestLeastVals[] = ($chartData->perf_sum_w) ? number_format($chartData->perf_sum_w * 100, 2) : 0;
                }
                $pieChartArray[$chartData->sub_id]['sub_id'] = $chartData->sub_id;
                $pieChartArray[$chartData->sub_id]['sub_name'] = $chartData->sub_name;
                $pieChartArray[$chartData->sub_id]['performance'] = ($chartData->perf_sum_w) ? number_format($chartData->perf_sum_w * 100, 2) : 0;
                $ii++;
            }

            if (in_array($chartData->sub_id, $tenst) && $chartData->kpi_count != '' && $chartData->sub_id != 2) {
                $tenantsName = \DB::select(\DB::raw("select id, short_name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3 and id=$chartData->sub_id"));

                $prog_sum_w = ($chartData->prog_sum_w) ? number_format($chartData->prog_sum_w * 100, 2) : 0;
                if (($prog_sum_w) && $prog_sum_w > 0) {
                    $x = number_format($chartData->prog_sum_w * 100, 2);
                }
                if (($prog_sum_w) && $prog_sum_w > 200) {
                    $x = 200;
                }

                if (($prog_sum_w) && $prog_sum_w < 0 || $prog_sum_w == 0) {
                    $x = 0;
                }

                $perf_sum_w = ($chartData->perf_sum_w) ? number_format($chartData->perf_sum_w * 100, 2) : 0;
                if (($perf_sum_w) && $perf_sum_w > 0) {
                    $y = number_format($chartData->perf_sum_w * 100, 2);
                    $perf_sum_w = round($perf_sum_w);
                    $size = number_format($perf_sum_w / 40, 2);
                }
                if (($perf_sum_w) && $perf_sum_w > 200) {
                    $y = 100;
                    $size = 4;
                }

                if (($perf_sum_w) && $perf_sum_w < 0 || $perf_sum_w == 0) {
                    $y = 0;
                    $size = 0;
                }

                if ($y != 0) {
                    $bubbleChartArray[$jj]['x'] = $x;
                    $bubbleChartArray[$jj]['y'] = $y;
                    $bubbleChartArray[$jj]['size'] = $size;
                    $bubbleChartArray[$jj]['short'] = ($tenantsName) ? $tenantsName[0]->short_name : '';
                    $bubbleChartArray[$jj]['text'] = $chartData->sub_name;
                    $jj++;
                }
            }
        }
        $kpisectorBest = count($kpiBestLeastVals) > 0 ? max($kpiBestLeastVals) : null;
        //$kpisectorBest = (number_format($kpisectorBest * 100, 2));
        $debug_show_icon_sector_best = 0;
        if ($debug_mode == 'false') {
            if ($kpisectorBest > 100) {
                $kpisectorBest = 100;
                $debug_show_icon_sector_best = 1;
            } else {
                $debug_show_icon_sector_best = 0;
            }

        }
        $kpisectorLeast = count($kpiBestLeastVals) > 0 ? min($kpiBestLeastVals): null;
        //$kpisectorLeast = (number_format($kpisectorLeast * 100, 2));
        $debug_show_icon_sector_least = 0;
        if ($debug_mode == 'false') {
            if ($kpisectorLeast > 100) {
                $kpisectorLeast = 100;
                $debug_show_icon_sector_least = 1;
            } else {
                $debug_show_icon_sector_least = 0;
            }

        }

        $dash_val = \DB::select(\DB::raw("call p_unit_perf_prog_recursive(
				$dept, -- a_subtenant_id: argument: (the parent subtenant id)
				/**
					-- Description: same as used with the old query:
					-- 1) if nothing is selected in the filter (i.e. null sector, null org unit)
									-> put the argument = 2 (it's the parent id of the sectors)
					-- 2) if sector in the filter is selected, and no org unit
									-> put the argument = id of the sector
					-- 3) if sector in the filter is selected, and org unit is selected
									-> put the argument = id of the org unit
				**/
				$mtp, -- a_mtp_id: argument
				'$asofperiod', -- fixed
				$yearno -- fixed
				);"));

        $mainVals = [];

        $activeCount = $notActiveCount = $uptodateCount = $notUpdatetodate = 0;
        $efficiencyCount = $strategyCount = $highCount = $midCount = $lowCount = 0;
        $activeCountSector = $activeCountStrategy = 0;
        $activeSectorName = '';
        $j = 0;
        foreach ($dash_val as $vals) {
//            if ($vals->sub_id != 2) {
//echo $vals->kpi_count;
            //&& $vals->kpi_count != ''
            $debug_progmode = 'false';
            if (in_array($vals->sub_id, $tenst)) {
                $vals->perf_sum_w = ($vals->perf_sum_w) ? (number_format($vals->perf_sum_w * 100, 2)) : 0;
                if ($vals->prog_sum_w < 0 && !$debug_progmode) {
                    $vals->prog_sum_w = 0;
                    $vals->show_icon = true;
                } else {
                    $vals->show_icon = false;
                    $vals->prog_sum_w = ($vals->prog_sum_w) ? (number_format($vals->prog_sum_w * 100, 2)) : 0;
                }
                $vals->by_strategy = ($vals->kpi_count - $vals->kpi_eff);
                $vals->not_uptodate = ($vals->kpi_count - $vals->kpi_up_to_date);
                $vals->not_active = ($vals->kpi_count - $vals->kpi_active);
                if ($vals->perf_sum_w > 0) {
                    $linksDept = "/departmentperformance" . "?query=ministry=".$vals->sub_id.",sector=".$vals->sub_id .",department=,supervision=,section=,mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno
                        .",fromministry=true,isDepartment=true,isSupervision=false,fromDepartment";

                    $vals->sectorlink = $linksDept;
                    /*"/departmentperformance" . "?query=" . $vals->sub_id . "," . $mtp . "," .
                        $asofperiod . "," . $yearno . "," . $dept . ",2,dept,fromministry";*/
                } else {
                    $vals->sectorlink = '#';
                }
                $mainVals[] = $vals;
                $j++;
            }
        }

        $dptperfprog = DB::select(
            " call p_unit_perf_prog_recursive($dept, $mtp,'$asofperiod',$yearno);"
        );
        $kpistrategic_perf = 0;
        $kpistrategic_perf = 0;
        $kpistrategic_prog = 0;
        $kpistrategic_prog = 0;
        $kpistrategic_kpi = 0;

        $ministry = $sector = '';
        if ($type == 0 || $type == 1) {
            $dptkpistrategic = DB::select("call p_unit_perf_prog_a_scope(
				$dept, $mtp,'$asofperiod',$yearno,'O');"
            );
            $kpistrategic_perfarr = [];
            $kpistrategic_progarr = [];
            $kpistrategic_kpiarr = [];
            $kpistrategicvalues = [];
            if ($dptkpistrategic) {
                if ($type == 0) {
                    $kpistrategic_perf = $dptkpistrategic[0]->kpi_perf;
                    $kpistrategic_perf = (number_format($kpistrategic_perf * 100, 2));
                    $kpistrategic_prog = $dptkpistrategic[0]->kpi_prog;
                    if ($debug_mode == 'false') {
                        if ($kpistrategic_prog < 0) {
                            $kpistrategic_prog = 0;
                            $debug_show_icon_progstrategic = 1;
                        } else {
                            $kpistrategic_prog = (number_format($dptkpistrategic[0]->kpi_prog * 100, 2));
                            $debug_show_icon_progstrategic = 0;
                        }

                    }
                    //$kpistrategic_prog =$kpistrategic_prog ;
                    $kpistrategic_kpi = $dptkpistrategic[0]->kpi_name;
                }
                if ($type == 1) {
                    foreach ($dptkpistrategic as $dptkpistrategicval) {
                        $kpistrategic_perf = $dptkpistrategicval->kpi_perf;
                        $kpistrategic_perfarr = (number_format($kpistrategic_perf * 100, 2));
                        $kpistrategic_prog = $dptkpistrategicval->kpi_prog;
                        $kpistrategic_progarr = (number_format($kpistrategic_prog * 100, 2));
                        $kpistrategic_kpiarr = $dptkpistrategicval->kpi_name;

                        if ($debug_progmode == 'false') {
                            if ($kpistrategic_progarr < 0) {
                                $kpistrategic_progarr = 0;
                                $debug_show_icon_prog = 1;
                            } else {
                                $kpistrategic_progarr = $kpistrategic_progarr;
                                $debug_show_icon_prog = 0;
                            }
                        }
                        $kpistrategicvalues[] = [
                            'kpigoalname' => $kpistrategic_kpiarr,
                            'kpigoalperf' => $kpistrategic_perfarr,
                            'kpigoalprog' => $kpistrategic_progarr,
                            'kpigoalkpiid' => $dptkpistrategicval->kpi_id

                        ];

                    }

                }
            }

        }

        /** Department */
        if ($type == 0) {
            $sectorlink = '';
            $sectorlinkar = '';
            foreach ($dptperfprog as $row) {
//                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
//                    $kpiperfarray[] = $row->perf_sum_w;
//
//                }
                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);
                if (isset($dataarray[0])) {
                    $supervisionarray[] = $dataarray[0];


                }

            }

            $supervision = '';
            $kpi_count = 0;
            $section = '';
            $supervisionname = '';
            $sectionname = '';
            $kpitotcount = $dptperfprog[0]->child_count_eff;
            $performingkpicount = $dptperfprog[0]->performing_count;
            $nonperformingkpicount = $kpitotcount - $performingkpicount;

            $kpiperfarray = [];
            foreach ($dptperfprog as $row) {
//echo $row->perf_sum_w;
                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
                    $kpiperfarray[] = $row->perf_sum_w;

                }
//            else{
//                $kpiperfarray[] =0;
//            }


                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);

                $dept = preg_replace('/[^A-Za-z0-9]/', '', $dataarray[0]);
                $deptnameval = \DB::select(\DB::raw("select name from subtenant where id=$dept"));
                $deptname = $deptnameval[0]->name;
                if (isset($dataarray[1])) {
                    $supervision = $dataarray[1];
                    $supervisionnameval = \DB::select(\DB::raw("select name from subtenant where id=$supervision"));
                    $supervisionname = $supervisionnameval[0]->name;
                }
                if (isset($dataarray[2])) {
                    $section = $dataarray[2];
                    $sectionnameval = \DB::select(\DB::raw("select name from subtenant where id=$section"));
                    $sectionname = $sectionnameval[0]->name;

                }


                if ($row->kpi_importance_sum == null) {
                    $row->kpi_importance_sum = 0;
                }
                if ($row->kpi_count == null) {
                    $row->kpi_count = 0;
                }
                $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$row->sub_id"));
                $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
//            if ($subtenant_type_idval == 9) {
//                $section = $supervision;
//                $sectionname = $supervisionname;
//            }
                $kpi_perfsum = (number_format($row->perf_sum_w * 100, 2));
                if ($debug_mode == 'false') {
                    if ($kpi_perfsum > 100) {
                        $kpi_perfsum = 100;
                        $debug_show_icon = 1;
                    } else {
                        $debug_show_icon = 0;
                    }

                }
                if ($kpi_perfsum >= 40 && $kpi_perfsum <= 60) {
                    $kpi_color = 1;

                } else {
                    $kpi_color = 0;
                }
                $kpid1[] = [


                    'supervision' => $supervision,
                    "dept" => $dept,
                    'section' => $section,
                ];

                $linksDept = "/departmentperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=true,isSupervision=false";

                $linksSuper = "/supervisionperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=false,isSupervision=false";

                $linksSection = "/sectionperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=false,isSupervision=false";

                if ($supervision == '') {
                    $supervision = $dept;
                    $deptidid = \DB::select(\DB::raw("select parent_id from subtenant where id=$dept"));
                    $deptidval = $deptidid[0]->parent_id;
                    $sectordid = \DB::select(\DB::raw("select parent_id from subtenant where id=$deptidval"));
                    $sectoridval = $sectordid[0]->parent_id;
//                $sectorlink='';
//                $sectorlinkar='';
//                if($supervision) {
//
//                    //$sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",supervision=" . $sectoridval . '>Show Section Report</a></div>';
//                   // $sectorlinkar = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",supervision=" . $sectoridval . '>عرض أداء القسم</a></div>';
//
//                   // $superlinkar = '<div style=float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=\'. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" .\'> عرض أداء المراقبة </a></div>\'<br><div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" . '> عرض أداء المراقبة </a></div>';
//                    $superlink='';
//
//
//                }
//            }
//            else {
//                if($this->arraycount($supervisionarray, $supervision)==1){
//                   // $superlink ='<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href='. "/departmentperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept".'">Show Supervision Report</a><br><a style="color:#ffffff;font-size:16px;" href='. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",sector=" . $sectoridval.'>Show Section Report</a></div>';
//                 //   $superlinkar ='<div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=\'. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" .\'> عرض أداء المراقبة </a></div>\'<br><div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href='. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" .'> عرض أداء المراقبة </a></div>';
//                   // $sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",sector=" . $sector . '>Show Section Report</a></div>';
//
//
//
//                }
                    if ($dataarray[0]) {
                        if ($this->arraycount($supervisionarray, $dataarray[0]) == 1) {
                            $sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';

                            $sectorlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';


                        } else {
                            $superlink = '';
                            $superlinkar = '';
                        }

                    }
                }

                if ($debug_progmode == 'false') {
                    if ($row->prog_sum_w < 0) {
                        $kpi_progsum = 0;
                        $debug_show_icon_prog_tool = 1;
                        $debug_show_icon_progic = '<i style="margin-left:10px;margin-right:10px;color:#ffffff" class="fas fa-exclamation-triangle fa-1x"></i>';
                    } else {
                        $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));

                        $debug_show_icon_prog_tool = 0;
                        $debug_show_icon_progic = '';
                    }
                } else {
                    $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));
                }

                $kpishowdata[] = [
                    'Category' => 'deptreport',
                    'deptname' => ($deptname) ? $deptname : null,
                    'supervision' => $supervision,
                    'section' => $section,
                    'kpi_importancesum' => $row->kpi_importance_sum,
                    'kpi_perfsum' => $kpi_perfsum,
                    'kpi_progsum' => $kpi_progsum,
                    'orgname' => $row->sub_name,
                    'calc_level' => $row->calc_level,
                    'supervisionname' => ($supervisionname) ? $supervisionname : null,
                    'sectionname' => ($sectionname) ? $sectionname : null,
                    'kpicount' => $row->kpi_count,
                    'kpicountIncrease' => ($row->kpi_count > 0 && $row->kpi_count < 10) ? $row->kpi_count+20 : $row->kpi_count,
                    'unitcount' => $row->unit_count,
                    "dept" => $dept,
                    "mtp" => $mtp,
                    "asofperiod" => $asofperiod,
                    "yearno" => $yearno,
                    "type" => $type,
                    "debug_show_icon" => $debug_show_icon,
                    "link" => $linksSection,
                    "supervisionlink" => $linksSuper,
                    /*"departmentlink" => "/departmentperformance" . "?query=section=" . $section . "," . $mtp . "," .
                        $asofperiod . "," . $yearno . "," . $dept,*/
                    "kpi_color" => $kpi_color,
                    "button" => $superlink,
                    "buttonar" => $superlinkar,
                    "sectorlink" => $sectorlink,
                    "sectorlinkar" => $sectorlinkar,
                    "debug_show_icon_prog_tool" => $debug_show_icon_prog_tool,
                    "debug_show_icon_progic" => $debug_show_icon_progic,
                    "debug_show_icon_perf_tool" => $debug_show_icon_perf_tool
                    //  "button"=>$button1


                ];


                $supervision = '';
                $section = '';
                $supervisionname = '';
                $sectionname = '';
                $dataarray = [];
            }


            $unitperformance = $dptperfprog[0]->perf_sum_w;
            $unitprogress = $dptperfprog[0]->prog_sum_w;

            $unitperformance = (number_format($unitperformance * 100, 2));
            $unitprogress = (number_format($unitprogress * 100, 2));
            $kpibestperforming = count($kpiperfarray) > 0 ? max($kpiperfarray) : null;
            $kpibestperforming = (number_format($kpibestperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpibestperforming > 100) {
                    $kpibestperforming = 100;
                    $debug_show_icon_best = 1;
                } else {
                    $debug_show_icon_best = 0;
                }

            }
            $kpileastperforming = count($kpiperfarray) > 0 ? min($kpiperfarray) : null;
            $kpileastperforming = (number_format($kpileastperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpileastperforming > 100) {
                    $kpileastperforming = 100;
                    $debug_show_icon_least = 1;
                } else {
                    $debug_show_icon_least = 0;
                }

            }
        }

        /** Supervision */
        if ($type == 1) {
            $sector = '';
            $sectorname = '';
            $supervision = '';
            $kpi_count = 0;
            $section = '';
            $supervisionname = '';
            $sectionname = '';
            $deptname = '';
            $dept = '';
            $kpitotcount = $dptperfprog[0]->child_count_eff;
            $performingkpicount = $dptperfprog[0]->performing_count;
            $nonperformingkpicount = $kpitotcount - $performingkpicount;
            $i = 0;
            $kpistrategic_sector = [];
            foreach ($dptperfprog as $row) {
//                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
//                    $kpiperfarray[] = $row->perf_sum_w;
//
//                }
                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);
                if (isset($dataarray[2])) {
                    $supervisionarray[] = $dataarray[2];


                }

            }
	    $kpiperfarray =[];
            foreach ($dptperfprog as $row) {

                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
                    $kpiperfarray[] = $row->perf_sum_w;

                }
//              var_dump($dptperfprog);
//                die();

                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);

                $sector = preg_replace('/[^A-Za-z0-9]/', '', $dataarray[0]);
                $sectornameval = \DB::select(\DB::raw("select name from subtenant where id=$sector"));
                $sectorname = $sectornameval[0]->name;
                if (isset($dataarray[1])) {
                    $dept = $dataarray[1];
                    $deptnameval = \DB::select(\DB::raw("select name from subtenant where id=$dept"));
                    $deptname = $deptnameval[0]->name;
                    $depts[] = $dept;

                }

                if (isset($dataarray[2])) {
                    $supervision = $dataarray[2];
                    $supervisionnameval = \DB::select(\DB::raw("select name from subtenant where id=$supervision"));
                    $supervisionname = $supervisionnameval[0]->name;

                }
                if (isset($dataarray[3])) {
                    $section = $dataarray[3];
                    $sectionnameval = \DB::select(\DB::raw("select name from subtenant where id=$section"));
                    $sectionname = $sectionnameval[0]->name;

                }


                if ($row->kpi_importance_sum == null) {
                    $row->kpi_importance_sum = 0;
                }
                if ($row->kpi_count == null) {
                    $row->kpi_count = 0;
                }
                $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$row->sub_id"));
                $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
//                if ($subtenant_type_idval == 9) {
//                    $section = $supervision;
//                    $sectionname = $supervisionname;
//                }
                $kpi_perfsum = (number_format($row->perf_sum_w * 100, 2));
                if ($debug_mode == 'false') {
                    if ($kpi_perfsum > 100) {
                        $kpi_perfsum = 100;
                        $debug_show_icon = 1;
                        $debug_show_icon_perf_tool = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $debug_show_icon = 0;
                        $debug_show_icon_perf_tool = '';
                    }

                }
                if ($kpi_perfsum >= 40 && $kpi_perfsum <= 60) {
                    $kpi_color = 1;

                } else {
                    $kpi_color = 0;
                }
                $kpid1[] = [

                    'sector' => $sector,

                    'supervision' => $supervision,
                    "dept" => $dept,
                    'section' => $section,
                ];

                $linksDept = "/departmentperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=true,isSupervision=false";

                $linksSuper = "/supervisionperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=false,isSupervision=false";

                $linksSection = "/sectionperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=false,isSupervision=false";

                if ($supervision) {
                    if ($this->arraycount($supervisionarray, $supervision) == 1) {
                        if ($supervision == 119) {
                            $superlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';

                            $superlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';
                        } else {
                            $superlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>Show Supervision Report</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';

                            $superlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper. '>عرض أداء المراقبة</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';
                        }


                    } else {
                        $superlink = '';
                        $superlinkar = '';
                    }

                }
                $button1 = '<i style="color:#ffffff" class="fas fa-exclamation-triangle fa-2x"></i>';;

                //$superlink='';
                // $superlink="/sectionperformance" . "?query=" . $section . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept.',sector='.$sector;
                if ($debug_progmode == 'false') {
                    if ($row->prog_sum_w < 0) {
                        $kpi_progsum = 0;
                        $debug_show_icon_prog_tool = 1;
                        $debug_show_icon_progic = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));

                        $debug_show_icon_prog_tool = 0;
                        $debug_show_icon_progic = '';

                    }
                } else {
                    $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));
                }

                $kpishowdata[] = [
                    'Category' => 'deptreport',
                    'sectorname' => ($sectorname) ? $sectorname : null,
                    'sector' => $sector,
                    'deptname' => ($deptname) ? $deptname : null,
                    'supervision' => $supervision,
                    'section' => $section,
                    'kpi_importancesum' => $row->kpi_importance_sum,
                    'kpi_perfsum' => $kpi_perfsum,
                    'kpi_progsum' => $kpi_progsum,
                    'orgname' => $row->sub_name,
                    'calc_level' => $row->calc_level,
                    'supervisionname' => ($supervisionname) ? $supervisionname : null,
                    'sectionname' => ($sectionname) ? $sectionname : null,
                    'kpicount' => $row->kpi_count,
                    'kpicountIncrease' => ($row->kpi_count > 0 && $row->kpi_count < 10) ? $row->kpi_count+20 : $row->kpi_count,
                    'unitcount' => $row->unit_count,
                    "dept" => $dept,
                    "mtp" => $mtp,
                    "asofperiod" => $asofperiod,
                    "yearno" => $yearno,
                    "type" => $type,
                    "debug_show_icon" => $debug_show_icon,
                    "link" => $linksSection,
                    "supervisionlink" => $linksSuper,
                    "departmentlink" => $linksDept,
                    'kpi_color' => $kpi_color,
                    "button" => $superlink,
                    "buttonar" => $superlinkar,
                    "debug_show_icon_prog_tool" => $debug_show_icon_prog_tool,
                    "debug_show_icon_progic" => $debug_show_icon_progic,
                    "debug_show_icon_perf_tool" => $debug_show_icon_perf_tool


                ];
                $supervisionarray[] = $supervision;

                $sectorname = '';
                $sector = '';
                $supervision = '';
                $section = '';
                $supervisionname = '';
                $sectionname = '';
                $dataarray = [];

                $i++;
            }
            array_unique($depts);


            $duplicate_keys = array();
            $tmp = array();

            foreach ($depts as $key => $val) {
                // convert objects to arrays, in_array() does not support objects
                if (is_object($val))
                    $val = (array)$val;

                if (!in_array($val, $tmp))
                    $tmp[] = $val;
                else
                    $duplicate_keys[] = $key;
            }

            foreach ($duplicate_keys as $key)
                unset($depts[$key]);
            $i = 0;
            foreach ($depts as $deptvalue) {
                $dptkpistrategic_sector = DB::select("call p_unit_perf_prog_a_scope(
				$deptvalue, $mtp,'$asofperiod',$yearno,'O');"
                );

                if (count($dptkpistrategic_sector) != 0) {
//                       var_dump($dptkpistrategic_sector);
//                       $kpistrategic_sector_perf = $dptkpistrategic_sector->kpi_perf;
//                       echo $kpistrategic_sector_perf;
//                       $kpistrategic_sector_perf = (number_format($kpistrategic_perf * 100, 2));
//                       $kpistrategic_sector_prog = $dptkpistrategic_sector[0]->kpi_prog;
//                        $kpistrategic_sector_prog = (number_format($kpistrategic_prog * 100, 2));
//                       $kpistrategic_sector_kpi = $dptkpistrategic_sector[0]->kpi_name;
//
//                        $kpistrategic_sector[$i]->perf=$kpistrategic_sector_perf;
//                       $kpistrategic_sector[$i]->prog=$kpistrategic_sector_prog;
//                       $kpistrategic_sector[$i]->kpi=$kpistrategic_sector_kpi;
//                        $kpistrategic_sector[$i]->dept=$dept;
                    $kpistrategic_sector[] = $dptkpistrategic_sector;


                }

                $i++;
            }

            $unitperformance = $dptperfprog[0]->perf_sum_w;
            $unitprogress = $dptperfprog[0]->prog_sum_w;

            $unitperformance = (number_format($unitperformance * 100, 2));
            $unitprogress = (number_format($unitprogress * 100, 2));
            $kpibestperforming = count($kpiperfarray) > 0 ?  max($kpiperfarray) : null;
            $kpibestperforming = (number_format($kpibestperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpibestperforming > 100) {
                    $kpibestperforming = 100;
                    $debug_show_icon_best = 1;
                } else {
                    $debug_show_icon_best = 0;
                }

            }
            $kpileastperforming = count($kpiperfarray) > 0 ? min($kpiperfarray) : null;
            $kpileastperforming = (number_format($kpileastperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpileastperforming > 100) {
                    $kpileastperforming = 100;
                    $debug_show_icon_least = 1;
                } else {
                    $debug_show_icon_least = 0;
                }

            }
        }

        /** Section */
        if ($type == 2) {
            $supervision = '';
            $kpi_count = 0;
            $section = '';
            $supervisionname = '';
            $sectionname = '';
            $sectorlink = '';
            $sectorlinkar = '';
            $kpitotcount = $dptperfprog[0]->child_count_eff;
            $performingkpicount = $dptperfprog[0]->performing_count;
            $nonperformingkpicount = $kpitotcount - $performingkpicount;

            foreach ($dptperfprog as $row) {
                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);
                if (isset($dataarray[0])) {
                    $supervisionarray[] = $dataarray[0];


                }

            }
	    $kpiperfarray =[];
            foreach ($dptperfprog as $row) {

                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
                    $kpiperfarray[] = $row->perf_sum_w;

                } else {
                    $kpiperfarray[] = 0;
                }


                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);

                $supervision = preg_replace('/[^A-Za-z0-9]/', '', $dataarray[0]);
                $supervisionnameval = \DB::select(\DB::raw("select name from subtenant where id=$supervision"));
                $supervisionname = $supervisionnameval[0]->name;

                if (isset($dataarray[1])) {
                    $section = $dataarray[1];
                    $sectionnameval = \DB::select(\DB::raw("select name from subtenant where id=$section"));
                    $sectionname = $sectionnameval[0]->name;

                }


                if ($row->kpi_importance_sum == null) {
                    $row->kpi_importance_sum = 0;
                }
                if ($row->kpi_count == null) {
                    $row->kpi_count = 0;
                }
                $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$row->sub_id"));
                $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
                $deptid = \DB::select(\DB::raw("select parent_id from subtenant where id=$supervision"));
                $deptval = $deptid[0]->parent_id;

                if ($subtenant_type_idval == 9) {
                    $section = $supervision;
                    $sectionname = $supervisionname;
                }
                $kpi_perfsum = (number_format($row->perf_sum_w * 100, 2));

                if ($debug_mode == 'false') {
                    if ($kpi_perfsum > 100) {
                        $kpi_perfsum = 100;
                        $debug_show_icon = 1;
                        $debug_show_icon_perf_tool = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $debug_show_icon = 0;
                        $debug_show_icon_perf_tool = '';
                    }


                }
                if ($kpi_perfsum >= 40 && $kpi_perfsum <= 60) {
                    $kpi_color = 1;

                } else {
                    $kpi_color = 0;
                }

//                if($supervision==''){
//                    $supervision=$dept;
//                    $deptidid = \DB::select(\DB::raw("select parent_id from subtenant where id=$dept"));
//                    $deptidval = $deptidid[0]->parent_id;
//                    $sectordid = \DB::select(\DB::raw("select parent_id from subtenant where id=$deptidval"));
//                    $sectoridval = $sectordid[0]->parent_id;
//                    if($supervision) {
//
//                        $sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",sector=" . $sectoridval . '>Show Section Report</a></div>';
//                        $sectorlinkar = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",sector=" . $sectoridval . '>عرض أداء القسم</a></div>';
//
//                        $superlinkar = '<div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=\'. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" .\'> عرض أداء المراقبة </a></div>\'<br><div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" . '> عرض أداء المراقبة </a></div>';
//                        $superlink='';
//
//
//                    }
//                }

                $linksDept = "/departmentperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=true,isSupervision=false";

                $linksSuper = "/supervisionperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=false,isSupervision=false";

                $linksSection = "/sectionperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=false,isSupervision=false";

                if ($supervision) {
                    if ($this->arraycount($supervisionarray, $supervision) == 1) {
                        if ($supervision == 119) {
                            $superlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';
                            $superlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';
                        } else {
                            $sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>Show Supervision Report</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';

                            $sectorlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>عرض أداء المراقبة</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';
                        }

                    } else {
                        $superlink = '';
                        $superlinkar = '';
                    }

                }
                $kpid1[] = [

                    'supervision' => $supervision,

                    'section' => $section,
                ];
                if ($debug_progmode == 'false') {
                    if ($row->prog_sum_w < 0) {
                        $kpi_progsum = 0;
                        $debug_show_icon_prog_tool = 1;
                        $debug_show_icon_progic = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));

                        $debug_show_icon_prog_tool = 0;
                        $debug_show_icon_progic = '';

                    }
                } else {
                    $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));
                }

                $kpishowdata[] = [
                    'Category' => 'deptreport',
                    'supervision' => $supervision,
                    'supervisionname' => ($supervisionname) ? $supervisionname : null,
                    'section' => $section,
                    'sectionname' => ($sectionname) ? $sectionname : null,
                    'kpi_importancesum' => $row->kpi_importance_sum,
                    'kpi_perfsum' => $kpi_perfsum,
                    'kpi_progsum' => $kpi_progsum,
                    'orgname' => $row->sub_name,
                    'calc_level' => $row->calc_level,
                    'kpicount' => $row->kpi_count,
                    'kpicountIncrease' => ($row->kpi_count > 0 && $row->kpi_count < 10) ? $row->kpi_count+20 : $row->kpi_count,
                    'unitcount' => $row->unit_count,
                    "mtp" => $mtp,
                    "asofperiod" => $asofperiod,
                    "yearno" => $yearno,
                    "type" => $type,
                    "debug_show_icon" => $debug_show_icon,
                    "link" => $linksSection,
                    "supervisionlink" =>$linksSuper,
                    "departmentlink" => $linksDept,
                    "kpi_color" => $kpi_color,
                    "sectorlink" => $sectorlink,
                    "sectorlinkar" => $sectorlinkar,
                    "debug_show_icon_prog_tool" => $debug_show_icon_prog_tool,
                    "debug_show_icon_progic" => $debug_show_icon_progic,
                    "debug_show_icon_perf_tool" => $debug_show_icon_perf_tool
                ];


                $supervision = '';
                $section = '';
                $supervisionname = '';
                $sectionname = '';
                $dataarray = [];


            }
            $unitperformance = $dptperfprog[0]->perf_sum_w;
            $unitprogress = $dptperfprog[0]->prog_sum_w;

            $unitperformance = (number_format($unitperformance * 100, 2));
            $unitprogress = (number_format($unitprogress * 100, 2));
            $kpibestperforming = count($kpiperfarray) > 0 ? max($kpiperfarray) : null;
            $kpibestperforming = (number_format($kpibestperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpibestperforming > 100) {
                    $kpibestperforming = 100;
                    $debug_show_icon_best = 1;
                } else {
                    $debug_show_icon_best = 0;
                }

            }
            $kpileastperforming = count($kpiperfarray) > 0 ? min($kpiperfarray) : null ;
            $kpileastperforming = (number_format($kpileastperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpileastperforming > 100) {
                    $kpileastperforming = 100;
                    $debug_show_icon_least = 1;
                } else {
                    $debug_show_icon_least = 0;
                }

            }
        }

        /** Ministry */
        if ($type == 3) {
            $sector = '';
            $sectorname = '';
            $supervision = '';
            $kpi_count = 0;
            $section = '';
            $supervisionname = '';
            $sectionname = '';
            $deptname = '';
            $dept = '';
            $kpitotcount = $dptperfprog[0]->child_count_eff;
            $performingkpicount = $dptperfprog[0]->performing_count;
            $nonperformingkpicount = $kpitotcount - $performingkpicount;
            $i = 0;
            $kpistrategic_sector = [];
            $departmentarray = [];
            foreach ($dptperfprog as $row) {
//                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
//                    $kpiperfarray[] = $row->perf_sum_w;
//
//                }

                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);
                if (isset($dataarray[3])) {
                    $supervisionarray[] = $dataarray[3];
                }
                if (isset($dataarray[2])) {
                    $departmentarray[] = $dataarray[2];
                }
            }
	    $kpiperfarray =[];
            foreach ($dptperfprog as $row) {

                $ministry = $dept = $sector = $supervision = $section = '';
                $sectorname = $ministryname = $deptname = $supervisionname = $sectionname = '';
                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
                    $kpiperfarray[] = $row->perf_sum_w;

                }
//              var_dump($dptperfprog);
//                die();

                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);
//echo "<pre>";
                //              var_dump($dataarray);
                $ministry = preg_replace('/[^A-Za-z0-9]/', '', $dataarray[0]);
                if($ministry != 2) {

                }
                $ministrynameval = \DB::select(\DB::raw("select name from subtenant where id=$ministry"));
                $ministryname = $ministrynameval[0]->name;
                if (!empty($dataarray[1])) {
                    $sector = $dataarray[1];
                    $sectornameval = \DB::select(\DB::raw("select name from subtenant where id=$sector"));
                    $sectorname = $sectornameval[0]->name;
                    $secoreArray[] = $sector;
                    // $depts[] = $dept;
                    //echo $sector;

                }

                if (!empty($dataarray[2])) {
                    $dept = $dataarray[2];
                    $deptnameval = \DB::select(\DB::raw("select name from subtenant where id=$dept"));
                    $deptname = $deptnameval[0]->name;
                    $depts[] = $dept;

                }

                if (!empty($dataarray[3])) {
                    $supervision = $dataarray[3];
                    $supervisionnameval = \DB::select(\DB::raw("select name from subtenant where id=$supervision"));
                    $supervisionname = $supervisionnameval[0]->name;

                }
                if (!empty($dataarray[4])) {
                    $section = $dataarray[4];
                    $sectionnameval = \DB::select(\DB::raw("select name from subtenant where id=$section"));
                    $sectionname = $sectionnameval[0]->name;

                }


                if ($row->kpi_importance_sum == null) {
                    $row->kpi_importance_sum = 0;
                }
                if ($row->kpi_count == null) {
                    $row->kpi_count = 0;
                }
                $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$row->sub_id"));
                $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
//                if ($subtenant_type_idval == 9) {
//                    $section = $supervision;
//                    $sectionname = $supervisionname;
//                }
                $kpi_perfsum = (number_format($row->perf_sum_w * 100, 2));
                if ($debug_mode == 'false') {
                    if ($kpi_perfsum > 100) {
                        $kpi_perfsum = 100;
                        $debug_show_icon = 1;
                        $debug_show_icon_perf_tool = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $debug_show_icon = 0;
                        $debug_show_icon_perf_tool = '';
                    }

                }
                if ($kpi_perfsum >= 40 && $kpi_perfsum <= 60) {
                    $kpi_color = 1;

                } else {
                    $kpi_color = 0;
                }
                $kpid1[] = [

                    'ministry' => $ministry,
                    'sector' => $sector,
                    'supervision' => $supervision,
                    "dept" => $dept,
                    'section' => $section,
                ];

                $linksDept = "/departmentperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=true,isSupervision=false";

                $linksSuper = "/supervisionperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=false,isSupervision=false";

                $linksSection = "/sectionperformance" . "?query=ministry=".$ministry.",sector=".$sector.",department=" .$dept .",supervision=" .$supervision.",section=".$section.",mtp=".$mtp.",period=".$asofperiod . ",year=" . $yearno.",fromministry=true,isDepartment=false,isSupervision=false";

                if ($supervision) {
                    if ($this->arraycount($supervisionarray, $supervision) == 1) {
                        $linkssss = "/departmentperformance" . "?query=section=" . $dept . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept;
                        if ($supervision == 119) {
                            $superlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';
                            $superlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';
                        } else {
                            $superlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>Show Supervision Report</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';
                            $superlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>عرض أداء المراقبة</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';
                        }
                    } else {
                        $superlink = '';
                        $superlinkar = '';
                    }


                }
                if ($dept) {
                    if ($this->arraycount($departmentarray, $dept) == 1) {
                        /*$linkssss = "/departmentperformance" . "?query=section=" . $dept . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept;*/
                        $deptsuperlink = '<div style="color:#ffffff" >
<a style="color:#ffffff;font-size:16px;" href=' . $linksDept . '>Show Department Report</a><br>
<a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>Show Supervision Report</a><br>
<a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';

                        $deptsuperlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" >
<a style="color:#ffffff;font-size:16px;left:-15px;" href=' . $linksDept . '>عرض أداء الإدارة</a><br>
<a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>عرض أداء المراقبة</a><br>
<a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a>
</div>';


                    } else {
                        $deptsuperlink = '';
                        $deptsuperlinkar = '';
                    }
                }
                $button1 = '<i style="color:#ffffff" class="fas fa-exclamation-triangle fa-2x"></i>';;

                //$superlink='';
                // $superlink="/sectionperformance" . "?query=" . $section . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept.',sector='.$sector;
                if ($debug_progmode == 'false') {
                    if ($row->prog_sum_w < 0) {
                        $kpi_progsum = 0;
                        $debug_show_icon_prog_tool = 1;
                        $debug_show_icon_progic = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));

                        $debug_show_icon_prog_tool = 0;
                        $debug_show_icon_progic = '';

                    }
                } else {
                    $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));
                }
                $kpishowdata[] = [
                    'Category' => 'deptreport',
                    'ministryname' => 'وزارة المالية',//$ministryname,
                    'ministry' => $ministry,
                    'sectorname' => ($sectorname) ? $sectorname : null,
                    'sector' => $sector,
                    'deptname' => ($deptname) ? $deptname : null,
                    'supervision' => $supervision,
                    'section' => $section,
                    'kpi_importancesum' => $row->kpi_importance_sum,
                    'kpi_perfsum' => $kpi_perfsum,
                    'kpi_progsum' => $kpi_progsum,
                    'orgname' => $row->sub_name,
                    'calc_level' => $row->calc_level,
                    'supervisionname' => ($supervisionname) ? $supervisionname : null,
                    'sectionname' => ($sectionname) ? $sectionname : null,
                    'kpicount' => $row->kpi_count,
                    'kpicountIncrease' => ($row->kpi_count > 0 && $row->kpi_count < 10) ? $row->kpi_count+20 : $row->kpi_count,
                    'unitcount' => $row->unit_count,
                    "dept" => $dept,
                    "mtp" => $mtp,
                    "asofperiod" => $asofperiod,
                    "yearno" => $yearno,
                    "type" => $type,
                    "debug_show_icon" => $debug_show_icon,
                    "link" => $linksSection,
                    "supervisionlink" => $linksSuper,
                    "departmentlink" => $linksDept,
                    'kpi_color' => $kpi_color,
                    "button" => $superlink,
                    "buttonar" => $superlinkar,
                    "debug_show_icon_prog_tool" => $debug_show_icon_prog_tool,
                    "debug_show_icon_progic" => $debug_show_icon_progic,
                    "debug_show_icon_perf_tool" => $debug_show_icon_perf_tool,
                    "deptsuperlink" => $deptsuperlink,
                    "deptsuperlinkar" => $deptsuperlinkar


                ];
                $supervisionarray[] = $supervision;

                $sectorname = '';
                $sector = '';
                $supervision = '';
                $section = '';
                $supervisionname = '';
                $sectionname = '';
                $dataarray = [];

                $i++;
            }
//            die();
            array_unique($depts);


            $duplicate_keys = array();
            $tmp = array();

            foreach ($depts as $key => $val) {
                // convert objects to arrays, in_array() does not support objects
                if (is_object($val))
                    $val = (array)$val;

                if (!in_array($val, $tmp))
                    $tmp[] = $val;
                else
                    $duplicate_keys[] = $key;
            }

            foreach ($duplicate_keys as $key)
                unset($depts[$key]);
            $i = 0;
            foreach ($depts as $deptvalue) {
                $dptkpistrategic_sector = DB::select("call p_unit_perf_prog_a_scope(
				$deptvalue, $mtp,'$asofperiod',$yearno,'O');"
                );

                if (count($dptkpistrategic_sector) != 0) {
//                       var_dump($dptkpistrategic_sector);
//                       $kpistrategic_sector_perf = $dptkpistrategic_sector->kpi_perf;
//                       echo $kpistrategic_sector_perf;
//                       $kpistrategic_sector_perf = (number_format($kpistrategic_perf * 100, 2));
//                       $kpistrategic_sector_prog = $dptkpistrategic_sector[0]->kpi_prog;
//                        $kpistrategic_sector_prog = (number_format($kpistrategic_prog * 100, 2));
//                       $kpistrategic_sector_kpi = $dptkpistrategic_sector[0]->kpi_name;
//
//                        $kpistrategic_sector[$i]->perf=$kpistrategic_sector_perf;
//                       $kpistrategic_sector[$i]->prog=$kpistrategic_sector_prog;
//                       $kpistrategic_sector[$i]->kpi=$kpistrategic_sector_kpi;
//                        $kpistrategic_sector[$i]->dept=$dept;
                    $kpistrategic_sector[] = $dptkpistrategic_sector;


                }

                $i++;
            }

            $unitperformance = $dptperfprog[0]->perf_sum_w;
            $unitprogress = $dptperfprog[0]->prog_sum_w;

            $unitperformance = (number_format($unitperformance * 100, 2));
            $unitprogress = (number_format($unitprogress * 100, 2));
            $kpibestperforming = count($kpiperfarray) > 0 ? max($kpiperfarray) : null;
            $kpibestperforming = (number_format($kpibestperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpibestperforming > 100) {
                    $kpibestperforming = 100;
                    $debug_show_icon_best = 1;
                } else {
                    $debug_show_icon_best = 0;
                }

            }
            $kpileastperforming = count($kpiperfarray) > 0 ? min($kpiperfarray) : null;
            $kpileastperforming = (number_format($kpileastperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpileastperforming > 100) {
                    $kpileastperforming = 100;
                    $debug_show_icon_least = 1;
                } else {
                    $debug_show_icon_least = 0;
                }

            }
        }

        if ($debug_progmode == 'false') {
            if ($unitprogress < 0) {
                $unitprogress = 0;
                $debug_show_icon_prog = 1;
            } else {
                $unitprogress = $unitprogress;
                $debug_show_icon_prog = 0;
            }
        }

        //var_dump($kpid1);
        //die;
        header('Content-type: application/json');
        if ($kpishowdata) {
            if ($type != 1) {
                return response()->json([
                    "code" => 200,
                    "kpid1" => $kpid1,
                    "kpishowdata" => $kpishowdata,
                    "unitperformance" => $unitperformance,
                    "unitprogress" => $unitprogress,
                    "kpi_count" => $kpitotcount,
                    'unitcount' => $row->unit_count,
                    "kpistrategic_perf" => $kpistrategic_perf,
                    "kpistrategic_prog" => $kpistrategic_prog,
                    "kpistrategic_kpi" => $kpistrategic_kpi,
                    "kpibestperforming" => $kpibestperforming,
                    "kpileastperforming" => $kpileastperforming,
                    "performingcount" => $performingkpicount,
                    "nonperformingcount" => $nonperformingkpicount,
                    "kpistrategic_sector" => $kpistrategic_sector,
                    "debug_show_icon_best" => $debug_show_icon_best,
                    "debug_show_icon_least" => $debug_show_icon_least,
                    "debug_show_icon_prog" => $debug_show_icon_prog,
                    "debug_progmode" => $debug_progmode,
                    "debug_show_icon_progstrategic" => $debug_show_icon_progstrategic,

                    "sectorData" => $mainVals,
                    "getKpiPerfProg" => $valsperfprog,
                    "getVisionKpiPerfProg" => $visionvalsperfprog,
                    "countPerfProgKPI" => $countPerfProgKPI,
                    "pieChartArray" => $pieChartArray,
                    "bubbleChartArray" => $bubbleChartArray,
                    "kpisectorbest" => $kpisectorBest,
                    "kpisectorleast" => $kpisectorLeast,
                    "debug_show_icon_sector_best" => $debug_show_icon_sector_best,
                    "debug_show_icon_sector_least" => $debug_show_icon_sector_least,


                ]);
            } else {
                return response()->json([
                    "code" => 200,
                    "kpid1" => $kpid1,
                    "kpishowdata" => $kpishowdata,
                    'unitcount' => $row->unit_count,
                    "unitperformance" => $unitperformance,
                    "unitprogress" => $unitprogress,
                    "kpi_count" => $kpitotcount,
                    "kpistrategic_perf" => $kpistrategic_perfarr,
                    "kpistrategic_prog" => $kpistrategic_progarr,
                    "kpistrategic_kpi" => $kpistrategic_kpiarr,
                    "kpibestperforming" => $kpibestperforming,
                    "kpileastperforming" => $kpileastperforming,
                    "performingcount" => $performingkpicount,
                    "nonperformingcount" => $nonperformingkpicount,
                    "kpistrategic_sector" => $kpistrategic_sector,
                    "kpistrategicvalues" => $kpistrategicvalues,
                    "debug_show_icon_best" => $debug_show_icon_best,
                    "debug_show_icon_least" => $debug_show_icon_least,
                    "debug_show_icon_prog" => $debug_show_icon_prog,
                    "debug_progmode" => $debug_progmode,
                    "debug_show_icon_progstrategic" => $debug_show_icon_progstrategic,
                    "debug_show_icon_prog_tool" => $debug_show_icon_prog_tool,
                    "debug_show_icon_progic" => $debug_show_icon_progic,

                    "sectorData" => $mainVals,
                    "getKpiPerfProg" => $valsperfprog,
                    "getVisionKpiPerfProg" => $visionvalsperfprog,
                    "countPerfProgKPI" => $countPerfProgKPI,
                    "pieChartArray" => $pieChartArray,
                    "bubbleChartArray" => $bubbleChartArray,
                    "kpisectorbest" => $kpisectorBest,
                    "kpisectorleast" => $kpisectorLeast,
                    "debug_show_icon_sector_best" => $debug_show_icon_sector_best,
                    "debug_show_icon_sector_least" => $debug_show_icon_sector_least,

                ]);
            }
        }

        return response()->json(["code" => 400]);
    }


    public function loadSubTenants($id)
    {
        //$subtenants = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), cast(id as char(200)) from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '-'), '>', s.name), s.parent_id, CONCAT(c.level, '-'), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id) select id, name from cte order by path"));
        $subtenants = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = $id and subtenant_type_id !=6 UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), $id, '', CONCAT(id, '') from subtenant where parent_id = $id and subtenant_type_id !=6) select id, name from cte order by path"));

        // $subtenants = \DB::select(\DB::raw("select id, name from subtenant where parent_id = $id and subtenant_type_id=4"));
        foreach ($subtenants as $subtenant) {
//            $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$subtenant->id"));
//            $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
            //echo "in";
            $word = "==>";

            if (strpos($subtenant->name, $word) !== false) {
//               //echo $subtenant->name;
                unset($subtenant->name);
                unset($subtenant->id);
                unset($subtenant);

            }
//
//
        }
////        var_dump($subtenants);
////die();
        //  $subtenants=  array_filter($subtenants);
        foreach ($subtenants as $key => $val) {
            if ($val === null || $val === '')
                unset($subtenants[$key]);
        }
        if ($subtenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $subtenants
            ]);
        }
    }

    public function getsubtenanttype($value)
    {
        $subtenant = $value;
        $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$value"));
        $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
        if ($subtenant_type_idval) {
            return response()->json([
                "code" => 200,
                "subtenanttype" => $subtenant_type_idval
            ]);
        }
    }

    public function arraycount($array, $value)
    {
        $counter = 0;
        foreach ($array as $thisvalue) /*go through every value in the array*/ {
            if ($thisvalue === $value) { /*if this one value of the array is equal to the value we are checking*/
                $counter++; /*increase the count by 1*/
            }
        }
        return $counter;
    }


}
