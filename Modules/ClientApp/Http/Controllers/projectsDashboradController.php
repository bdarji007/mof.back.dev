<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Maher\Counters\Facades\Counters;
use Maher\Counters\Models\Counter;
use Modules\ClientApp\Dynamic;
use Modules\ClientApp\Entities\Projects;
use Modules\ClientApp\Entities\Projectscost;
use DateTime;
use Modules\ClientApp\User;
use Spatie\Permission\Models\Role;


class projectsDashboradController extends Controller
{
    //
    public function index()
    {


    }

    public function projectdashboardtreereport($value)
    {
        $debug_mode = true;
        $debug_show_icon = 0;
        $debug_show_icon_best = 0;
        $debug_show_icon_least = 0;
        $kpi_perf = 0;
        $debug_progmode = 0;
        $debug_show_icon_prog = 0;
        $debug_tooltip_prog = 0;
        $valuearray = explode(",", $value);
        $supervisionback='';
        $ministryback='';

        $section = $valuearray[0];
        $mtp = $valuearray[1];
        $asofperiod = $valuearray[2];
        $yearno = $valuearray[3];
        if($valuearray[4]) {
            $supervisionback = $valuearray[4];
        }
        if($valuearray[5]) {
            $ministryback = $valuearray[5];
        }

        $deptid = \DB::select(\DB::raw("select parent_id from subtenant where id=$section"));
        $deptval = $deptid[0]->parent_id;
        $supervision=$deptval;

        $sectorpid = \DB::select(\DB::raw("select parent_id from subtenant where id=$deptval"));
        $sectorpval = $sectorpid[0]->parent_id;
        $sectorid = \DB::select(\DB::raw("select parent_id from subtenant where id=$sectorpval"));
        $sectorval = $sectorid[0]->parent_id;


        if ($sectorval == 2) {
            $sectorval = $sectorpval;
        }
        $perfprog = DB::select(
            "call p_unit_perf_prog($section,$mtp,'$asofperiod',$yearno);"
        );
//       var_dump($perfprog);
//       die();
        $acheivedcount = 0;
        $uptodatecount = 0;
        $unitperformance = 0;
        $unitprogress = 0;
        $nonuptodatecount = 0;
        $performingkpicount = 0;
        $nonperformingkpicount = 0;

        foreach ($perfprog as $row) {
            if ($row->kpi_perf * 100 >= 50) {
                $acheivedcount = $acheivedcount + 1;
                $performingkpicount = $performingkpicount + 1;
            }
            if ($row->kpi_perf * 100 < 50) {

                $nonperformingkpicount = $nonperformingkpicount + 1;
            }

            $weighedperf = $row->adjusted_weight * $row->kpi_perf;
            $weighedprog = $row->adjusted_weight * $row->kpi_prog;
            $unitperformance = $unitperformance + $weighedperf;
            $unitprogress = $unitprogress + $weighedprog;
            if ($row->kpi_up_to_date == 1) {
                $uptodatecount = $uptodatecount + 1;

            }
            if ($row->kpi_up_to_date == 0) {
                $nonuptodatecount = $nonuptodatecount + 1;

            }


            if ($debug_mode == true) {
                if ($row->kpi_perf * 100 > 100) {
                    $kpi_perf = 100;
                    $debug_show_icon = 1;
                    $debug_show_icon_perf_tool = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                } else {
                    $kpi_perf = $row->kpi_perf * 100;
                    $debug_show_icon = 0;
                    $debug_show_icon_perf_tool = '';

                }

            }
            if ($debug_progmode == false) {
                if ($row->kpi_prog * 100 < 0) {
                    $kpi_prog = 0;
                    $debug_show_icon_prog = 1;
                } else {
                    $kpi_prog = $row->kpi_prog * 100;
                    $kpi_prog = (number_format($kpi_prog, 2));
                    $debug_show_icon_prog = 0;
                }

            }

            $uptodateicon = '';
            $debugicon = '';

            if ($row->kpi_up_to_date == 0) {
                $uptodateicon = '<i class="fas fa-exclamation-circle fa-2x"></i>';
            }
            if ($debug_show_icon == 1 || $debug_show_icon_prog == 1) {
                $debugicon = '<i style="color:#ffffff" class="fas fa-exclamation-triangle fa-2x"></i>';
            }

            $gaugelink = "gaugechart/" . $row->kpi_id . "?query=" . $section . "," . $mtp . "," . $asofperiod . "," . $yearno . ",fromsector," . $sectorval;
            if($supervisionback!='' || $supervisionback!='') {
                if ($supervisionback == 'supervision') {
                    $superbacklink = ',' . $supervision;
                    $gaugelink = $gaugelink . $superbacklink;
                }
                if ($ministryback == 'ministry') {
                    $ministrybacklink = ',' . 2;
                    $gaugelink = $gaugelink . $ministrybacklink;
                }
            }
            else{
                $gaugelink = "gaugechart/" . $row->kpi_id . "?query=" . $section . "," . $mtp . "," . $asofperiod . "," . $yearno . ",fromsector," . $sectorval;

            }
            $kpishowdata[] = [
                'KPIdata' => $row->kpi_symbol,

                'importance' => $row->weight,
                'importancetxt' => 'Importance: ' . $row->weight,
                'performance' => $kpi_perf,
                'performancetxt' => (number_format($kpi_perf)) . '%',
                'progress' => $kpi_prog . '%',
                'KPIname' => $row->kpi_name,
                'adjustedweight' => (number_format($row->adjusted_weight * 100, 2)) . '%',
                'weightedperformance' => (number_format(($row->adjusted_weight * $row->kpi_perf) * 100, 2)) . '%',
                'kpi_up_to_date' => $row->kpi_up_to_date,
                'debug_show_icon' => $debug_show_icon,
                "link" => $gaugelink,
                "button" => ($uptodateicon || $debugicon) ? '<div style="color:#ffffff" >' . $uptodateicon . $debugicon
                    .'</div>' : '',

            ];
        }
        $unitperformance = (number_format($unitperformance * 100, 2));
        $unitprogress = (number_format($unitprogress * 100, 2));
        if ($debug_progmode == 0) {
            if ($unitprogress < 0) {
                $unitprogress = 0;
                $debug_show_icon_prog = 1;
            } else {
                $unitprogress = $unitprogress;
                $debug_show_icon_prog = 0;
            }
        }


        header('Content-type: application/json');
        //   echo json_encode($kpishowdata);


        if ($kpishowdata) {
            return response()->json([
                "code" => 200,
                "kpishowdata" => $kpishowdata,
                "totalkpi" => count($perfprog),
                "acheivedcount" => $acheivedcount,
                "uptodatecount" => $uptodatecount,
                "unitperformance" => $unitperformance,
                "unitprogress" => $unitprogress,
                "nonuptodatecount" => $nonuptodatecount,
                "performingkpicount" => $performingkpicount,
                "nonperformingkpicount" => $nonperformingkpicount,
                "debug_show_icon_prog" => $debug_show_icon_prog


            ]);
        }

        return response()->json(["code" => 400]);
    }



}
