<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Modules\ClientApp\Reports\MyReport;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->user = \JWTAuth::parseToken()->authenticate() ;
        //$this->middleware("guest");
    }


    public function index()//(Request $request)
    {
        // $language=$request->lang;
//var_dump($language);
        $report = new MyReport;
        // (array(
           // "language"    =>$language,
        // ));
        $report->run();
        return view("report",["report"=>$report]);
    }

    /***
     * @param Request $request
     * Upload Image form Ej-Synfucion Rich Editor
     *
     */
    public function notificationsaveImage(Request $request) {
        if (!empty($request->UploadFiles->getClientOriginalName())) {
            $fileName = $request->UploadFiles->getClientOriginalName();
            if (file_exists(public_path('richeditor') . '/' . $fileName)) {
                unlink(public_path('richeditor') . '/' . $fileName);
            }
        }
        $imageName = $fileName;
        if (! File::exists(public_path('richeditor'))) {
            File::makeDirectory(public_path("richeditor"), $mode = 0777, true, true);
        }
        $request->UploadFiles->move(public_path('richeditor'), $imageName);
    }

    public function authUser(Request $request, $id = null)
    {
        if ($id != 'null') {
            $vals = (explode("-", $id));
            $id = $vals[1];
        }
        $url = explode("/api/", url()->current());
        $this->user->url = $url[0];

        if ($this->user) {
            //$userType = TenantUserType::find($this->user->user_type);
            //$this->user->roles = $userType->name;
            $this->user->roles = auth()->user()->getRoleNames();
        }

        $userPremissions = [];

        $allPermissions = [];
        $rolesMain = [];
        $rolesSec = [];
        foreach (auth()->user()->roles as $role) {
            if($role->is_main == 1) {
                $rolesMain[$role->name] = $role->name;
                $rolesSec[$role->name] = $role->name;
            } else {
                $rolesSec[$role->name] = $role->name;
            }
        }
        sort($rolesMain);
        sort($rolesSec);

        $sqlAdd = '';
        if(count($rolesMain) > 0 ) {
            //$sqlAdd .= " and obj.role='".$rolesMain[0]."'";
            $sqlAdd .= " and obj.role='".auth()->user()->currentRole."'";
        } else {
            //$sqlAdd .= " and obj.role='".$rolesSec[0]."'";
            $sqlAdd .= " and obj.role='".auth()->user()->currentRole."'";
        }
        $checkRoles = \DB::select(\DB::raw("SELECT app.id, model.name FROM check_roles_group app INNER JOIN app_objects obj on obj.app_id=app.id INNER JOIN object_model model on model.id= obj.object_id WHERE app.id=$id $sqlAdd"));
        //var_dump($checkRoles);

        $permissions = Permission::all();
        if ($permissions) {
            //$userGroupPremissions = [];
            foreach (Permission::all() as $permission) {
                if (Auth::user()->can($permission->name)) {

                    $allPermissions[] = $permission->name;
                    if ($checkRoles) {
                        foreach ($checkRoles as $permissionGrp) {
                            //echo $permission->name."===".$permissionGrp->routes.PHP_EOL;
                            if (strpos($permission->name, $permissionGrp->name) !== false) {
                                //echo 'inn';
                                //$userGroupPremissions[] = $permissionGrp->routes;
                                $userPremissions[] = $permission->name;
                            }
                        }
                    }
                }
            }
        }
        $this->user->allPermissions = $userPremissions;
        $this->user->totalpermissions = $allPermissions;
        //$this->user->userGroupPremissions = $userGroupPremissions;
        return response()->json([
            "code" => 200,
            "data" => $this->user
        ]);
    }

}
