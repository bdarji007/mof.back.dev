<?php

namespace Modules\ClientApp\Http\Controllers;

use App\Jobs\ProcessNotificationEmail;
use App\Notifications\MyNotificationChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Redis;
use Maher\Counters\Facades\Counters;
use Maher\Counters\Models\Counter;
use Modules\ClientApp\Entities\NotificationMessage;
use Modules\ClientApp\Entities\Stp;
use Modules\ClientApp\User;
use Spatie\Permission\Models\Role;

class StpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:stps-view|stps-create|stps-edit|stps-delete|stps-view', ['only' => ['loadStpDataSector', 'show']]);
        $this->middleware('permission:stps-create', ['only' => ['create', 'saveStp']]);
        $this->middleware('permission:stps-edit', ['only' => ['stpeditview']]);
        $this->middleware('permission:stps-view', ['only' => ['stpviewonly']]);
        $this->middleware('permission:stps-delete', ['only' => ['destroy', 'removeStpPermanently']]);
        $this->middleware('permission:stps-costupdation', ['only' => ['stpviewupdation']]);
        $this->middleware('permission:stps-quickview', ['only' => ['loadProcessProjectData']]);

        $this->middleware('permission:stps-approve', ['only' => ['stpApproveReject']]);
        $this->middleware('permission:stps-reject', ['only' => ['stpApproveReject']]);
        $this->middleware('permission:stps-active', ['only' => ['stpActiveInactive']]);
        $this->middleware('permission:stps-deactive', ['only' => ['stpActiveInactive']]);

        $this->middleware('permission:reportsstp-strategiccost', ['only' => ['loadDefaultSectorOrgMtpFiscal']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    function transformTree($treeArrayGroups, $rootArray)
    {
        // Read through all nodes where parent is root array
        foreach ($treeArrayGroups[$rootArray['id']] as $child) {
            //echo $child['id'].PHP_EOL;
            // If there is a group for that child, aka the child has children
            if (isset($treeArrayGroups[$child['id']])) {
                // Traverse into the child
                $newChild = $this->transformTree($treeArrayGroups, $child);
            } else {
                $newChild = $child;
            }

            if ($child['id'] != '') {
                // Assign the child to the array of children in the root node
                $rootArray['tree'][] = $newChild;
            }
        }
        return $rootArray;
    }

    public function loadSTPDefaultData(Request $request)
    {
        $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = 2 UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), 2, '', CONCAT(id, '') from subtenant where id = 2) select id, name as label, name, parent_id from cte order by path"));
        $result = array_map(function ($value) {
            return (array)$value;
        }, $rows);

        // Group by parent id
        $treeArrayGroups = [];
        foreach ($result as $record) {
            $treeArrayGroups[$record['parent_id']][] = $record;
        }
        // Get the root
        $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
        // Transform the data
        $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

        $data = [];
        $data[] = $outputTree;

        $financialuser = \DB::select(\DB::raw("SELECT users.id, CONCAT(users.name, ' ', users.last_name, ' (', users.email, ')') as name FROM users users WHERE (users.subtenant_id =38)"));

        $communicationuser = \DB::select(\DB::raw("SELECT users.id, CONCAT(users.name, ' ', users.last_name, ' (', users.email, ')') as name FROM users users INNER JOIN model_has_roles model on model.model_id=users.id INNER JOIN roles role on model.role_id=role.id WHERE (role.name ='Strategy Executive')"));

        return response()->json([
            "code" => 200,
            "sectors" => $data,
            "communicationuser" => $communicationuser,
            "financialuser" => $financialuser
        ]);
    }

    /***
     * @param $orgid
     * @param $mtp
     * @param $stpid
     * @return \Illuminate\Http\JsonResponse
     *
     * Use Into STP Add and Updation cost
     */
    public function loadSTPUserBySector($orgid, $mtp, $stpid, $fy_id)
    {
        $users = $fiscalYear = $processList = $projectList = $projectMinistry = [];
        $getCostP = $getCostA = $getCostV = $getPercentage = [];
        $getCostPA = $getCostAA = $getCostVA = $getPercentageProjA = $getPercentagePartA = [];
        $registerUser = null;
        if (!empty($orgid)) {
            $getRegisterUser = \DB::select(\DB::raw("SELECT * FROM subtenant where id=$orgid"));
            $registerUser = $getRegisterUser[0]->emp_count;

            $subtypes = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id='" . $orgid . "'"));
            $subtype = $subtypes[0]->subtenant_type_id;
            if ($subtype == 2 or $subtype == 3) {
                $sectorid = $orgid;
                $subtenantid = NULL;
            } else {
                $subtenantid = $orgid;
                $parentids = \DB::select(\DB::raw("SELECT parent_id  FROM subtenant  WHERE id='" . $orgid . "'"));
                $parentid1 = $parentids[0]->parent_id;
                $subtypes1 = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$parentid1"));
                $subtype = $subtypes1[0]->subtenant_type_id;
                if ($subtype == 2 or $subtype == 3) {
                    $sectorid = $parentid1;

                } else {
                    $parentids2 = \DB::select(\DB::raw("SELECT parent_id  FROM subtenant  WHERE id=$parentid1"));
                    $parentid2 = $parentids2[0]->parent_id;
                    $subtypes2 = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$parentid2"));
                    $subtype = $subtypes2[0]->subtenant_type_id;
                    if ($subtype == 2 or $subtype == 3) {
                        $sectorid = $parentid2;

                    } else {
                        $parentids3 = \DB::select(\DB::raw("SELECT parent_id  FROM subtenant  WHERE id=$parentid2"));
                        $parentid3 = $parentids3[0]->parent_id;
                        $subtypes3 = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$parentid3"));
                        $subtype = $subtypes3[0]->subtenant_type_id;
                        if ($subtype == 2 or $subtype == 3) {
                            $sectorid = $parentid3;

                        }
                    }
                }
            }

            $sectorid = ($sectorid) ? $sectorid : $orgid;
            $users = \DB::select(\DB::raw("WITH RECURSIVE cte (level1_id, id, parent_id, subtenant_type, name, path) AS (
	-- This is end of the recursion: Select low level
	select id, id, parent_id, subtenant_type_id, name, concat( cast(id as char(200)), '_')
		from subtenant where
        id = $sectorid -- set your arg here
	UNION ALL
    -- This is the recursive part: It joins to cte
    select c.level1_id, s.id, s.parent_id, s.subtenant_type_id, s.name, CONCAT(c.path, ',', s.id)
		from subtenant s
        inner join cte c on s.parent_id = c.id
	)
	-- select id, name, subtenant_type, parent_id
	--  cte.level1_id, cte.id, cte.parent_id, cte.subtenant_type,
	select cte.name as subname, u.id, CONCAT(u.name, ' ', u.last_name, ' (', u.email, ')') as name
	from cte, users u where
	u.subtenant_id = cte.id
	order by path, u.name;"));


            \DB::statement("set @subtenant = $orgid;");
            \DB::statement("set @fy_id = $fy_id;");
            \DB::statement("set @tenant = 1;");
            $processList = \DB::select(\DB::raw("select pd.id as process_id, pd.name as process_name from process_subtenant_rel psr, process_def pd where psr.subtenant_id = @subtenant and pd.id = psr.process_id;"));

            foreach ($processList as $process) {
                $process->cost_planned = 0;
                $process->cost_actual = 0;
                $process->varitation = 0;
            }


            /*$projectList = \DB::select(\DB::raw("select p.id as project_id, p.symbol as project_symbol,  p.name as project_name, p.has_budget, p.budget_base as cost_planned, p.spending_total as cost_actual, p.subtenant_id from project p where p.subtenant_id = @subtenant and p.status_approval = 1 and p.status_operational != 3;"));*/
            $projectList = \DB::select(\DB::raw("select p.id as project_id, p.subtenant_id, p.symbol as project_symbol,  p.name as project_name, p.has_budget, p.budget_base as cost_planned, p.spending_total as cost_actual from project p, fiscal_year fy where
						fy.id = @fy_id and
						p.subtenant_id = @subtenant and
						p.status_approval = 1 and
						p.status_operational not in (2, 3) and -- exclude on hold and closed
						(
									(p.start_dt_actual between fy.start_date and fy.end_date) -- project start date is within the selected fiscal year
									or
									(ifnull(p.end_dt_actual, CURDATE()) between fy.start_date and fy.end_date) -- project end date is within the selected f.y., if it's null assume it's now to reduce the conditions
									or
									(p.start_dt_actual < fy.start_date and ifnull(p.end_dt_actual, CURDATE()) > fy.end_date) -- project spanning multiple fiscal years
							);"));

            foreach ($projectList as $process) {

                $process->cost_planned = 0;
                $process->cost_actual = 0;
                $process->varitation = 0;

            }

            \DB::statement("set @tenant = 1;");
            /*$projectMinistry = \DB::select(\DB::raw("select p.id as project_id, p.symbol as project_symbol,  p.name as project_name, p.has_budget, p.budget_base as cost_planned, p.spending_total as cost_actual, p.subtenant_id from project p where
						p.tenant_id = @tenant and
						p.status_approval = 1 and
						p.status_operational != 3 and
						not exists (select 1 from project p2 where p2.subtenant_id = @subtenant and p2.id = p.id);"));*/

            $projectMinistry = \DB::select(\DB::raw("select p.id as project_id, p.symbol as project_symbol,  p.name as project_name, p.has_budget, p.budget_base as cost_planned, p.spending_total as cost_actual from project p, fiscal_year fy where
						fy.id = @fy_id and
						p.tenant_id = @tenant and
						p.status_approval = 1 and
						p.status_operational not in (2, 3) and -- exclude on hold and closed
						not exists (select 1 from project p2 where p2.subtenant_id = @subtenant and p2.id = p.id) and
						(
									(p.start_dt_actual between fy.start_date and fy.end_date) -- project start date is within the selected fiscal year
									or
									(ifnull(p.end_dt_actual, CURDATE()) between fy.start_date and fy.end_date) -- project end date is within the selected f.y., if it's null assume it's now to reduce the conditions
									or
									(p.start_dt_actual < fy.start_date and ifnull(p.end_dt_actual, CURDATE()) > fy.end_date) -- project spanning multiple fiscal years
							);"));
        }

        if (!empty($mtp) && !empty($orgid)) {

            if (!empty($stpid)) {
                \DB::statement("set @stp_id = $stpid;");
            } else {
                \DB::statement("set @stp_id = null;");
            }

            \DB::statement("set @mtp = $mtp;");
            \DB::statement("set @subtenant_id = $orgid;");
            $fiscalYear = \DB::select(\DB::raw("select 	fy.* from fiscal_year fy, mtp m,  fiscal_year fs, fiscal_year fe where
						m.id = @mtp and
						fs.id = m.mtp_start and
						fe.id = m.mtp_end and
						fy.start_date >= fs.start_date and
						fy.end_date <= fe.end_date and
						(
								not exists (select 1 from stp where
											stp.subtenant_id = @subtenant_id and
											stp.fiscal_year  = fy.id and
											(stp.id != @stp_id or @stp_id is null)
								) -- exclude fiscal years already defined
						)
order by fy.start_date;"));
        }

        if (!empty($stpid)) {

            $getStp = \DB::select(\DB::raw("SELECT * FROM stp WHERE  id= $stpid  ORDER BY id DESC"));

            $accamulated_total = \DB::select(\DB::raw("SELECT * FROM stp_cost_values WHERE  stp_id= $stpid  ORDER BY id DESC"));
            $totalAccamulate = $totalCost = $totalCostDirect = $totalCostIndirect = $totalProjectCost = 0;
            foreach ($accamulated_total as $total) {
                $totalCostDirect = $totalCostDirect = $total->cost_direct;
                $totalCostIndirect = $totalCostIndirect = $total->cost_indirect;
                $totalProjectCost = $totalProjectCost = $total->cost_projects;
                $totalCost = $total->cost_direct + $total->cost_indirect;
                $totalAccamulate += $total->cost_direct + $total->cost_indirect + $total->cost_projects;
            }

            $getStpProcessItem = \DB::select(\DB::raw("SELECT * FROM `stp_item` where item_type=1 and stp_id=$stpid"));
            $kk = 0;
            foreach ($getStpProcessItem as $process) {
                $getProcess = \DB::select(\DB::raw("SELECT * FROM `process_def` where id='" . $process->item_id . "'"));

                $costA = ($process->pct_capacity) * $totalCost;
                $process->process_id = $getProcess[0]->id;
                $process->process_name = $getProcess[0]->name;
                $process->pct_capacity = ($process->pct_capacity) * 100;
                $process->cost_planned = !empty($process->cost_p) ? number_format(round($process->cost_p, 3), 3) : 0;
                $process->cost_actual = ($totalCost != 0 ? number_format(round($costA, 3), 3) : 0);
                $process->varitation = number_format(round(($costA - $process->cost_p), 3), 3);

                $getCostP[$kk] = $process->cost_p;
                $getCostA[$kk] = $costA;
                $getCostV[$kk] = $costA - $process->cost_p;
                $getPercentage[$kk] = ($process->pct_capacity);
                $kk++;
            }

            $getStpProjectItem = \DB::select(\DB::raw("SELECT * FROM `stp_item` where item_type=2 and stp_id=$stpid"));

            $ki = 0;
            foreach ($getStpProjectItem as $process) {
                $getProject = \DB::select(\DB::raw("SELECT * FROM `project` where id='" . $process->item_id . "'"));
                if ($getProject[0]->has_budget == 1) {
                    if ($getStp[0]->subtenant_id == $getProject[0]->subtenant_id) {
                        $proj = ($process->pct_project) * $totalProjectCost;
                        $actul = ($process->pct_capacity) * $totalCost;
                        $costA = round(((float)$proj + (float)$actul), 3);

                    } else {
                        $costA = round(($process->pct_capacity) * $totalProjectCost, 3);
                    }
                } else {
                    $costA = round(($process->pct_capacity) * $totalCost, 3);
                }

                //$costA = $process->cost_a;
                $process->project_id = $getProject[0]->id;
                $process->project_name = $getProject[0]->name;
                $process->pct_capacity = (int)(($process->pct_capacity) * 100);
                $process->pct_project = (int)(($process->pct_project) * 100);
                $process->cost_planned = number_format(round(($process->cost_p), 3), 3);
                $process->cost_actual = ($totalCost != 0 ? number_format($costA, 3) : 0);
                $process->varitation = number_format(round(($costA - $process->cost_p), 3), 3);
                $process->has_budget = ($getProject[0]->has_budget);
                $process->subtenant_id = ($getProject[0]->subtenant_id);

                $getCostPA[$ki] = $process->cost_p;
                $getCostAA[$ki] = $costA;
                $getCostVA[$ki] = $costA - $process->cost_p;
                $getPercentagePartA[$ki] = (int)(($process->pct_capacity));
                $getPercentageProjA[$ki] = (int)(($process->pct_project));
                $ki++;
            }
        }
        return response()->json([
            "code" => 200,
            "orgapproveuser" => $users,
            "fiscalYear" => $fiscalYear,
            "registerUser" => $registerUser,
            "processList" => (count($getStpProcessItem) > 0) ? $getStpProcessItem : $processList,
            "projectList" => (count($getStpProjectItem) > 0) ? $getStpProjectItem : $projectList,
            "projectMinistry" => $projectMinistry,
            "getCostP" => $getCostP,
            "getCostA" => $getCostA,
            "getCostV" => $getCostV,
            "getPercentage" => $getPercentage,
            "getCostPA" => $getCostPA,
            "getCostAA" => $getCostAA,
            "getCostVA" => $getCostVA,
            "getPercentageProjA" => $getPercentageProjA,
            "getPercentagePartA" => $getPercentagePartA,

        ]);
    }

    /***
     * @param $orgid
     * @param $mtp
     * Use For STP View Get Default
     */
    public function loadDefaultSectorOrgMtpFiscal($orgid, $mtp, $period, $fiscal, $fromList)
    {
        $isAnother = true;
        if(in_array($orgid, [1,2,3,4,5,6,7,8,9])) {
            $isAnother = false;
        }

        $fiscalDates = '';
        if ($fiscal != 'null' && $mtp != 'null') {
            \DB::statement("set @mtp_id = $mtp;");
            $fiscalDate = \DB::select(\DB::raw("select 	mfy.year_no, fy.id as fiscal_year_id, fy.start_date, fy.end_date from fiscal_year fy, mtp_fiscal_years mfy where
						mfy.mtp_id = @mtp_id and
						mfy.fiscal_year_id = fy.id and mfy.year_no=$fiscal
order by mfy.year_no;"));
            $fiscalDates = ($fiscalDate[0]->start_date) ? date('d-m-Y', strtotime($fiscalDate[0]->start_date)) . ' - ' . date('d-m-Y', strtotime($fiscalDate[0]->end_date)) : 0;

        }
        $resultNew = array();
        //if (auth()->user()->hasRole(['Admin'])) {
        if(auth()->user()->currentRole == 'Admin') {
            $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = 2 UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), 2, '', CONCAT(id, '') from subtenant where id = 2) select id, name as label, name, parent_id from cte order by path"));

            $result = array_map(function ($value) {
                return (array)$value;
            }, $rows);

            // Group by parent id
            $treeArrayGroups = [];
            foreach ($result as $record) {
                $treeArrayGroups[$record['parent_id']][] = $record;
            }
            // Get the root
            $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
            // Transform the data
            $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

            $data = [];
            $data[] = $outputTree;
        } else {
            $getSectors = [];
            $dd = 0;
            $getSec = User::select('sector_id', 'subtenant_id')->where('id', auth()->user()->id)->get();
            $getSectors[$dd]['subtenant_id'] = ($getSec[0]['subtenant_id']);
            $getSectors[$dd]['sector_id'] = ($getSec[0]['sector_id']);
            $roles = [];
            foreach (auth()->user()->roles as $role) {
                $roles[$role->id] = $role->id;
            }
            sort($roles);
            $getRole = Role::where('name', auth()->user()->currentRole)->pluck('id')->all();
            /*$workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $roles[0] . "'"));*/
            $workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $getRole[0] . "'"));

            $getBehalfSector = [];
            if (count($workbelf)) {
                $i = 0;
                foreach ($workbelf as $behalf) {
                    $getBehalfSector[$i]['subtenant_id'] = $behalf->subtenant_id;
                    $getBehalfSector[$i]['sector_id'] = $behalf->sector_id;
                    $i++;
                }
            }

            $kk = 1;

            /*var_dump(array_merge($getSectors, $getBehalfSector));
            die;*/
            $isExits = [];
            foreach ((array_merge($getSectors, $getBehalfSector)) as $key => $sector) {
                if (in_array($sector['sector_id'], [1,2,3,4,5,6,7,8,9]) && $sector['sector_id'] == $orgid) {
                    if (!in_array($sector['subtenant_id'], $isExits)) {
                        $isExits[] = $sector['subtenant_id'];
                        $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = '" . $sector['subtenant_id'] . "' UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), '" . $sector['subtenant_id'] . "', '', CONCAT(id, '') from subtenant where id = '" . $sector['subtenant_id'] . "') select id, name as label, name, parent_id from cte order by path"));
                        $result = array_map(function ($value) {
                            return (array)$value;
                        }, $rows);

                        // Group by parent id
                        $treeArrayGroups = [];
                        foreach ($result as $record) {
                            $treeArrayGroups[$record['parent_id']][] = $record;
                        }
                        // Get the root
                        $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
                        // Transform the data
                        $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

                        $data = [];
                        $data[] = $outputTree;
                        $resultNew = array_merge($resultNew, $data);
                    }
                } else if ($isAnother && !in_array($sector['subtenant_id'], [1,2,3,4,5,6,7,8,9])) {
                    if (!in_array($sector['subtenant_id'], $isExits)) {
                        $isExits[] = $sector['subtenant_id'];
                        $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = '" . $sector['subtenant_id'] . "' UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), '" . $sector['subtenant_id'] . "', '', CONCAT(id, '') from subtenant where id = '" . $sector['subtenant_id'] . "') select id, name as label, name, parent_id from cte order by path"));
                        $result = array_map(function ($value) {
                            return (array)$value;
                        }, $rows);

                        // Group by parent id
                        $treeArrayGroups = [];
                        foreach ($result as $record) {
                            $treeArrayGroups[$record['parent_id']][] = $record;
                        }
                        // Get the root
                        $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
                        // Transform the data
                        $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

                        $data = [];
                        $data[] = $outputTree;
                        $resultNew = array_merge($resultNew, $data);
                    }
                }
            }
        }

        $mtps = \DB::select(\DB::raw(" select id,name from mtp ;"));

        if ($mtp == 'null') {
            $mtp = 4;
        }

        if ($orgid == 'null') {
            $orgid = 2;
        }

        if (!empty($mtp) && !empty($orgid)) {

            \DB::statement("set @stp_id = null;");
            \DB::statement("set @mtp = $mtp;");
            \DB::statement("set @subtenant_id = $orgid;");
            $fiscalYear = \DB::select(\DB::raw("select 	fy.* from fiscal_year fy, mtp m,  fiscal_year fs, fiscal_year fe where
						m.id = @mtp and
						fs.id = m.mtp_start and
						fe.id = m.mtp_end and
						fy.start_date >= fs.start_date and
						fy.end_date <= fe.end_date and
						(
								not exists (select 1 from stp where
											stp.subtenant_id = @subtenant_id and
											stp.fiscal_year  = fy.id and
											(stp.id != @stp_id or @stp_id is null)
								) -- exclude fiscal years already defined
						)
order by fy.start_date;"));
        }

        if ($fromList == 'startegic') {

            if ($period != 'null') {
                \DB::statement("set @period = '$period'");
            } else {
                \DB::statement("set @period = $period");
            }
            \DB::statement("set @mtp = $mtp");
            \DB::statement("set @year_no = $fiscal");
            \DB::statement("set @org_unit = $orgid");

            $dataSql = "call p_strategic_direction_perf_and_cost(
                @org_unit,
                @mtp,
                @period,
                @year_no
            );";
            $results = [];
            $pdo = \DB::connection()->getPdo();
            $result = $pdo->prepare($dataSql);
            $result->execute();
            do {
                $resultSet = [];
                foreach ($result->fetchall(\PDO::FETCH_ASSOC) as $res) {
                    array_push($resultSet, $res);
                }
                array_push($results, $resultSet);
            } while ($result->nextRowset());

            $dataHeadingTarget = $results[0];

            $headerBlocksTable = [];
            foreach ($dataHeadingTarget as $ke => $val) {
                if($val['obj_type'] == 'v') {
                    $headerBlocksTable['vision'][] = $val;
                } else if($val['obj_type'] == 't') {
                    $headerBlocksTable['targets'][] = $val;
                } else {
                    $headerBlocksTable['tabledata'][] = $val;
                }
            }
            $dataHeading = $results[1];

            $debugprog = \DB::select(\DB::raw("select value  from system_vars where name='show_<0_prog'"));
            $debugperf = \DB::select(\DB::raw("select value  from system_vars where name='show_>100_perf'"));

            $debugmodeprog = $debugprog[0]->value;
            $debugmodeperf = $debugperf[0]->value;
            if ($debugmodeprog == "true") {
                $debug_progmode = 1;
            } else {
                $debug_progmode = 0;
            }
            if ($debugmodeperf == "true") {
                $debug_mode = true;
            } else {
                $debug_mode = false;
            }
            $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
            $getTenant = [];
            foreach ($tenants as $tent) {
                $getTenant[] = $tent->id;
            }
            $mainVals = [];
            $i=0;
            foreach ($dataHeading as $key => $datas) {
                if ($key == 0) {
                    //$datas['sub_parent'] = null;
                    unset($datas['sub_parent']);
                }

                if ($datas['unit_count'] > 1) {
                    $datas['isParent'] = true;
                } else {
                    $datas['isParent'] = false;
                }

                $datas['prog_sum_w'] = $datas['prog_sum_w'] * 100;

                $datas['showProgIcon'] = 0;
                if ($debug_progmode == 1) {
                    if ($datas['prog_sum_w'] > 100) {
                        $datas['prog_sum_w'] = 100;
                    }
                    //echo $datas->prog_sum_w;
                    if ($datas['prog_sum_w'] < 0) {
                        //echo 'ssss';
                        $datas['prog_sum_w'] = 0;
                        $datas['showProgIcon'] = 1;
                    }
                }

                $datas['perf_sum_w'] = $datas['perf_sum_w'] * 100;

                $datas['showPrefIcon'] = 0;
                if ($debug_mode == true) {
                    if ($datas['perf_sum_w'] > 100) {
                        $datas['perf_sum_w'] = 100;
                    } else if ($datas['perf_sum_w'] < 0) {
                        $datas['perf_sum_w'] = 0;
                        $datas['showPrefIcon'] = 1;
                    }
                }
                $mainVals[] = $datas;
                $i++;
            }

            return response()->json([
                "code" => 200,
                "sectors" => ($resultNew) ? $resultNew : $data,
                "mtp" => $mtps,
                "fiscalYear" => $fiscalYear,
                "dataHeading" => $headerBlocksTable,
                "dataHeirarchy" => (count($mainVals) > 0) ? $mainVals : $dataHeading,
                "fiscalDates" => $fiscalDates
            ]);
        } else {
            if ($period != 'null') {
                \DB::statement("set @period = '$period'");
            } else {
                \DB::statement("set @period = $period");
            }
            \DB::statement("set @mtp = $mtp");
            \DB::statement("set @year_no = $fiscal");
            \DB::statement("set @org_unit = $orgid");

            $dataHeading = \DB::select(\DB::raw("call p_unit_perf_prog_recursive(
        @org_unit,
        @mtp,
        @period,
        @year_no);"));

            $debugprog = \DB::select(\DB::raw("select value  from system_vars where name='show_<0_prog'"));
            $debugperf = \DB::select(\DB::raw("select value  from system_vars where name='show_>100_perf'"));

            $debugmodeprog = $debugprog[0]->value;
            $debugmodeperf = $debugperf[0]->value;
            if ($debugmodeprog == "true") {
                $debug_progmode = 1;
            } else {
                $debug_progmode = 0;
            }
            if ($debugmodeperf == "true") {
                $debug_mode = true;
            } else {
                $debug_mode = false;
            }
            $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
            $getTenant = [];
            foreach ($tenants as $tent) {
                $getTenant[] = $tent->id;
            }
            $mainVals = [];
            foreach ($dataHeading as $key => $datas) {
                if ($fromList == 'startegic') {
                    if ($key == 0) {
                        unset($datas->sub_parent);
                    }
                    if (in_array($datas->sub_id, $getTenant)) {
                        $mainVals[] = $datas;
                    }
                } else {
                    if ($key == 0) {
                        unset($datas->sub_parent);
                    }
                    if ($datas->unit_count > 1) {
                        $datas->isParent = true;
                    } else {
                        $datas->isParent = false;
                    }
                }

                $datas->prog_sum_w = $datas->prog_sum_w * 100;

                $datas->showProgIcon = 0;
                if ($debug_progmode == 1) {
                    if ($datas->prog_sum_w > 100) {
                        $datas->prog_sum_w = 100;
                    }
                    //echo $datas->prog_sum_w;
                    if ($datas->prog_sum_w < 0) {
                        //echo 'ssss';
                        $datas->prog_sum_w = 0;
                        $datas->showProgIcon = 1;
                    }
                }

                $datas->perf_sum_w = $datas->perf_sum_w * 100;

                $datas->showPrefIcon = 0;
                if ($debug_mode == true) {
                    if ($datas->perf_sum_w > 100) {
                        $datas->perf_sum_w = 100;
                    } else if ($datas->perf_sum_w < 0) {
                        $datas->perf_sum_w = 0;
                        $datas->showPrefIcon = 1;
                    }
                }

            }

            return response()->json([
                "code" => 200,
                "sectors" => ($resultNew) ? $resultNew : $data,
                "mtp" => $mtps,
                "fiscalYear" => $fiscalYear,
                "dataHeading" => $dataHeading[0],
                "dataHeirarchy" => (count($mainVals) > 0) ? $mainVals : $dataHeading,
                "fiscalDates" => $fiscalDates
            ]);
        }
    }

    public function loadProcessProjectData($orgid, $mtp, $period, $fiscal)
    {
        if ($period == 'null') {
            $period = '';
        }
        //echo 'sss';
        $dataProcessProject = \DB::select(\DB::raw("
        call p_unit_stp_items(
				$orgid, -- a_sub_id (or id)
				$mtp, -- a_mtp_id
				'$period', -- a_period_name
				$fiscal -- a_year_no
				);
        "));
        $totalPlan = $totalActul = $totalVariance = $totalUnutilize = 0;
        $variancePct = $perfPct = $progPct = 0;

        foreach ($dataProcessProject as $projproc) {
            $totalPlan = $totalPlan + $projproc->cost_p;
            $totalActul = $totalActul + $projproc->cost_a;
            $totalVariance = $totalVariance + $projproc->variance;
            $totalUnutilize = $totalUnutilize + $projproc->squandering;

            $perfPct = $perfPct + $projproc->item_perf;
            $progPct = $progPct + $projproc->item_prog;
        }
        $variancePct = $totalVariance/$totalPlan;
        //$dataProcessProject = [];
        return response()->json([
            "code" => 200,
            "dataProcessProject" => $dataProcessProject,
            "totalPlan" => $totalPlan,
            "totalActul" => $totalActul,
            "totalVariance" => $totalVariance,
            "totalUnutilize" => $totalUnutilize,
            "variancePct" => $variancePct,
            "perfPct" => $perfPct,
            "progPct" => $progPct
        ]);
    }

    public function saveStp(Request $request)
    {

        /* var_dump($request->stp['stp_process']);
         exit();*/
        $subtypes = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id='" . $request->stp["subtenant_id"] . "'"));
        $subtype = $subtypes[0]->subtenant_type_id;
        if ($subtype == 2 or $subtype == 3) {
            $sectorid = $request->stp['subtenant_id'];
            $subtenantid = NULL;
        } else {
            $subtenantid = $request->stp['subtenant_id'];
            $parentids = \DB::select(\DB::raw("SELECT parent_id  FROM subtenant  WHERE id='" . $request->stp['subtenant_id'] . "'"));
            $parentid1 = $parentids[0]->parent_id;
            $subtypes1 = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$parentid1"));
            $subtype = $subtypes1[0]->subtenant_type_id;
            if ($subtype == 2 or $subtype == 3) {
                $sectorid = $parentid1;

            } else {
                $parentids2 = \DB::select(\DB::raw("SELECT parent_id  FROM subtenant  WHERE id=$parentid1"));
                $parentid2 = $parentids2[0]->parent_id;
                $subtypes2 = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$parentid2"));
                $subtype = $subtypes2[0]->subtenant_type_id;
                if ($subtype == 2 or $subtype == 3) {
                    $sectorid = $parentid2;

                } else {
                    $parentids3 = \DB::select(\DB::raw("SELECT parent_id  FROM subtenant  WHERE id=$parentid2"));
                    $parentid3 = $parentids3[0]->parent_id;
                    $subtypes3 = \DB::select(\DB::raw("SELECT subtenant_type_id  FROM subtenant  WHERE id=$parentid3"));
                    $subtype = $subtypes3[0]->subtenant_type_id;
                    if ($subtype == 2 or $subtype == 3) {
                        $sectorid = $parentid3;

                    }
                }
            }
        }
        if (empty($request->stp['id']) || !isset($request->stp['id']) || $request->stp['id'] === 0) {
            $stp = new Stp();
            $stp->mtp_id = $request->stp['mtp_id'];
            $stp->fiscal_year = $request->stp['fiscal_year'];
            $stp->sector_id = $sectorid;
            $stp->subtenant_id = $request->stp['subtenant_id'];
            $stp->responsible_user = $request->stp['responsible_user'];
            $stp->comm_officer = $request->stp['comm_officer'];
            $stp->notes = $request->stp['notes'];
            $cost_direct_p = preg_replace("/[^0-9.]/", "", $request->stp['cost_direct_p']);

            $cost_indirect_p = preg_replace("/[^0-9.]/", "", $request->stp['cost_indirect_p']);

            $cost_projects_p = preg_replace("/[^0-9.]/", "", $request->stp['cost_projects_p']);

            $cost_direct_a = preg_replace("/[^0-9.]/", "", $request->stp['cost_direct_a']);

            $cost_indirect_a = preg_replace("/[^0-9.]/", "", $request->stp['cost_indirect_a']);

            $cost_projects_a = preg_replace("/[^0-9.]/", "", $request->stp['cost_projects_a']);

            $stp->cost_direct_p = ($cost_direct_p) ? (float)(str_replace(",", '', $cost_direct_p)) : null;
            $stp->cost_indirect_p = ($cost_indirect_p) ? (float)(str_replace(",", '', $cost_indirect_p)) : null;
            $stp->cost_projects_p = ($cost_projects_p) ? (float)(str_replace(",", '', $cost_projects_p)) : null;
            $stp->cost_direct_a = ($cost_direct_a) ? (float)(str_replace(",", '', $cost_direct_a)) : null;
            $stp->cost_indirect_a = ($cost_indirect_a) ? (float)(str_replace(",", '', $cost_indirect_a)) : null;
            $stp->cost_projects_a = ($cost_projects_a) ? (float)(str_replace(",", '', $cost_projects_a)) : null;
            $stp->capacity_available = $request->stp['capacity_available'];
            $stp->value_period = $request->stp['value_period'];
            $stp->created_by = auth()->user()->id;
            $getValCount = \DB::select(\DB::raw("select sub.id, sub.name, concat('S', sub.code) as stp_symbol_prefix from subtenant sub where sub.id = '" . $sectorid . "'"));

            if (count($getValCount) > 0 && $getValCount[0]->stp_symbol_prefix != null) {
                $checkCounter = \DB::select(\DB::raw("select * from counters where `key` = '" . $getValCount[0]->stp_symbol_prefix . "'"));
                if (count($checkCounter) > 0) {
                    $count = Counters::increment($getValCount[0]->stp_symbol_prefix);
                    $counter = Counters::getValue($getValCount[0]->stp_symbol_prefix);
                } else {
                    Counter::create([
                        'key' => $getValCount[0]->stp_symbol_prefix,
                        'name' => 'stp_symbol_counter',
                        'initial_value' => 0,
                        'step' => 1
                    ]);
                    $count = Counters::increment($getValCount[0]->stp_symbol_prefix);
                    $counter = Counters::getValue($getValCount[0]->stp_symbol_prefix);
                }

            }
            $stp->symbol = $getValCount[0]->stp_symbol_prefix . '-' . sprintf("%04d", $counter);
            /* echo "<pre>";
             var_dump($request->all());
             die;*/
            if ($stp->save()) {
                if (count($request->stp['stp_user_group']['user_finance']) > 0) {
                    $financeUser = $request->stp['stp_user_group']['user_finance'];
                    $i = 1;
                    foreach ($financeUser as $user) {
                        $notifUser = \DB::table("fn_user_group")->insert(//insert(
                            [
                                'notif_event_id' => 7,
                                'record_id' => $stp->id,
                                'user_id' => $user,
                                'sort_no' => $i
                            ]
                        );
                        $i++;
                    }
                }

                if (count($request->stp['stp_user_group']['user_approval']) > 0) {
                    $approval = $request->stp['stp_user_group']['user_approval'];
                    $i = 1;
                    foreach ($approval as $user) {
                        $notifUser = \DB::table("fn_user_group")->insert(//insert(
                            [
                                'notif_event_id' => 6,
                                'record_id' => $stp->id,
                                'user_id' => $user,
                                'sort_no' => $i
                            ]
                        );
                        $i++;
                    }
                }

                if (count($request->stp['stp_process']) > 0) {
                    $financeUser = $request->stp['stp_process'];
                    $i = 1;
                    foreach ($financeUser as $user) {
                        $cost_p = trim(str_replace("KWD", "", $user['cost_planned']));
                        $cost_a = trim(str_replace("KWD", "", $user['cost_actual']));
                        $notifUser = \DB::table("stp_item")->insert(//insert(
                            [
                                'stp_id' => $stp->id,
                                'item_type' => 1,
                                'item_id' => $user['process_id'],
                                'pct_capacity' => $user['pct_capacity'] / 100,
                                'pct_project' => 0,
                                'cost_p' => ($cost_p) ? (float)(str_replace(",", '', $cost_p)) : 0,
                                'cost_a' => isset($cost_a) ? (float)(str_replace(",", '', $cost_a)) : 0,
                            ]
                        );
                        $i++;
                    }
                }

                if (count($request->stp['stp_project']) > 0) {
                    $approval = $request->stp['stp_project'];
                    $i = 1;
                    foreach ($approval as $user) {
                        $cost_p = trim(str_replace("KWD", "", $user['cost_planned']));
                        $cost_a = trim(str_replace("KWD", "", $user['cost_actual']));

                        $notifUser = \DB::table("stp_item")->insert(//insert(
                            [
                                'stp_id' => $stp->id,
                                'item_type' => 2,
                                'item_id' => $user['project_id'],
                                'pct_capacity' => isset($user['pct_capacity']) ? $user['pct_capacity'] / 100 : 0,
                                'pct_project' => isset($user['pct_project']) ? $user['pct_project'] / 100 : 0,
                                'cost_p' => ($cost_p) ? (float)(str_replace(",", '', $cost_p)) : 0,
                                'cost_a' => isset($cost_a) ? (float)(str_replace(",", '', $cost_a)) : 0,
                            ]
                        );
                        $i++;
                    }
                }

                $messageData['id'] = $stp->symbol;
                $messageData['user'] = auth()->user()->name;
                $messageData['userImage'] = auth()->user()->file_name;
                $messageData['kpiname'] = '';//$model->name;
                $messageData['textshow'] = 'data inserted successfully';
                $messageData['time'] = date('Y-m-d H:i:s');
                //$messageData['allowusers'] = $kpidefdata;

                $notification = new NotificationMessage();
                $notification->user = auth()->user()->name;
                $notification->userImage = auth()->user()->file_name;
                $notification->message = json_encode($messageData, true);
                $notification->save();

                $token = array(
                    'USER_NAME' => auth()->user()->name,
                    'STP_SYMBOL' => $stp->symbol,
                    'KPI_NAME' => '',//$model->name,
                    'MESSAGE' => null,
                    'DATE_TIME' => date('Y-m-d H:i:s'),
                );
                $pattern = '[%s]';
                foreach ($token as $key => $val) {
                    $varMap[sprintf($pattern, $key)] = $val;
                }

                $sendMail = false;

                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=10 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notiArgs = \DB::select(\DB::raw("select name from event_arg arg inner join event_arg_rel rel on rel.arg_id=arg.id where rel.event_id=10"));

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    //$sendMail = true;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=10 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $stp->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=10 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $stp->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $messageRedis = [];
                $messageRedis['id'] = $notification->id;
                $messageRedis['user'] = auth()->user()->name;
                $messageRedis['userImage'] = auth()->user()->file_name;
                $messageRedis['message'] = $messageData;

                $redis = Redis::connection();
                $redis->publish('stpupdate', json_encode($messageRedis, true));

                return response()->json([
                    "code" => 200,
                    "msg" => "data inserted successfully"
                ]);
            }
        } else {

            $stp = new Stp($request->all());
            $stp->setTable("stp");
            $id = $request->stp['id'];

            $query = $stp->find($id);

            $stp = Stp::find($id);
            //$stp->active_status = $request->get('status');
            /*if ($query->update($updates)) {*/
            //if ($stp->save()) {

            /*echo $request->stp['cost_direct_p'].'=='.preg_replace("/[^0-9.]/", "", $request->stp['cost_direct_p']).'==';
            die;*/
            $cost_direct_p = preg_replace("/[^0-9.]/", "", $request->stp['cost_direct_p']);
            $cost_indirect_p = preg_replace("/[^0-9.]/", "", $request->stp['cost_indirect_p']);
            $cost_projects_p = preg_replace("/[^0-9.]/", "", $request->stp['cost_projects_p']);

            $cost_direct_a = preg_replace("/[^0-9.]/", "", $request->stp['cost_direct_a']);
            $cost_indirect_a = preg_replace("/[^0-9.]/", "", $request->stp['cost_indirect_a']);
            $cost_projects_a = preg_replace("/[^0-9.]/", "", $request->stp['cost_projects_a']);

            $stp->mtp_id = $request->stp['mtp_id'];
            $stp->fiscal_year = $request->stp['fiscal_year'];
            $stp->sector_id = $sectorid;
            $stp->subtenant_id = $request->stp['subtenant_id'];
            $stp->responsible_user = $request->stp['responsible_user'];
            $stp->comm_officer = $request->stp['comm_officer'];
            $stp->notes = $request->stp['notes'];
            $stp->cost_direct_p = ($cost_direct_p) ? $cost_direct_p : null;
            $stp->cost_indirect_p = ($cost_indirect_p) ? $cost_indirect_p : null;
            $stp->cost_projects_p = ($cost_projects_p) ? $cost_projects_p : null;

            $stp->cost_direct_a = ($cost_direct_a) ? $cost_direct_a : null;
            $stp->cost_indirect_a = ($cost_indirect_a) ? $cost_indirect_a : null;
            $stp->cost_projects_a = ($cost_projects_a) ? $cost_projects_a : null;

            $stp->capacity_available = $request->stp['capacity_available'];
            $stp->value_period = $request->stp['value_period'];

            if ($stp->save()) {
                if (count($request->stp['stp_user_group']['user_finance']) > 0) {
                    \DB::statement('SET FOREIGN_KEY_CHECKS=0');
                    \DB::delete(\DB::raw("DELETE FROM `fn_user_group` WHERE notif_event_id=7 and record_id='" . $request->stp['id'] . "'"));
                    \DB::statement('SET FOREIGN_KEY_CHECKS=1');
                    //DELETE FROM `fn_user_group` WHERE 0
                    $financeUser = $request->stp['stp_user_group']['user_finance'];
                    $i = 1;
                    foreach ($financeUser as $user) {
                        $notifUser = \DB::table("fn_user_group")->insert(//insert(
                            [
                                'notif_event_id' => 7,
                                'record_id' => $id,
                                'user_id' => $user,
                                'sort_no' => $i
                            ]
                        );
                        $i++;
                    }
                }

                if (count($request->stp['stp_user_group']['user_approval']) > 0) {
                    \DB::statement('SET FOREIGN_KEY_CHECKS=0');
                    \DB::delete(\DB::raw("DELETE FROM `fn_user_group` WHERE notif_event_id=6 and record_id='" . $request->stp['id'] . "'"));
                    \DB::statement('SET FOREIGN_KEY_CHECKS=1');
                    $approval = $request->stp['stp_user_group']['user_approval'];
                    $i = 1;
                    foreach ($approval as $user) {
                        $notifUser = \DB::table("fn_user_group")->insert(//insert(
                            [
                                'notif_event_id' => 6,
                                'record_id' => $id,
                                'user_id' => $user,
                                'sort_no' => $i
                            ]
                        );
                        $i++;
                    }
                }

                /*var_dump($request->stp['stp_project']);
                die;*/
                if (count($request->stp['stp_process']) > 0) {
                    //echo 'sss';
                    $financeUser = $request->stp['stp_process'];
                    $i = 1;
                    foreach ($financeUser as $user) {
                        $checkExistProcess = \DB::select(\DB::raw("SELECT * FROM `stp_item` where item_type=1 and item_id = '" . $user['process_id'] . "' and stp_id='" . $id . "'"));
                        if (count($checkExistProcess) > 0) {
                            $cost_p = trim(str_replace("KWD", "", $user['cost_planned']));
                            $cost_a = trim(str_replace("KWD", "", $user['cost_actual']));

                            $notifUser = \DB::table("stp_item")
                                ->where('stp_id', $id)
                                ->where('item_type', 1)
                                ->where('item_id', $user['process_id'])
                                ->update(
                                    [
                                        'pct_capacity' => isset($user['pct_capacity']) ? $user['pct_capacity'] / 100 : 0,
                                        'cost_p' => ($cost_p) ? (float)(str_replace(",", '', $cost_p)) : 0,
                                        'cost_a' => isset($cost_a) ? (float)(str_replace(",", '', $cost_a)) : 0,
                                    ]
                                );
                        } else {
                            $cost_p = trim(str_replace("KWD", "", $user['cost_planned']));
                            $cost_a = trim(str_replace("KWD", "", $user['cost_actual']));
                            $notifUser = \DB::table("stp_item")->insert(//insert(
                                [
                                    'stp_id' => $id,
                                    'item_type' => 1,
                                    'item_id' => $user['process_id'],
                                    'pct_capacity' => isset($user['pct_capacity']) ? $user['pct_capacity'] / 100 : 0,
                                    'cost_p' => ($cost_p) ? (float)(str_replace(",", '', $cost_p)) : 0,
                                    'cost_a' => isset($cost_a) ? (float)(str_replace(",", '', $cost_a)) : 0,
                                ]
                            );
                            $i++;
                        }
                    }
                }

                if (count($request->stp['stp_project']) > 0) {
                    $approval = $request->stp['stp_project'];
                    $i = 1;
                    foreach ($approval as $user) {
                        $checkExistProcess = \DB::select(\DB::raw("SELECT * FROM `stp_item` where item_type=2 and item_id = '" . $user['project_id'] . "' and stp_id='" . $id . "'"));
                        if (count($checkExistProcess) > 0) {
                            $cost_p = trim(str_replace("KWD", "", $user['cost_planned']));
                            $cost_a = trim(str_replace("KWD", "", $user['cost_actual']));
                            $notifUser = \DB::table("stp_item")
                                ->where('stp_id', $id)
                                ->where('item_type', 2)
                                ->where('item_id', $user['project_id'])
                                ->update(
                                    [
                                        'pct_capacity' => isset($user['pct_capacity']) ? $user['pct_capacity'] / 100 : 0,
                                        'pct_project' => isset($user['pct_project']) ? $user['pct_project'] / 100 : 0,
                                        'cost_p' => ($cost_p) ? (float)(str_replace(",", '', $cost_p)) : 0,
                                        'cost_a' => isset($cost_a) ? (float)(str_replace(",", '', $cost_a)) : 0,
                                    ]
                                );
                        } else {
                            $cost_p = trim(str_replace("KWD", "", $user['cost_planned']));
                            $cost_a = trim(str_replace("KWD", "", $user['cost_actual']));
                            $notifUser = \DB::table("stp_item")->insert(//insert(
                                [
                                    'stp_id' => $id,
                                    'item_type' => 2,
                                    'item_id' => $user['project_id'],
                                    'pct_capacity' => isset($user['pct_capacity']) ? $user['pct_capacity'] / 100 : 0,
                                    'pct_project' => isset($user['pct_project']) ? $user['pct_project'] / 100 : 0,
                                    'cost_p' => ($cost_p) ? (float)(str_replace(",", '', $cost_p)) : 0,
                                    'cost_a' => isset($cost_a) ? (float)(str_replace(",", '', $cost_a)) : 0,
                                ]
                            );
                            $i++;
                        }
                    }
                }

                $messageData['id'] = $stp->symbol;
                $messageData['user'] = auth()->user()->name;
                $messageData['userImage'] = auth()->user()->file_name;
                $messageData['kpiname'] = '';//$model->name;
                $messageData['textshow'] = 'data updated successfully';
                $messageData['time'] = date('Y-m-d H:i:s');
                //$messageData['allowusers'] = $kpidefdata;

                $notification = new NotificationMessage();
                $notification->user = auth()->user()->name;
                $notification->userImage = auth()->user()->file_name;
                $notification->message = json_encode($messageData, true);
                $notification->save();

                $token = array(
                    'USER_NAME' => auth()->user()->name,
                    'STP_SYMBOL' => $stp->symbol,
                    'KPI_NAME' => '',//$model->name,
                    'MESSAGE' => null,
                    'DATE_TIME' => date('Y-m-d H:i:s'),
                );
                $pattern = '[%s]';
                foreach ($token as $key => $val) {
                    $varMap[sprintf($pattern, $key)] = $val;
                }

                $sendMail = false;

                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=11 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notiArgs = \DB::select(\DB::raw("select name from event_arg arg inner join event_arg_rel rel on rel.arg_id=arg.id where rel.event_id=10"));

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    //$sendMail = true;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=11 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $stp->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=11 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $stp->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $messageRedis = [];
                $messageRedis['id'] = $notification->id;
                $messageRedis['user'] = auth()->user()->name;
                $messageRedis['userImage'] = auth()->user()->file_name;
                $messageRedis['message'] = $messageData;

                $redis = Redis::connection();
                $redis->publish('stpupdate', json_encode($messageRedis, true));

                return response()->json([
                    "code" => 200,
                    "msg" => "data updated successfully"
                ]);
            }
        }

        //var_dump($request->stp['mtp_id']);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadStpDataSector($id)
    {
        if ($id != 'null') {
            \DB::statement("set @subtenant = $id;");
            $PriorityType = \DB::select(\DB::raw("WITH RECURSIVE cte ( l1_id, id, parent_id, subtenant_type ) AS (
	SELECT 	id, id, parent_id, subtenant_type_id FROM subtenant WHERE
								id = @subtenant
	UNION ALL
	SELECT c.l1_id, s.id, s.parent_id, s.subtenant_type_id FROM subtenant s
		INNER JOIN 	cte c
								ON 	s.parent_id = c.id
	)
SELECT	@rownum := @rownum + 1 Num,
							sub.NAME AS subtenant_name,
							stp.*,
							(
							SELECT 	min( v.period_dt ) FROM stp_cost_values v WHERE
														v.stp_id = stp.id and
														v.cost_direct is null and
														v.cost_indirect is null and
														v.cost_projects is null
							) next_reading_date
FROM cte, ( SELECT @rownum := 0 ) r, stp
	INNER JOIN 	subtenant sub
							ON 	sub.id = stp.subtenant_id
WHERE
		stp.subtenant_id = cte.id
ORDER BY Num ASC, CAST( stp.symbol AS UNSIGNED ) DESC;"));

            if ($PriorityType) {
                $i = 0;
                foreach ($PriorityType as $Priority) {

                    \DB::statement("set @mtp_id = $Priority->mtp_id;");
                    $fiscalDate = \DB::select(\DB::raw("select 	mfy.year_no, fy.id as fiscal_year_id, fy.start_date, fy.end_date from fiscal_year fy, mtp_fiscal_years mfy where
						mfy.mtp_id = @mtp_id and
						mfy.fiscal_year_id = fy.id and mfy.fiscal_year_id=$Priority->fiscal_year
order by mfy.year_no;"));

                    $PriorityType[$i]->year_no = ($fiscalDate[0]->year_no) ? ($fiscalDate[0]->year_no) : null;
                    $costVal = \DB::select(\DB::raw("SELECT * FROM `stp_cost_values` where stp_id='" . $Priority->id . "' ORDER BY `stp_cost_values`.`id` DESC"));
                    $nextDate = '';
                    if (count($costVal) > 0) {
                        $nextDate = date('Y-m-d', strtotime($costVal[0]->period_dt));
                    }

                    if ($Priority->value_period == 'Q') {
                        $totalEntry = 4;
                    } elseif ($Priority->value_period == 'Y') {
                        $totalEntry = 1;
                    }
                    $isEnable = 'false';
                    // echo count($costVal).' == '.$totalEntry.$nextDate;
                    if (count($costVal) == $totalEntry) {
                        $isEnable = 'true';
                        $PriorityType[$i]->uptodate = true;
                        $PriorityType[$i]->next_reading_date = isset($nextDate) ? $nextDate : null;;
                    } else {
                        $PriorityType[$i]->uptodate = false;
                        $PriorityType[$i]->next_reading_date = isset($Priority->next_reading_date) ? date('Y-m-d', strtotime($Priority->next_reading_date)) : null;
                    }

                    $subtenant = \DB::select(\DB::raw("select name as orgname from subtenant where id='" . $Priority->subtenant_id . "'"));
                    $fiscalyear = \DB::select(\DB::raw("select CONCAT(start_date, '-', end_date) as stp_year  from fiscal_year where id='" . $Priority->fiscal_year . "'"));

                    $PriorityType[$i]->value_period = $Priority->value_period == 'Q' ? 'Quarterly' : 'yearly';
                    $PriorityType[$i]->orgname = $subtenant[0]->orgname;
                    $PriorityType[$i]->stp_year = $fiscalyear[0]->stp_year;

                    $i++;
                }
                return response()->json([
                    "code" => 200,
                    "kpidata" => $PriorityType
                ]);
            } else {
                return response()->json([
                    "code" => 200,
                    "kpidata" => []
                ]);
            }
        } else {
            //if(auth()->user()->hasRole(['Admin'])) {
            if(auth()->user()->currentRole == 'Admin') {
                $id = 1;
            } else {
                $getSectors = [];
                $getSec = User::select('subtenant_id')->where('id',auth()->user()->id)->get();
                $getSectors[] = ($getSec[0]['subtenant_id']);
                $roles = [];
                foreach (auth()->user()->roles as $role) {
                    $roles[$role->id] = $role->id;
                }
                sort($roles);
                /*$workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='".$roles[0]."'"));*/
                $getRole = Role::where('name', auth()->user()->currentRole)->pluck('id')->all();
                $workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $getRole[0] . "'"));
                $getBehalfSector = [];
                if(count($workbelf)) {
                    foreach ($workbelf as $behalf) {
                        $getBehalfSector[] = $behalf->subtenant_id;
                    }
                }

                $id = implode(",", array_merge($getSectors, $getBehalfSector));
            }
            \DB::statement("set @subtenant = 1;");
            $PriorityType = \DB::select(\DB::raw("WITH RECURSIVE cte ( l1_id, id, parent_id, subtenant_type ) AS (
	SELECT 	id, id, parent_id, subtenant_type_id FROM subtenant WHERE
								id in ( $id )
	UNION ALL
	SELECT c.l1_id, s.id, s.parent_id, s.subtenant_type_id FROM subtenant s
		INNER JOIN 	cte c
								ON 	s.parent_id = c.id
	)
SELECT	@rownum := @rownum + 1 Num,
							sub.NAME AS subtenant_name,
							stp.*,
							(
							SELECT 	min( v.period_dt ) FROM stp_cost_values v WHERE
														v.stp_id = stp.id and
														v.cost_direct is null and
														v.cost_indirect is null and
														v.cost_projects is null
							) next_reading_date
FROM cte, ( SELECT @rownum := 0 ) r, stp
	INNER JOIN 	subtenant sub
							ON 	sub.id = stp.subtenant_id
WHERE
		stp.subtenant_id = cte.id
ORDER BY Num ASC, CAST( stp.symbol AS UNSIGNED ) DESC;"));

            if ($PriorityType) {
                $i = 0;
                foreach ($PriorityType as $Priority) {

                    \DB::statement("set @mtp_id = $Priority->mtp_id;");
                    $fiscalDate = \DB::select(\DB::raw("select 	mfy.year_no, fy.id as fiscal_year_id, fy.start_date, fy.end_date from fiscal_year fy, mtp_fiscal_years mfy where
						mfy.mtp_id = @mtp_id and
						mfy.fiscal_year_id = fy.id and mfy.fiscal_year_id=$Priority->fiscal_year
order by mfy.year_no;"));

                    $PriorityType[$i]->year_no = ($fiscalDate[0]->year_no) ? ($fiscalDate[0]->year_no) : null;

                    $costVal = \DB::select(\DB::raw("SELECT * FROM `stp_cost_values` where stp_id='" . $Priority->id . "' ORDER BY `stp_cost_values`.`id` DESC"));
                    $nextDate = '';
                    if (count($costVal) > 0) {
                        $nextDate = date('Y-m-d', strtotime($costVal[0]->period_dt));
                    }

                    if ($Priority->value_period == 'Q') {
                        $totalEntry = 4;
                    } elseif ($Priority->value_period == 'Y') {
                        $totalEntry = 1;
                    }
                    $isEnable = 'false';
                    // echo count($costVal).' == '.$totalEntry.$nextDate;
                    if (count($costVal) == $totalEntry) {
                        $isEnable = 'true';
                        $PriorityType[$i]->uptodate = true;
                        $PriorityType[$i]->next_reading_date = isset($nextDate) ? $nextDate : null;;
                    } else {
                        $PriorityType[$i]->uptodate = false;
                        $PriorityType[$i]->next_reading_date = isset($Priority->next_reading_date) ? date('Y-m-d', strtotime($Priority->next_reading_date)) : null;
                    }

                    $subtenant = \DB::select(\DB::raw("select name as orgname from subtenant where id='" . $Priority->subtenant_id . "'"));
                    $fiscalyear = \DB::select(\DB::raw("select CONCAT(start_date, '-', end_date) as stp_year  from fiscal_year where id='" . $Priority->fiscal_year . "'"));

                    $PriorityType[$i]->value_period = $Priority->value_period == 'Q' ? 'Quarterly' : 'yearly';
                    $PriorityType[$i]->orgname = $subtenant[0]->orgname;
                    $PriorityType[$i]->stp_year = $fiscalyear[0]->stp_year;

                    $i++;
                }
                return response()->json([
                    "code" => 200,
                    "kpidata" => $PriorityType
                ]);
            }
        }
        return response()->json([
            "code" => 200,
            "kpidata" => []
        ]);
    }

    public function stpviewonly(Request $request, $id) {
        return $this->stpeditview($request, $id);
    }
    public function stpeditview(Request $request, $id)
    {
        $stp = Stp::Where('id', $id)->first();
        if ($stp) {
            $getFiscalYear = \DB::select(\DB::raw("SELECT * FROM fiscal_year where id='" . $stp->fiscal_year . "'"));

            $registerUser = $nextdate = $capacity_justification = null;
            $getRegisterUser = \DB::select(\DB::raw("SELECT * FROM subtenant where id=$stp->subtenant_id"));
            $registerUser = $getRegisterUser[0]->emp_count;

            $projnextdate = \DB::select(\DB::raw("SELECT period_dt, capacity_justification FROM stp_cost_values WHERE  stp_id= $id  ORDER BY id DESC LIMIT 1"));
            if ($projnextdate) {
                $nextdate = $projnextdate[0]->period_dt;
                $capacity_justification = $projnextdate[0]->capacity_justification;
            }

            $projprevoiusdate = \DB::select(\DB::raw("SELECT period_dt, capacity_justification FROM stp_cost_values WHERE  stp_id= $id  ORDER BY id DESC LIMIT 1, 1"));
            $perviousDate = '';
            if ($projprevoiusdate) {
                $perviousDate = $projprevoiusdate[0]->period_dt;
            }

            $accamulated_total = \DB::select(\DB::raw("SELECT * FROM stp_cost_values WHERE  stp_id= $id  ORDER BY id DESC"));
            $totalAccamulate = $totalCost = $totalCostDirect = $totalCostIndirect = $totalProjectCost = 0;
            foreach ($accamulated_total as $total) {
                $totalCostDirect = $totalCostDirect + $total->cost_direct;
                $totalCostIndirect = $totalCostIndirect + $total->cost_indirect;
                $totalProjectCost = $totalProjectCost + $total->cost_projects;
                $totalCost = $totalCost + $total->cost_direct + $total->cost_indirect + $total->cost_projects;
                $totalAccamulate = $total->cost_direct + $total->cost_indirect + $total->cost_projects;
            }

            $getUsersApproval = \DB::select(\DB::raw("SELECT * FROM `fn_user_group` where notif_event_id=6 and record_id=$id"));
            $getUserFinance = \DB::select(\DB::raw("SELECT * FROM `fn_user_group` where notif_event_id=7 and record_id=$id"));
            $userApproval = [];
            foreach ($getUsersApproval as $role) {
                $userApproval[] = $role->user_id;
            }

            $userApp['user_approval'] = $userApproval;

            $userFinance = [];
            foreach ($getUserFinance as $user) {
                $userFinance[] = $user->user_id;
            }

            $userApp['user_finance'] = $userFinance;

            $readingslist = \DB::select(\DB::raw("SELECT * FROM stp_cost_values WHERE  stp_id= $id and cost_direct!=''"));

            /*if ($readingslist) {
                $i = 0;
                foreach ($readingslist as $readlist) {
                    $readingslist[$i]->period_dt = $readlist->period_dt;
                    $dt = new \DateTime($readingslist[$i]->period_dt);
                    $readingslist[$i]->period_dt = $dt->format('d-m-Y');
                    $i++;
                }
                return response()->json([
                    "code" => 200,
                    "readingslist" => $readingslist
                ]);
            }*/

            $stp->stp_user_group = $userApp;
            $stp->fiscal_dates = ($getFiscalYear[0]->start_date) . '-' . ($getFiscalYear[0]->end_date);
            $stp->stp_total_p = 'KWD ' . number_format(($stp->cost_direct_p + $stp->cost_indirect_p + $stp->cost_projects_p), 3);
            $stp->capacity_justification = ($capacity_justification) ? $capacity_justification : '';
            $stp->registerUser = $registerUser;
            $stp->nextdate = $nextdate;
            $stp->perviousDate = $perviousDate;
            $stp->totalAccamulate = 'KWD ' . number_format($totalAccamulate, 3);
            $stp->cost_direct_a = 'KWD ' . number_format($totalCostDirect, 3);
            $stp->cost_indirect_a = 'KWD ' . number_format($totalCostIndirect, 3);
            $stp->cost_projects_a = 'KWD ' . number_format($totalProjectCost, 3);
            $stp->stp_total_a = 'KWD ' . number_format($totalCost, 3);
            $stp->cost_direct_p = 'KWD ' . number_format($stp->cost_direct_p, 3);
            $stp->cost_indirect_p = 'KWD ' . number_format($stp->cost_indirect_p, 3);
            $stp->cost_projects_p = 'KWD ' . number_format($stp->cost_projects_p, 3);


            return response()->json([
                "code" => 200,
                "stpdata" => $stp,
                "readingslist" => $readingslist,
            ]);
        }
        return response()->json([
            "code" => 404,
            "msg" => "data not found"
        ]);
    }

    public function stpviewupdation(Request $request, $id)
    {
        $stp = Stp::Where('id', $id)->first();
        if ($stp) {
            $getFiscalYear = \DB::select(\DB::raw("SELECT * FROM fiscal_year where id='" . $stp->fiscal_year . "'"));

            $registerUser = $nextdate = $capacity_justification = null;
            $getRegisterUser = \DB::select(\DB::raw("SELECT * FROM subtenant where id=$stp->subtenant_id"));
            $registerUser = $getRegisterUser[0]->emp_count;

            $projnextdate = \DB::select(\DB::raw("SELECT period_dt, capacity_justification FROM stp_cost_values WHERE  stp_id= $id  ORDER BY id DESC LIMIT 1"));
            if ($projnextdate) {
                $nextdate = $projnextdate[0]->period_dt;
                $capacity_justification = $projnextdate[0]->capacity_justification;
            }

            $projprevoiusdate = \DB::select(\DB::raw("SELECT period_dt, capacity_justification FROM stp_cost_values WHERE  stp_id= $id  ORDER BY id DESC LIMIT 1, 1"));
            $perviousDate = '';
            if ($projprevoiusdate) {
                $perviousDate = $projprevoiusdate[0]->period_dt;
            }

            $accamulated_total = \DB::select(\DB::raw("SELECT * FROM stp_cost_values WHERE  stp_id= $id  ORDER BY id DESC"));
            $totalAccamulate = $totalCost = $totalCostDirect = $totalCostIndirect = $totalProjectCost = 0;
            $countTot = 0;
            foreach ($accamulated_total as $total) {
                $totalCostDirect = $totalCostDirect = $total->cost_direct;
                $totalCostIndirect = $totalCostIndirect = $total->cost_indirect;
                $totalProjectCost = $totalProjectCost = $total->cost_projects;
                $totalCost = $total->cost_direct + $total->cost_indirect + $total->cost_projects;
                $totalAccamulate += $total->cost_direct + $total->cost_indirect + $total->cost_projects;
                $countTot++;
            }

            $getUsersApproval = \DB::select(\DB::raw("SELECT * FROM `fn_user_group` where notif_event_id=6 and record_id=$id"));
            $getUserFinance = \DB::select(\DB::raw("SELECT * FROM `fn_user_group` where notif_event_id=7 and record_id=$id"));
            $userApproval = [];
            foreach ($getUsersApproval as $role) {
                $userApproval[] = $role->user_id;
            }

            $userApp['user_approval'] = $userApproval;

            $userFinance = [];
            foreach ($getUserFinance as $user) {
                $userFinance[] = $user->user_id;
            }

            $userApp['user_finance'] = $userFinance;

            $readingslist = \DB::select(\DB::raw("SELECT * FROM stp_cost_values WHERE  stp_id= $id and (cost_direct is not null OR cost_indirect is not null OR cost_projects is not null )"));

            /*if ($readingslist) {
                $i = 0;
                foreach ($readingslist as $readlist) {
                    $readingslist[$i]->period_dt = $readlist->period_dt;
                    $dt = new \DateTime($readingslist[$i]->period_dt);
                    $readingslist[$i]->period_dt = $dt->format('d-m-Y');
                    $i++;
                }
                return response()->json([
                    "code" => 200,
                    "readingslist" => $readingslist
                ]);
            }*/

            if ($stp->value_period == 'Q') {
                $totalEntry = 4;
            } elseif ($stp->value_period == 'Y') {
                $totalEntry = 1;
            }
            $isEnable = 'false';
            if ($countTot <= $totalEntry) {
                $isEnable = 'true';
            }

            $stp->stp_user_group = $userApp;
            $stp->fiscal_dates = ($getFiscalYear[0]->start_date) . '-' . ($getFiscalYear[0]->end_date);
            $stp->stp_total_p = 'KWD ' . number_format(($stp->cost_direct_p + $stp->cost_indirect_p + $stp->cost_projects_p), 3);
            $stp->capacity_justification = ($capacity_justification) ? $capacity_justification : '';
            $stp->registerUser = $registerUser;
            $stp->nextdate = $nextdate;
            $stp->perviousDate = $perviousDate;
            $stp->totalAccamulate = 'KWD ' . number_format($totalAccamulate, 3);
            $stp->cost_direct_a = '';
            $stp->cost_indirect_a = '';
            $stp->cost_projects_a = '';
            $stp->stp_total_a = 'KWD';
            $stp->cost_direct_p = 'KWD ' . number_format($stp->cost_direct_p, 3);
            $stp->cost_indirect_p = 'KWD ' . number_format($stp->cost_indirect_p, 3);
            $stp->cost_projects_p = 'KWD ' . number_format($stp->cost_projects_p, 3);
            $stp->isEnable = $isEnable;

            return response()->json([
                "code" => 200,
                "stpdata" => $stp,
                "readingslist" => $readingslist,
            ]);
        }
        return response()->json([
            "code" => 404,
            "msg" => "data not found"
        ]);
    }

    public function removeStpPermanently($id)
    {
        /**delete kpi_def**/
        if (\DB::delete(\DB::raw("delete sd from stp sd where sd.status_approval = 0 and sd.active_status = 0 and sd.id = $id"))) {
            return response()->json([
                "code" => 200,
                "msg" => "deleted the record"
            ]);
        }

        return response()->json([
            "code" => 400,
            "msg" => "error deleting the data"
        ]);
    }

    public function updateEmpCount(Request $request)
    {
        $update = \DB::update(\DB::raw("UPDATE subtenant set emp_count='" . $request->value . "' where id='" . $request->orgId . "'"));
        if ($update) {
            return response()->json([
                "code" => 400,
                "msg" => "data updated successfully"
            ]);
        }
        return response()->json([
            "code" => 400,
            "msg" => "error deleting the data"
        ]);
    }

    public function selectProjects(Request $request)
    {

        if (count($request->ministryProj) > 0) {
            $ministryProject = [];
            $j = 0;
            foreach ($request->ministryProj as $proj) {
                $getProject = \DB::select(\DB::raw("SELECT * FROM `project` where id='" . $proj . "'"));
                $ministryProject[$j]['project_id'] = $getProject[0]->id;
                $ministryProject[$j]['project_name'] = $getProject[0]->name;
                $ministryProject[$j]['cost_planned'] = 0;
                $ministryProject[$j]['cost_actual'] = 0;
                $ministryProject[$j]['has_budget'] = $getProject[0]->has_budget;
                $j++;
            }
            return response()->json([
                "code" => 400,
                "ministryProject" => $ministryProject
            ]);
        } else {
            return response()->json([
                "code" => 400,
                "msg" => "error deleting the data"
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Kpi Approve and Reject
     */
    public function stpApproveReject(Request $request)
    {

        /////// For Audit Purpose
        $model = new Stp($request->all());
        $model->setTable("stp");
        $id = $request->get('stpid');
        $query = $model->find($id);
        $updates["status_approval"] = $request->get('status');
        //$updates["reject_reason"] = $request->get('rejectreason');
        if ($query->update($updates)) {
            $status = $request->get('status');

            $messageData['id'] = $query->symbol;
            $messageData['user'] = auth()->user()->name;
            $messageData['userImage'] = auth()->user()->file_name;
            $messageData['kpiname'] = '';//$query->name;
            $messageData['textshow'] = ($status == 1) ? 'kpi approve successfully' : 'kpi reject successfully';
            $messageData['time'] = date('Y-m-d H:i:s');
            //$messageData['allowusers'] = $kpidefdata;

            $notification = new NotificationMessage();
            $notification->user = auth()->user()->name;
            $notification->userImage = auth()->user()->file_name;
            $notification->message = json_encode($messageData, true);
            $notification->save();

            $token = array(
                'USER_NAME' => auth()->user()->name,
                'STP_SYMBOL' => $query->symbol,
                'KPI_NAME' => '',//$query->name,
                'MESSAGE' => null,
                'DATE_TIME' => date('Y-m-d H:i:s'),
            );
            $pattern = '[%s]';
            foreach ($token as $key => $val) {
                $varMap[sprintf($pattern, $key)] = $val;
            }
            if ($status == 1) {
                $value_periodval = \DB::select(\DB::raw("select f.start_date, s.value_period, s.cost_direct_p, s.cost_indirect_p, s.capacity_available from stp s INNER JOIN  fiscal_year f on f.id=s.fiscal_year where s.`id` =$id"));
                $value_period = $value_periodval[0]->value_period;
                $start_date = $value_periodval[0]->start_date;
                $cost_direct_p = $value_periodval[0]->cost_direct_p;
                $cost_indirect_p = $value_periodval[0]->cost_indirect_p;
                $capacity_available = $value_periodval[0]->capacity_available;

                $currentdate = date("Y-m-d");
                if ($start_date < $currentdate) {
                    // echo "progress";
                    //$updates["status_operational"] = 1;
                    //$updates["start_dt_actual"] = $currentdate;
                    //$query->update($updates);

                    if ($value_period == 'Q') {
                        $time = strtotime($start_date);
                        $nextreadingdate = date("Y-m-d H:i:s", strtotime("+3 month", $time));
                    }

                    if ($value_period == 'Y') {
                        $nextreadingdate = date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime($start_date)) . " + 1 year"));
                    }

                    \DB::insert("INSERT INTO `stp_cost_values` (`stp_id`, `period_no`,`period_dt`) values ( $id, 1, '$nextreadingdate')");
                } else {
                    echo "not";
                }

                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=14 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notiArgs = \DB::select(\DB::raw("select name from event_arg arg inner join event_arg_rel rel on rel.arg_id=arg.id where rel.event_id=4"));

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    $sendMail = false;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=14 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=14 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }
            } else if($status == 0 ) {
                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=15 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);
                    $notiArgs = \DB::select(\DB::raw("select * from event_arg arg inner join event_arg_rel rel on rel.arg_id=arg.id where rel.event_id=5"));
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    //$sendMail = false;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=15 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {

                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    //echo $userString;
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;

                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];
                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=15 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {

                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    //echo $userString;
                    $messageDataNotif['id'] = $query->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;

                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];
                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }
            }

            $messageRedis = [];
            $messageRedis['id'] = $notification->id;
            $messageRedis['user'] = auth()->user()->name;
            $messageRedis['userImage'] = auth()->user()->file_name;
            $messageRedis['message'] = $messageData;

            $redis = Redis::connection();
            $redis->publish('stpupdate', json_encode($messageRedis, true));

            /*$redis = Redis::connection();
            $redis->publish('message', '');*/

            /* $redis1 = Redis::connection();
             $redis1->publish('dashboard', '');*/

            return response()->json([
                "code" => 200,
                "msg" => ($status == 1) ? 'stp approved successfully' : 'stp rejected successfully.please check the rejected reason and Resubmit the form'
            ]);
        }
        return response()->json([
            "code" => 400,
            "msg" => 'failed to update status'
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Kpi Active and Deactive
     */
    public function stpActiveInactive(Request $request)
    {
        /////// For Audit Purpose
        /*$model = new Stp($request->all());
        $model->setTable("stp");
        $id = $request->get('stpid');
        $query = $model->find($id);
        $updates["active_status"] = 1;*/
        $stp = Stp::find($request->stpid);
        $stp->active_status = $request->get('status');
        /*if ($query->update($updates)) {*/
        if ($stp->save()) {
            $status = $request->get('status');

            $messageData['id'] = $stp->symbol;
            $messageData['user'] = auth()->user()->name;
            $messageData['userImage'] = auth()->user()->file_name;
            $messageData['kpiname'] = '';//$query->name;
            $messageData['textshow'] = ($status == 1) ? 'kpi activated successfully' : 'kpi deactivated successfully';
            $messageData['time'] = date('Y-m-d H:i:s');
            //$messageData['allowusers'] = $kpidefdata;

            $notification = new NotificationMessage();
            $notification->user = auth()->user()->name;
            $notification->userImage = auth()->user()->file_name;
            $notification->message = json_encode($messageData, true);
            $notification->save();

            $token = array(
                'USER_NAME' => auth()->user()->name,
                'STP_SYMBOL' => $stp->symbol,
                'KPI_NAME' => '',//$query->name,
                'MESSAGE' => null,
                'DATE_TIME' => date('Y-m-d H:i:s'),
            );
            $pattern = '[%s]';
            foreach ($token as $key => $val) {
                $varMap[sprintf($pattern, $key)] = $val;
            }


            $sendMail = false;
            if ($status == 1) {
                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=12 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);

                    $notiArgs = \DB::select(\DB::raw("select name from event_arg arg inner join event_arg_rel rel on rel.arg_id=arg.id where rel.event_id=4"));

                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    $sendMail = false;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=12 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $stp->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                    /*$insertNotiId = \DB::table("notif")->insertGetId(//insert(
                        [
                            'notif_def_id' => $notiId,
                            'content' => json_encode($messageData, true),
                            'created_by' => auth()->user()->id,
                        ]
                    );

                    if($insertNotiId) {
                        if(count($notificUsers) > 0 ) {
                            foreach ($notificUsers as $user) {
                                $insertNotiId = \DB::table("notif_deliv_status")->insertGetId(//insert(
                                    [
                                        'notif_id' => $insertNotiId,
                                        'user_id' => $user->user_id,
                                        'delivered_at' => date('Y-m-d H:i:s'),
                                    ]
                                );
                            }
                        }
                    }*/
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=12 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {
                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    /*echo $userString;*/
                    $messageDataNotif['id'] = $stp->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;


                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];

                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }
            } else if ($status == 0) {
                $checkEmailEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=13 and channel='e' and status_active=1"));
                if (count($checkEmailEvent) > 0) {
                    $notiId = $checkEmailEvent[0]->id;
                    $notiContent = strtr($checkEmailEvent[0]->contents, $varMap);
                    $notiArgs = \DB::select(\DB::raw("select * from event_arg arg inner join event_arg_rel rel on rel.arg_id=arg.id where rel.event_id=5"));
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));
                    //$sendMail = false;
                    $this->dispatch(new ProcessNotificationEmail($notificUsers, 'Test', $notiContent));
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=13 and channel='i' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {

                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    //echo $userString;
                    $messageDataNotif['id'] = $stp->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;

                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];
                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                    /* $insertNotiId = \DB::table("notif")->insertGetId(//insert(
                         [
                             'notif_def_id' => $notiId,
                             'content' => json_encode($messageData, true),
                             'created_by' => auth()->user()->id,
                         ]
                     );

                     if ($insertNotiId) {
                         if (count($notificUsers) > 0) {
                             foreach ($notificUsers as $user) {
                                 $insertNotiId = \DB::table("notif_deliv_status")->insertGetId(//insert(
                                     [
                                         'notif_id' => $insertNotiId,
                                         'user_id' => $user->user_id,
                                         'delivered_at' => date('Y-m-d H:i:s'),
                                     ]
                                 );
                             }
                         }
                     }*/
                }

                $checkInBoxEvent = \DB::select(\DB::raw("SELECT * from notif_def where event_id=13 and channel='a' and status_active=1"));
                if (count($checkInBoxEvent) > 0) {

                    $notiId = $checkInBoxEvent[0]->id;
                    $notiContent = strtr($checkInBoxEvent[0]->contents, $varMap);
                    $notificUsers = \DB::select(\DB::raw("SELECT * from notif_to_user where notif_id=$notiId"));

                    $userNoti = [];
                    foreach ($notificUsers as $usernoti) {
                        $userNoti[] = $usernoti->user_id;
                    }
                    $userString = implode(',', $userNoti);
                    //echo $userString;
                    $messageDataNotif['id'] = $stp->symbol;
                    $messageDataNotif['user'] = auth()->user()->name;
                    $messageDataNotif['userImage'] = auth()->user()->file_name;
                    $messageDataNotif['message'] = $notiContent;

                    $user = User::whereIn('id', $userNoti)->get();

                    $details = [
                        'content' => $messageDataNotif,
                        'noti_def_id' => $notiId
                    ];
                    Notification::send($user, new MyNotificationChannel($details, $notiId));
                }
            }


            $messageRedis = [];
            $messageRedis['id'] = $notification->id;
            $messageRedis['user'] = auth()->user()->name;
            $messageRedis['userImage'] = auth()->user()->file_name;
            $messageRedis['message'] = $messageData;

            $redis = Redis::connection();
            $redis->publish('stpupdate', json_encode($messageRedis, true));

            return response()->json([
                "code" => 200,
                "msg" => ($status == 1) ? 'stp activated successfully' : 'stp deactivated successfully'
            ]);
        }
        return response()->json([
            "code" => 400,
            "msg" => 'failed to update active and inactive status'
        ]);
    }

    public function saveStpCostUpdation(Request $request)
    {
        $stpid = $request->stp['id'];

        $data = \DB::select(\DB::raw("SELECT * FROM stp_cost_values WHERE  stp_id= $stpid  ORDER BY id DESC LIMIT 1"));
        $valueid = $data[0]->id;
        $valuedate = $data[0]->period_dt;
        $period_no = $data[0]->period_no + 1;

        $cost_direct_a = preg_replace("/[^0-9.]/", "", $request->stp['cost_direct_a']);

        $cost_indirect_a = preg_replace("/[^0-9.]/", "", $request->stp['cost_indirect_a']);

        $cost_projects_a = preg_replace("/[^0-9.]/", "", $request->stp['cost_projects_a']);

        //if(count($data) > 0 ) {
        $notifUser = \DB::table("stp_cost_values")
            ->where('stp_id', $stpid)
            ->where('period_no', $data[0]->period_no)
            ->update(
                [
                    'cost_direct' => ($cost_direct_a) ? $cost_direct_a : null,
                    'cost_indirect' => ($cost_indirect_a) ? $cost_indirect_a : null,
                    'cost_projects' => ($cost_projects_a) ? $cost_projects_a : null,
                    'capacity_available' => $request->stp['capacity_available'],
                    'capacity_registered' => $request->stp['registerUser'],
                    'capacity_justification' => $request->stp['capacity_justification'],
                ]
            );

        if ($notifUser) {
            $accamulated_total = \DB::select(\DB::raw("SELECT * FROM stp_cost_values WHERE  stp_id= $stpid  ORDER BY id DESC"));
            $countTot = count($accamulated_total);

            $stp = Stp::Where('id', $stpid)->first();
            if ($stp->value_period == 'Q') {
                $totalEntry = 4;
            } elseif ($stp->value_period == 'Y') {
                $totalEntry = 1;
            }
            $isEnable = 'false';
            if ($countTot < $totalEntry) {
                $isEnable = 'true';
            }

            $timestamp = $valuedate;
            if ($request->stp['value_period'] == 'Q') {
                $time = strtotime($timestamp);
                $nextreadingdate = date("Y-m-d", strtotime("+3 month", $time));
            }

            if ($request->stp['value_period'] == 'Y') {
                $nextreadingdate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($timestamp)) . " + 1 year"));
            }

            if ($isEnable != 'false') {
                $notifUser = \DB::table("stp_cost_values")->insert(//insert(
                    [
                        'stp_id' => $stpid,
                        'period_no' => $period_no,
                        'period_dt' => $nextreadingdate,
                    ]
                );
            }
        }


        /*} else {

        }*/
    }
}
