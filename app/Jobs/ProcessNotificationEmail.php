<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ProcessNotificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $emailto;
    protected $user_info;
    protected $isMulti;
    protected $isGroup;
    protected $subject;
    protected $message;
    protected $userLogin;

    public function __construct($emailto, $subject, $message)
    {
        $this->emailto = $emailto;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        foreach ($this->emailto as $ids) {
            Mail::send('emails.message', [
                'USER_NAME' => 'Bhavesh',
                'CONTENT' => $this->message,

            ], function ($message) {
                $message->from('bhaveshdarji386@gmail.com', 'Developer');
                $message->to('granthjain14@gmail.com')
                    ->subject(date('dMY H:i').' Test Email');
            });
        }

        /*if (!$this->isMulti) {
            Mail::send('emails.message', [
                'USER_NAME' => $this->emailto['assignee_name'],
                'USER_TASK_REF' => ($this->emailto) ? $this->emailto['task_ref'] : '',
                'SUPERVISION_ROLE' => $this->user_info[0]->name,
                'CONTENT' => $this->message,
                'SUPERVISION_NAME' => $this->user_info[0]->name_ar,
                'IsGroup' => $this->isGroup

            ], function ($message) {
                $message->from('task-notifier@iriyada.com', $this->user_info[0]->name);
                $message->to($this->emailto['assignee_email'])
                    ->subject(date('dMY H:i').' '.$this->subject);
            });
        } else {
            if ($this->isGroup) {
                $supe = [];
                foreach ($this->emailto as $em) {
                    if (!in_array($em['id'], $supe)) {
                        $supe[$em['id']] = $em;
                    }
                }

                $i = 1;
                foreach ($supe as $su) {
                    if ($su["id"] == $this->user_info[0]->supervision || $this->userLogin->hasRole('Manager')) {
                        $user_without_task = \DB::select(\DB::raw("select s.id,	s.name 'supervision_name', ifnull(u.name_ar, u.name) 'assignee_name', case when u.id = u_sup.id and u.supervision != 2 then u_sup_1.email else u_sup.email end supervisor_email, 0 'Task Count' from users u, supervision s, users u_sup, users u_sup_1, supervision supervision_1 where s.code = u.supervision and u_sup.id =  s.supervisor_id and (supervision_1.id = 1 and u_sup_1.id = supervision_1.supervisor_id ) and s.code in (1,2,3,4,5) and u.isactive = 1 and (u.expiry_date is null or current_date() <=u.expiry_date) and not exists ( select 1 from easycases c, projects p where c.project_id = p.id and p.project_type in (1,2,3,4,5) and c.assign_to = u.id and c.istype = 1 and c.legend not in (3,5)) and u.is_external = 0 and u.id != s.supervisor_id and not exists(select 1 from leaves l where l.user_id = u.id and date(curdate())>= l.date_from and date(curdate())<= l.date_to) and s.id='" . $su["id"] . "' order by s.code, assignee_name"));

                        $table = '<table><tbody>';
                        $j = 1;
                        foreach ($user_without_task as $without) {
                            $table .= '<tr><td>' . $j . '</td><td>' . $without->assignee_name . '</td></tr>';
                            $j++;
                        }
                        $table .= '</tbody></table>';


                        $this->email =  $su['supervisor_email'];
                        Mail::send('emails.message', [
                            'USER_NAME' => $su['supervision_name'],
                            'USER_TASK_REF' => '',
                            'SUPERVISION_ROLE' => $this->user_info[0]->name,
                            'CONTENT' => $this->message . $table,
                            'SUPERVISION_NAME' => $this->user_info[0]->name_ar,
                            'IsGroup' => $this->isGroup

                        ], function ($message) {
                            $message->from('task-notifier@iriyada.com', $this->user_info[0]->name);
                            $message->to($this->email)
                                ->subject(date('dMY H:i').' '.$this->subject);
                        });

                    }
                }
            } else {
                foreach ($this->emailto as $em) {
                    if ($em['supervision_id'] == $this->user_info[0]->supervision || $this->userLogin->hasRole('Manager')) {
                        $this->email = $em['assignee_email'];
                        Mail::send('emails.message', [
                            'USER_NAME' => $em['assignee_name'],
                            'USER_TASK_REF' => $em['task_ref'],
                            'SUPERVISION_ROLE' => $this->user_info[0]->name,
                            'CONTENT' => $this->message,
                            'SUPERVISION_NAME' => $this->user_info[0]->name_ar,
                            'IsGroup' => $this->isGroup

                        ], function ($message) {
                            $message->from('task-notifier@iriyada.com', $this->user_info[0]->name);
                            $message->to($this->email)
                                ->subject(date('dMY H:i').' '.$this->subject);
                        });

                    }
                }
            }
        }*/
    }
}
